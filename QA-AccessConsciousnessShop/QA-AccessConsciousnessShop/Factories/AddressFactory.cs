﻿using SeleniumCommon.Objects.User.Address;

namespace QA_AccessConsciousnessShop.Factories
{
    class AddressFactory
    {
        public Address GetAddress(AddressType addressType)
        {
            Address address = new Address();
            switch (addressType)
            {
                case AddressType.USAAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "United States";
                        address.AddressStreet = "Street Force One";
                        address.Zip = "94549";
                        address.City = "Lafayette";
                        address.State = "California";
                        address.PhoneNumber = "+18317527321";
                        break;
                    }
                case AddressType.AustraliaAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "Australia";
                        address.AddressStreet = "Street Force One";
                        address.Zip = "96720";
                        address.City = "Queensland";
                        address.State = "Queensland";
                        address.PhoneNumber = "+43252342342";
                        break;
                    }
                case AddressType.AlaskaAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "United States";
                        address.AddressStreet = "White Great Forest";
                        address.AddressUnit = "cluji-cluji";
                        address.Zip = "99605";
                        address.City = "Hope";
                        address.State = "Alaska";
                        address.PhoneNumber = "(907) 269-0330";
                        break;
                    }
                case AddressType.TexasAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "United States";
                        address.AddressStreet = "Great Sand desert";
                        address.AddressUnit = "Oil Stret";
                        address.Zip = "75220";
                        address.City = "Dallas";
                        address.State = "Texas";
                        address.PhoneNumber = "(972) 591-5250";
                        break;
                    }
                case AddressType.OregonAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "United States";
                        address.AddressStreet = "forest ducks";
                        address.AddressUnit = "state football";
                        address.Zip = "97391";
                        address.City = "Toledo";
                        address.State = "Oregon";
                        address.PhoneNumber = "(503) 299-8989";
                        break;
                    }
                case AddressType.MaineAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "United States";
                        address.AddressStreet = "arcadia national park";
                        address.AddressUnit = "portland museum of art";
                        address.Zip = "04072";
                        address.City = "Saco";
                        address.State = "Maine";
                        address.PhoneNumber = "(207) 299-8989";
                        break;
                    }
                case AddressType.CanadaAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "Canada";
                        address.AddressStreet = "Yukon Whitehorse";
                        address.AddressUnit = "Tombstone Territory Park";
                        address.Zip = "G1A1C5";
                        address.City = "Quebec City";
                        address.State = "Quebec";
                        address.PhoneNumber = "418-555-0199";
                        break;
                    }
                case AddressType.PennsylvaniaAddress:
                    {
                        address.DefaultAddress = false;
                        address.Country = "United States";
                        address.AddressStreet = "Bushkill Falls";
                        address.AddressUnit = "The Amish Village";
                        address.Zip = "15001";
                        address.City = "Aliquippa";
                        address.State = "Pennsylvania";
                        address.PhoneNumber = "724-375-5179";
                        break;
                    }
            }
            return address;
        }

        public enum AddressType
        {
            CanadaAddress,
            USAAddress,
            AustraliaAddress,
            CaliforniaAddress,
            HawaiiAddress,
            AlaskaAddress,
            TexasAddress,
            OregonAddress,
            MaineAddress,
            PennsylvaniaAddress
        }
    }
}
