﻿using QA_AccessConsciousnessShop.Objects;


namespace QA_AccessConsciousnessShop.Factories
{
	class ClassFactory
	{
		public Classes GetClass(ClassType classType)
		{
			Classes classEntity = new Classes();
			switch (classType)
			{
				case ClassType.Occurrence:
					{
						classEntity.ClassGroup = "";
						classEntity.ClassType = "";
						classEntity.Medium = "";
						classEntity.Language = "";
					}
					break;
				case ClassType.Event:
					{
						classEntity.ClassGroup = "";
						classEntity.ClassType = "";
						classEntity.Medium = "";
						classEntity.Language = "";
					}
					break;
				case ClassType.Teleseries:
					{
						classEntity.ClassGroup = "";
						classEntity.ClassType = "";
						classEntity.Medium = "";
						classEntity.Language = "";
					}
					break;
				default:
					break;
			}
			return classEntity;
		}

		internal enum ClassType
		{
			Occurrence,
			Event,
			Teleseries,
			DoubleProcessClass
		}
	}
}
