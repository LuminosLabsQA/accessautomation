﻿using SeleniumCommon.Objects.User.CreditCard;

namespace QA_AccessConsciousnessShop.Factories
{
    class CreditCardFactory
    {
        public CreditCard GetCard(CardType cardType)
        {
            CreditCard Card = new CreditCard();
            //EnumUtils.StringValueOf(CreditCardNumber.CardNumber.Visa);
            switch (cardType)
            {
                case CardType.VISA:
                    {
                        Card.CardNumber = CreditCardNumber.CardNumber.Visa;
                        Card.Type = CreditCardType.CardType.Visa;
                        Card.Cvv = CreditCardCvv.Cvv.VisaCVV;
                        Card.ExpirationMonth = CreditCardExpirationMonth.ExpirationMonth.January;
                        Card.ExpirationYear = "2020";
                        break;
                    }
                case CardType.MasterCard:
                    {
                        Card.CardNumber = CreditCardNumber.CardNumber.Mastercard;
                        Card.Type = CreditCardType.CardType.MasterCard;
                        Card.Cvv = CreditCardCvv.Cvv.VisaCVV;
                        Card.ExpirationMonth = CreditCardExpirationMonth.ExpirationMonth.December;
                        Card.ExpirationYear = "2021";
                        break;
                    }
                case CardType.Discover:
                    {
                        Card.CardNumber = CreditCardNumber.CardNumber.Discover;
                        Card.Type = CreditCardType.CardType.Discover;
                        Card.Cvv = CreditCardCvv.Cvv.VisaCVV;
                        Card.ExpirationMonth = CreditCardExpirationMonth.ExpirationMonth.December;
                        Card.ExpirationYear = "2022";
                        break;
                    }
                case CardType.AMEX:
                    {
                        Card.CardNumber = CreditCardNumber.CardNumber.AmEx;
                        Card.Type = CreditCardType.CardType.AmericanExpress;
                        Card.Cvv = CreditCardCvv.Cvv.AmexCVV;
                        Card.ExpirationMonth = CreditCardExpirationMonth.ExpirationMonth.December;
                        Card.ExpirationYear = "2023";
                        break;
                    }
            }
            return Card;
        }

        public enum CardType
        {
            VISA,
            MasterCard,
            Discover,
            AMEX,
        }
    }
}
