﻿//using QA_AccessConsciousnessShop.Objects;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace QA_AccessConsciousnessShop.Factories
//{
//    class OccurrenceBuilder
//    {
//        private IOccurrence _occurrence;

//        public IOccurrence GetOccurrence(OccurrenceType type)
//        {
//            Occurrence _occurrence = new Occurrence();
//            switch (type)
//            {
//                case OccurrenceType.ClassOccurrence:
//                    CreateOccurrence(OccurrenceFactory.OccurrenceType.ClassOccurrence);
//                    break;
//                case OccurrenceType.Event:
//                    CreateOccurrence(OccurrenceFactory.OccurrenceType.Event);
//                    break;
//                case OccurrenceType.Teleseries:
//                    CreateOccurrence(OccurrenceFactory.OccurrenceType.Teleseries);
//                    break;
//            }
//            return _occurrence;
//        }

//        internal enum OccurrenceType
//        {
//            ClassOccurrence,
//            Event,
//            Teleseries
//        }

//        private void CreateOccurrence(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            SetClassGroup(occurrenceType);
//            SetClassTitle(occurrenceType);
//            SetClassMedium(occurrenceType);
//            SetClassLanguage(occurrenceType);
//            SetClassTimeZone(occurrenceType);
//            SetClassStartDate(occurrenceType);
//            SetClassEndDate(occurrenceType);
//            SetClassPrice(occurrenceType);
//            SetClassCurrency(occurrenceType);
//            SetClassVenueName(occurrenceType);
//            SetClassAddressLine1(occurrenceType);
//            SetClassAddressLine2(occurrenceType);
//            SetClassCountry(occurrenceType);
//            SetClassState(occurrenceType);
//            SetClassCity(occurrenceType);
//            SetClassPaymentCompany(occurrenceType);
//        }

//        private void SetClassMedium(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.ClassMedium = occurrence.ClassMedium;
//        }

//        private void SetClassTitle(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.ClassTitle = occurrence.ClassTitle;
//        }

//        private void SetClassGroup(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.ClassGroup = occurrence.ClassGroup;
//        }

//        private void SetClassLanguage(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.Language = occurrence.Language;
//        }

//        private void SetClassTimeZone(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.TimeZone = occurrence.TimeZone;
//        }

//        private void SetClassStartDate(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.StartDate = occurrence.StartDate;
//        }

//        private void SetClassEndDate(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.EndDate = occurrence.EndDate;
//        }

//        private void SetClassPrice(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.ClassPrice = occurrence.ClassPrice;
//        }

//        private void SetClassCurrency(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.Currency = occurrence.Currency;
//        }

//        private void SetClassVenueName(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.VenueName = occurrence.VenueName;
//        }

//        private void SetClassAddressLine1(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.AddressLine1 = occurrence.AddressLine1;
//        }

//        private void SetClassAddressLine2(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.AddressLine2 = occurrence.AddressLine2;
//        }

//        private void SetClassCountry(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.Country = occurrence.Country;
//        }

//        private void SetClassState(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.State = occurrence.State;
//        }

//        private void SetClassCity(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.City = occurrence.City;
//        }

//        private void SetClassPaymentCompany(OccurrenceFactory.OccurrenceType occurrenceType)
//        {
//            Occurrence occurrence = new OccurrenceFactory().GetOccurrence(occurrenceType);
//            _occurrence.PaymentCompany = occurrence.PaymentCompany;
//        }
//    } 
//}
