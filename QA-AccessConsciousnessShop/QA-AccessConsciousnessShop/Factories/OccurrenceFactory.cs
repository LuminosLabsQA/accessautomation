﻿using QA_AccessConsciousnessShop.Objects;
using QA_AccessConsciousnessShop.Objects.OccurrenceInstances;
using SeleniumCommon.ConfigManager;
using System;

namespace QA_AccessConsciousnessShop.Factories
{
    class OccurrenceFactory
    {
        readonly DateTime currentDate = DateTime.Today;

        public IOccurrence GetOccurrence(OccurrenceType occurrenceType)
        {
            ClassOccurrence classOccurrence = new ClassOccurrence();
            EventOccurrence eventOccurrence = new EventOccurrence();
            TeleseriesOccurrence teleseriesOccurrence = new TeleseriesOccurrence();

            switch (occurrenceType)
            {
                case OccurrenceType.ClassOccurrence:
                    {
                        classOccurrence.ClassGroup = "Special Topics";
                        classOccurrence.ClassTitle = TestConfig.ClassName;
                        classOccurrence.ClassMedium = "Live";
                        classOccurrence.Language = "English";
                        classOccurrence.TimeZone = "(UTC+02:00) Athens, Bucharest";
                        classOccurrence.StartDate = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        classOccurrence.EndDate = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        classOccurrence.ClassPrice = 25000;
                        classOccurrence.Currency = "EUR";
                        classOccurrence.VenueName = "Occurrence Test Automation VN";
                        classOccurrence.AddressLine1 = "This is the 1st automation address";
                        classOccurrence.AddressLine2 = "This is the 2nd automation address";
                        classOccurrence.Country = "Romania";
                        classOccurrence.City = "Cluj-Napoca";
                        classOccurrence.PaymentCompany = "Access Consciousness International (Ireland)";
                        classOccurrence.Discount = 0.4;
                        return classOccurrence;
                    }
                case OccurrenceType.FacilitatorClassOccurrence:
                    {
                        classOccurrence.ClassGroup = "Core Classes";
                        classOccurrence.ClassTitle = TestConfig.FacilitatorClass;
                        classOccurrence.ClassMedium = "Live";
                        classOccurrence.Language = "English";
                        classOccurrence.ClassPrice = 7000;
                        classOccurrence.TimeZone = "(UTC+02:00) Athens, Bucharest";
                        classOccurrence.StartDate = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        classOccurrence.EndDate = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        classOccurrence.VenueName = "Facilitator Test Automation VN";
                        classOccurrence.AddressLine1 = "This is the 1st automation address";
                        classOccurrence.AddressLine2 = "This is the 2nd automation address";
                        classOccurrence.Country = "Romania";
                        classOccurrence.City = "Cluj-Napoca";
                        classOccurrence.PaymentCompany = "Access Consciousness International (Ireland)";
                        classOccurrence.Discount = 0.4; //Location Discount for Facilitator Class
                        classOccurrence.SecondDiscount = 0.1;
                        return classOccurrence;
                    }
                case OccurrenceType.Event:
                    {
                        eventOccurrence.ClassGroup = "Events";
                        eventOccurrence.ClassTitle = TestConfig.EventName;
                        eventOccurrence.ClassMedium = "Online - Access TV";
                        eventOccurrence.Language = "Romanian";
                        eventOccurrence.TimeZone = "(UTC+02:00) Athens, Bucharest";
                        eventOccurrence.StartDate = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        eventOccurrence.ClassPrice = 25000;
                        eventOccurrence.Currency = "USD";
                        eventOccurrence.VenueName = "Event Test Automation VN";
                        eventOccurrence.AddressLine1 = "This is the 1st automation address";
                        eventOccurrence.AddressLine2 = "This is the 2nd automation address";
                        eventOccurrence.Country = "United States";
                        eventOccurrence.State = "Arkansas";
                        eventOccurrence.City = "Cluj-Napoca";
                        eventOccurrence.PaymentCompany = "Access Consciousness (USA)";
                        eventOccurrence.Discount = 0.5;
                        return eventOccurrence;
                    }
                case OccurrenceType.Teleseries:
                    {
                        teleseriesOccurrence.ClassGroup = "Telecall Series";
                        teleseriesOccurrence.ClassTitle = TestConfig.TeleseriesName;
                        teleseriesOccurrence.ClassMedium = "Online - Access TV";
                        teleseriesOccurrence.Language = "Romanian";
                        teleseriesOccurrence.NumberOfCalls = "2";
                        teleseriesOccurrence.NumberOfCallsToDisplay = "2";
                        teleseriesOccurrence.TimeZone = "(UTC+02:00) Athens, Bucharest";
                        teleseriesOccurrence.TelecallOne = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        teleseriesOccurrence.TelecallTwo = currentDate.AddYears(1).ToString("yyyy-MM-dd");
                        teleseriesOccurrence.ClassPrice = 25000;
                        teleseriesOccurrence.Currency = "AUD";
                        teleseriesOccurrence.PaymentCompany = "Access Seminars Australia";
                        teleseriesOccurrence.Discount = 0.1;
                        return teleseriesOccurrence;
                    }
            }
            return null;
        }

        public enum OccurrenceType
        {
            ClassOccurrence,
            Event,
            Teleseries,
            FacilitatorClassOccurrence
        }
    }
}
