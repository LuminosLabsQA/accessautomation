﻿using OpenQA.Selenium;
using QA_AccessConsciousnessShop.Pages;
using QA_AccessConsciousnessShop.Pages.ClassCalendarPage;
using QA_AccessConsciousnessShop.Pages.LandingPage;
using QA_AccessConsciousnessShop.Pages.LoginPage;
using QA_AccessConsciousnessShop.Pages.ProductPage;
using SeleniumCommon.ConfigManager;

namespace QA_AccessConsciousnessShop.Factories
{
    class PageFactory
    {
        public ILandingPage GetLandingPage(IWebDriver driver)
        {
            if (TestConfig.Desktop) return new DesktopLandingPage(driver);
            else return null;
            //else return new MobileLandingPage(driver);
        }

        public IProductPage GetProductPage(IWebDriver driver)
        {
            if (TestConfig.Desktop) return new DesktopProductPage(driver);
            else return null;
            //else return new MobileProductPage(driver);
        }

        public ILoginPage GetLoginPage(IWebDriver driver)
        {
            if (TestConfig.Desktop) return new DesktopLoginPage(driver);
            else return null;
            //else return new MobileProductPage(driver);
        }

        public IClassCalendar GetClassCalendarPage(IWebDriver driver)
        {
            if (TestConfig.Desktop) return new DesktopClassCalendar(driver);
            else return null;
        }
    }
}
