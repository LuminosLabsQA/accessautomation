﻿using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.Product;

namespace QA_AccessConsciousnessShop.Factories
{
    class ProductTypeFactory
    {
        public Products GetProduct(ProductType type)
        {

            Products _product = new Products();
            switch (type)
            {
                case ProductType.PublicProfile:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/public-profile/public-profile/";
                        break;
                    }
                case ProductType.Membership:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/membership/the-creative-edge-of-consciousness-club/";
                        break;
                    }
                case ProductType.Certification:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/certifications/bars-facilitator-certification/";
                        break;
                    }
                case ProductType.AlternateDigitalProductOnSale:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/past-teleclasses/business-time-nov-17-teleseries-2/";
                        break;
                    }
                case ProductType.AlternateDigitalProductOnSaleAndCountryPrice:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/classes/a-taste-of-having-ease-with-money-nov-17-noosa/";
                        break;
                    }
                case ProductType.RegularPhysicalProductRetail:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/book/the-home-of-infinite-possibilities-621c2383/";
                        break;
                    }
                case ProductType.RegularPhysicalProductCountryPrice:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/book/the-adventures-of-smudge--friends/";
                        break;
                    }
                case ProductType.RegularPhysicalProductWholeSale:
                    {
                        _product.Url = "https://access.crm.techromix.com/en/shop-catalog/book/getting-out-of-debt-joyfully/";
                        break;
                    }
            }
            return _product;
        }
    }

    internal enum ProductType
    {
        PublicProfile,
        Membership,
        Certification,
        AlternateDigitalProductOnSale,
        AlternateDigitalProductOnSaleAndCountryPrice,
        RegularPhysicalProductRetail,
        RegularPhysicalProductCountryPrice,
        RegularPhysicalProductWholeSale,
    }
}
