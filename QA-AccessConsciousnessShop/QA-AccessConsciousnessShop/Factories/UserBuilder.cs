﻿using System;
using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;

namespace QA_AccessConsciousnessShop.Factories
{
    class UserBuilder
    {
        private IAccessUsers _user;
        private readonly Random _random = new Random();

        public IAccessUsers GetUser(UserTypes type)
        {
            _user = new AccessUsers();

            switch (type)
            {
                case UserTypes.NewRandomUser:
                {
                    CreateNewRandomUser();
                    break;
                }
                case UserTypes.ExistingRegularUserEurope:
                {
                        CreateExistingUser(UserRoleFactory.UserType.ExistingRegularUserEurope);
                    break;
                }
                case UserTypes.ExistingRegularUserUSA:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.ExistingRegularUserUSA);
                        break;
                    }
                case UserTypes.ExistingRegularUserAUS:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.ExistingRegularUserAUS);
                        break;
                    }
                case UserTypes.ExistingPayPalUser:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.ExistingPayPalUser);
                        break;
                    }
                case UserTypes.ExistingAdminUser:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.AdminUser);
                        break;
                    }
                case UserTypes.ExistingFacilitatorUser:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.ExistingFacilitatorUser);
                        break;
                    }
                case UserTypes.ExistingForgotPasswordUser1:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.RegistrationUser);
                        break;
                    }
                case UserTypes.ExistingForgotPasswordUser2:
                    {
                        ForgotPasswordAlternateFlow(UserRoleFactory.UserType.ForgotPasswordUser2);
                        break;
                    }
                case UserTypes.UserThatDoesNotExist:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.UserThatDoesNotExist);
                        break;
                    }
                case UserTypes.UserWithDependents:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.UserWithDependents);
                        break;
                    }
                case UserTypes.RegistrationUser:
                    {
                        CreateExistingUser(UserRoleFactory.UserType.RegistrationUser);
                        break;
                    }
            }
            return _user;
        }

        private void CreateNewRandomUser()
        {
            SetNewRandomUserName();
            SetNewRandomUserFirstName();
            SetNewRandomUserLastName();
            SetNewRandomPassword();
            _user.PhoneField = "+4233524234324";
            _user.HomePhone = "+423523424234";
            _user.MobilePhone = "+5345345435353";
            _user.Currency = "EUR";
            _user.ConfirmationPassword = _user.Password;
            _user.Email = _user.UserName + "@gmail.com";
            _user.Addresses.Add(new AddressFactory().GetAddress(AddressFactory.AddressType.CaliforniaAddress));
        }

        private void SetNewRandomPassword()
        {
            _user.Password = GeneratRandomString(_random, 8);
        }

        private void SetNewRandomUserLastName()
        {
            _user.LastName = GeneratRandomString(_random, 8);

        }

        private void SetNewRandomUserFirstName()
        {
            _user.FirstName = GeneratRandomString(_random, 8);
        }

        private void SetNewRandomUserName()
        {         
            _user.UserName = GeneratRandomString(_random, 8);
        }

        internal enum UserTypes
        {
            NewRandomUser,
            ExistingRegularUserEurope,
            ExistingRegularUserUSA,
            ExistingRegularUserAUS,
            ExistingPayPalUser,
            ExistingAdminUser,
            ExistingFacilitatorUser,
            ExistingForgotPasswordUser1,
            ExistingForgotPasswordUser2,
            UserThatDoesNotExist,
            UserWithDependents,
            RegistrationUser
        }

        private static string GeneratRandomString(Random random, int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var stringChars = new char[length];

            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new string(stringChars);
            return finalString;
        }

        private void CreateExistingUser(UserRoleFactory.UserType userType)
        {
            SetExistingRegularUserUserName(userType);
            SetExistingRegularUserPassword(userType);
            SetExistingRegularUserFirstName(userType);
            SetExistingRegularUserLastName(userType);
            SetExistingRegularUserEmail(userType);
            SetExistingUserPhone(userType);
            SetExistingUserHomePhone(userType);
            SetExistingUserMobilePhone(userType);
            SetExistingUserCurrency(userType);
        }

        private void SetExistingRegularUserPassword(UserRoleFactory.UserType userType)
        {
            IAccessUsers user = new UserRoleFactory().GetUsers(userType);
            _user.Password = user.Password;
        }

        private void SetExistingRegularUserUserName(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.UserName = user.UserName;
        }
        private void SetExistingRegularUserFirstName(UserRoleFactory.UserType userType)
        {
            IAccessUsers user = new UserRoleFactory().GetUsers(userType);
            _user.FirstName = user.FirstName;
        }
        private void SetExistingRegularUserLastName(UserRoleFactory.UserType userType)
        {
            IAccessUsers user = new UserRoleFactory().GetUsers(userType);
            _user.LastName = user.LastName;
        }
        private void SetExistingRegularUserEmail(UserRoleFactory.UserType userType)
        {
            IAccessUsers user = new UserRoleFactory().GetUsers(userType);
            _user.Email = user.Email;
        }
        private void ForgotPasswordAlternateFlow(UserRoleFactory.UserType userType)
        {
            SetExistingUserFirstName(userType);
            SetExistingUserEmail(userType);
        }

        private void SetExistingUserFirstName(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.FirstName = user.FirstName;
        }

        private void SetExistingUserEmail(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.Email = user.Email;
        }

        private void SetExistingUserPhone(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.PhoneField = user.PhoneField;
        }

        private void SetExistingUserHomePhone(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.HomePhone = user.HomePhone;
        }

        private void SetExistingUserMobilePhone(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.MobilePhone = user.MobilePhone;
        }

        private void SetExistingUserCurrency(UserRoleFactory.UserType userType)
        {
            var user = new UserRoleFactory().GetUsers(userType);
            _user.Currency = user.Currency;
        }
    }
}
