﻿using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;


namespace QA_AccessConsciousnessShop.Factories
{
    class UserRoleFactory
    {
        public IAccessUsers GetUsers(UserType userType)
        {
            IAccessUsers user = new AccessUsers();
            switch (userType)
            {
                case UserType.ExistingRegularUserEurope:
                    {
                        user.UserName = "AutomationEurope";
                        user.Password = "store";
                        user.FirstName = "Automation";
                        user.LastName = "Europe";
                        user.Email = "automationeurope@mail.com";
                        user.PhoneField = "+40435343423434";
                        user.HomePhone = "+5345345345435";
                        user.MobilePhone = "+534535345435";
                        user.Currency = "EUR";
                        break;
                    }
                case UserType.ExistingRegularUserUSA:
                    {
                        user.UserName = "AutomationUSA";
                        user.Password = "store";
                        user.FirstName = "Automation";
                        user.LastName = "USA";
                        user.Email = "automationusa@mail.com";
                        break;
                    }
                case UserType.ExistingRegularUserAUS:
                    {
                        user.UserName = "AutomationAustralia";
                        user.Password = "store";
                        user.FirstName = "Automation";
                        user.LastName = "Australia";
                        user.Email = "automationaustralia@mail.com";
                        break;
                    }
                case UserType.ExistingPayPalUser:
                    {
                        user.UserName = "AutomationPayPalUserr";
                        user.Password = "Techromix15";
                        user.Email = "millionaire@techromix.com";
                        user.FirstName = "Automation";
                        user.LastName = "PayPal";
                        break;
                    }
                case UserType.ExistingFacilitatorUser:
                    {
                        user.UserName = "SeleniumFacilitator";
                        user.Password = "store";
                        user.FirstName = "Selenium";
                        user.LastName = "Facilitator";
                        user.Email = "seleniumfacilitator@mail.com";
                        break;
                    }
                case UserType.AdminUser:
                    {
                        user.UserName = "AutomationAdmin";
                        user.Password = "store";
                        user.FirstName = "Automation";
                        user.LastName = "Admin";
                        break;
                    }
                case UserType.RegistrationUser:
                    {
                        user.UserName = "AutomationRegistration";
                        user.Password = "store";
                        break;
                    }
                case UserType.ForgotPasswordUser2:
                    {
                        user.FirstName = "Automation";
                        user.Email = "AutomationForgotPassword@mailinator.com";
                        break;
                    }
                case UserType.UserThatDoesNotExist:
                    {
                        user.UserName = "UserThatDoesNotExist";
                        user.Password = "store";
                        break;
                    }
                case UserType.UserWithDependents:
                    {
                        user.UserName = "DependentsAutomation";
                        user.Password = "store";
                        break;
                    }
            }
            return user;
        }

        public enum UserType
        {
            RegularUser,
            ExistingFacilitatorUser,
            AdminUser,
            RegistrationUser,
            ForgotPasswordUser2,
            ExistingRegularUserEurope,
            ExistingRegularUserUSA,
            ExistingRegularUserAUS,
            ExistingPayPalUser,
            UserThatDoesNotExist,
            UserWithDependents,
        }
    }

}

