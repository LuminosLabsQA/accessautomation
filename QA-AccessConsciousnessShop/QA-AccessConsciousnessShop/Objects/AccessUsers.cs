﻿using SeleniumCommon.Objects.User;

namespace QA_AccessConsciousnessShop.Objects
{
    public class AccessUsers : User, IAccessUsers
    {
        public string PhoneField { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Currency { get; set; }
    }
}
