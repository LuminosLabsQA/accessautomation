﻿using SeleniumCommon.Objects.User;

namespace QA_AccessConsciousnessShop.Objects
{
    public interface IAccessUsers : IUser
    {
        string PhoneField { get; set; }
        string HomePhone { get; set; }
        string MobilePhone { get; set; }
        string Currency { get; set; }
    }
}
