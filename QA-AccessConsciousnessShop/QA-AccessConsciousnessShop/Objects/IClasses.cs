﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Objects
{
	interface IClasses
	{
		string ClassGroup { get; set; }
		string ClassType { get; set; }
		string Medium { get; set; }
		string Language { get; set; }
		string Facilitators { get; set; }
		string Host { get; set; }
		string TimeZone { get; set; }
		DateTime StartDate { get; set; }
		DateTime EndDate { get; set; }
		string Country { get; set; }
		string City { get; set; }
		string PaymentCompany { get; set; }
	}
}
