﻿using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass;

namespace QA_AccessConsciousnessShop.Objects
{
    public interface IOccurrence
    {
        void CreateOccurrence(ICreateAClassPage createAClass);
        void CreateFacilitatorOccurrence(ICreateAClassPage createAClass);
        double Discount { get; set; }
        double SecondDiscount { get; set; }
        double ClassPrice { get; set; }
    }
}
