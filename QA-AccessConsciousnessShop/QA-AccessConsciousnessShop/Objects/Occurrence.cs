﻿using QA_AccessConsciousnessShop.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Objects
{
    public abstract class Occurrence
    {
        public string ClassGroup { get; set; }
        public string ClassTitle { get; set; }
        public string ClassMedium { get; set; }
        public string Language { get; set; }
        public string TimeZone { get; set; }
        public double ClassPrice { get; set; }
        public double Discount { get; set; }
        public double SecondDiscount { get; set; }
        public string Currency { get; set; }
        public string PaymentCompany { get; set; }
    }
}
