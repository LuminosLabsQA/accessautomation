﻿using QA_AccessConsciousnessShop.Pages.MyAccountPage;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Objects.OccurrenceInstances
{
    public class ClassOccurrence : Occurrence, IOccurrence
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string VenueName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        public void CreateOccurrence(ICreateAClassPage createAClass)
        {
            createAClass.SetClassGroup(ClassGroup);
            createAClass.SetClassTitle(ClassTitle);
            createAClass.SetClassMedium(ClassMedium);
            createAClass.SetClassLanguage(Language);
            createAClass.SetClassTimeZone(TimeZone);
            createAClass.SetClassStartDate(StartDate);
            createAClass.SetClassEndDate(EndDate);
            createAClass.SetClassPrice(ClassPrice.ToString());
            createAClass.SetClassCurrency(Currency);
            createAClass.SelectCountryDiscount();
            createAClass.SetClassAddress(VenueName, AddressLine1, AddressLine2, Country, City);
            createAClass.SetClassPaymentCompany(PaymentCompany);
        }

        public void CreateFacilitatorOccurrence(ICreateAClassPage createAClass)
        {
            createAClass.SetClassGroup(ClassGroup);
            createAClass.SetClassTitle(ClassTitle);
            createAClass.SetClassLanguage(Language);
            createAClass.SetClassTimeZone(TimeZone);
            createAClass.SetClassStartDate(StartDate);
            createAClass.SetClassEndDate(EndDate);
            createAClass.SetClassAddress(VenueName, AddressLine1, AddressLine2, Country, City);
        }
    }
}
