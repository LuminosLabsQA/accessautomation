﻿using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass;

namespace QA_AccessConsciousnessShop.Objects.OccurrenceInstances
{
    class EventOccurrence : Occurrence, IOccurrence
    {
        public string StartDate { get; set; }
        public string VenueName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        public void CreateFacilitatorOccurrence(ICreateAClassPage createAClass)
        {
            throw new System.NotImplementedException();
        }

        public void CreateOccurrence(ICreateAClassPage createEvent)
        {
            createEvent.SetClassGroup(ClassGroup);
            createEvent.SetClassTitle(ClassTitle);
            createEvent.SetClassMedium(ClassMedium);
            createEvent.SetClassLanguage(Language);
            createEvent.SetClassTimeZone(TimeZone);
            createEvent.SetClassStartDate(StartDate);
            createEvent.SetClassPrice(ClassPrice.ToString());
            createEvent.SetClassCurrency(Currency);
            createEvent.SelectRepeatDiscount();
            createEvent.SetClassAddress(VenueName, AddressLine1, AddressLine2, Country, City);
            createEvent.SetClassPaymentCompany(PaymentCompany);
        }
    }
}
