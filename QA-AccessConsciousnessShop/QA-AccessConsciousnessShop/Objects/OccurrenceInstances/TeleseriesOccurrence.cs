﻿using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass;

namespace QA_AccessConsciousnessShop.Objects.OccurrenceInstances
{
    class TeleseriesOccurrence : Occurrence, IOccurrence
    {
        public string NumberOfCalls { get; set; }
        public string NumberOfCallsToDisplay { get; set; }
        public string TelecallOne { get; set; }
        public string TelecallTwo { get; set; }

        public void CreateFacilitatorOccurrence(ICreateAClassPage createAClass)
        {
            throw new System.NotImplementedException();
        }

        public void CreateOccurrence(ICreateAClassPage createTeleseries)
        {
            createTeleseries.SetClassGroup(ClassGroup);
            createTeleseries.SetClassTitle(ClassTitle);
            createTeleseries.SetClassMedium(ClassMedium);
            createTeleseries.SetClassLanguage(Language);
            createTeleseries.SetTelecallNumbersOfCalls(NumberOfCalls, NumberOfCallsToDisplay);
            createTeleseries.SetClassTimeZone(TimeZone);
            createTeleseries.SetTelecallDates(TelecallOne, TelecallTwo);
            createTeleseries.SetClassPrice(ClassPrice.ToString());
            createTeleseries.SetClassCurrency(Currency);
            createTeleseries.SelectAgeDiscount();
            createTeleseries.SetClassPaymentCompany(PaymentCompany);
        }
    }
}
