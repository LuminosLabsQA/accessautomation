﻿using System;
using OpenQA.Selenium;
using SeleniumCommon.Objects.Product;

namespace QA_AccessConsciousnessShop.Objects
{
    public class Products : IProduct
    {
        public string Category
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string Id
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string Model
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public float Price
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Quantity
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string StockStatus
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string Url { get; set; }

        public void GetProductDetials(IWebDriver driver)
        {
            throw new NotImplementedException();
        }

        public bool IsSelectionValidated(IWebDriver driver)
        {
            throw new NotImplementedException();
        }

        public void SelectProductAttributes(IWebDriver driver)
        {
            throw new NotImplementedException();
        }

        public void SelectRandomProductAttributes(IWebDriver driver)
        {
            throw new NotImplementedException();
        }
    }
}
