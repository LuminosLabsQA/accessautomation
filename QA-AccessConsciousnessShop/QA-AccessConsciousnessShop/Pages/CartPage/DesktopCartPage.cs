﻿using System;
using OpenQA.Selenium;
using SeleniumCommon.Objects.Product;
using System.Threading;
using SeleniumCommon.Objects.User.CreditCard;
using SeleniumCommon.Objects.User;
using SeleniumCommon.Objects.User.Address;
using SeleniumCommon.Utilities;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using SeleniumExtras.PageObjects;
using QA_AccessConsciousnessShop.Objects;

namespace QA_AccessConsciousnessShop.Pages.CartPage
{
    class DesktopCartPage : SeleniumCommon.Page.CartPage.CartPage, ICartPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/access-shop/shopping-cart/']")]
        private IWebElement CartButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='removeItem(cartItem)']")]
        private IWebElement RemoveItemButton { get; set; }
     
        [FindsBy(How = How.XPath, Using = "//a[@class='btn btn-green btn-lg btn-block marbottom30']")]
        private IWebElement CheckoutButton { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressFirstName")]
        private IWebElement BillingFirstNameField { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressLastName")]
        private IWebElement BillingLastNameField { get; set; }

        [FindsBy(How = How.Id, Using = "billingEmail")]
        private IWebElement BillingEmailField { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressLine1")]
        private IWebElement BillingAddressLine1 { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressCity")]
        private IWebElement BillingCityField { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressZip")]
        private IWebElement BillingZipCodeField { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressCountry")]
        private IWebElement BillingCountryField { get; set; }

        [FindsBy(How = How.Id, Using = "billingAddressState")]
        private IWebElement BillingStateField { get; set; }

        [FindsBy(How = How.Id, Using = "shippingAddressLine1")]
        private IWebElement BillingShippingAddressField1 { get; set; }

        [FindsBy(How = How.Id, Using = "shippingAddressCity")]
        private IWebElement BillingShippingCityField { get; set; }

        [FindsBy(How = How.Id, Using = "shippingAddressZip")]
        private IWebElement BillingShippingZipCode { get; set; }

        [FindsBy(How = How.Id, Using = "shippingAddressCountry")]
        private IWebElement BillingShippingCountryField { get; set; }

        [FindsBy(How = How.Id, Using = "shippingAddressState")]
        private IWebElement BillingShippingStateField { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//div[@ng-show='checkoutViewModel.currentStep.isAddressStep']//button[@type='submit']")]
        private IWebElement BillingContinueButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@ng-show='checkoutViewModel.currentStep.isShippingStep']//button[@type='submit']")]
        private IWebElement ShippingContinueButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@ng-show='checkoutViewModel.currentStep.isShippingStep']//input[@type='email']")]
        private IWebElement ShippingEmailField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='phone']")]
        private IWebElement ShippingPhoneField { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@ng-show='checkoutViewModel.currentStep.isPaymentStep && showPlaceOrderButtonWhen()']//button[@type='submit']")]
        private IWebElement PlaceOrderButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//label[@ng-show='checkoutViewModel.shoppingCart.DisplayCheckoutPayPalOption']")]
        private IWebElement PayPalRadioButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='continueToPayPal()']")]
        private IWebElement ContinueToPayPalButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='ccFirstName']")]
        private IWebElement CCFirstNameField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='ccLastName']")]
        private IWebElement CCLastNameField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='ccNumber']")]
        private IWebElement CCNumberField { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@name='selectCC']")]
        private IWebElement SelectCCField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='ccCVV']")]
        private IWebElement CVVField { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@name='ccExpirationMonth']")]
        private IWebElement ExpirationMonth { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@name='ccExpirationYear']")]
        private IWebElement ExpirationYear { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='block bordered pad15 col-sm-6 text-center']")]
        private IWebElement CheckoutSuccessMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//strong[@class='ng-binding']")]
        private IWebElement CartProductPrice { get; set; }

        [FindsBy(How = How.Id, Using = "email")]
        private IWebElement PayPalEmailField { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        private IWebElement PayPalPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        private IWebElement PayPalLoginButton { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        private IWebElement PayPalContinuedCheckoutButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='logWebviewLoginClickWrapper(); redirect()']")]
        private IWebElement PayPalAlreadyHaveAnAccountButton { get; set; }

        [FindsBy(How = How.Id, Using = "btnNext")]
        private IWebElement PayPalNextButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='button']")]
        private IWebElement PayPalContinuedButton { get; set; }

        //div[@id='button']//input[@type='submit']

        [FindsBy(How = How.Id, Using = "preloaderSpinner")]
        private IWebElement PayPalPreloaderSpinner { get; set; }

        [FindsBy(How = How.Id, Using = "//h6[@ng-show='isLoadingEnabled']")]
        private IWebElement CheckoutLoadingModal { get; set; }

        [FindsBy(How = How.Id, Using = "//h6[@ng-show='isLoadingEnabled']")]
        private IWebElement CartSubtotal { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@ng-show='checkoutViewModel.totals']//strong[@class='ng-binding']")]
        private IWebElement CheckoutSubtotal { get; set; }

        public DesktopCartPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public override void CheckoutNow()
        {
            WaitForVisibleElement(_driver, CheckoutButton);
            CheckoutButton.Click();
        }

        public void ClickRemoveItemButton()
        {
            WaitForPageToLoad();
            RemoveItemButton.Click();
            WaitForPageToLoad();
        }

        public float GetCartButtonCount()
        {
            var charArray = new char[2] { '(', ')' };
            Thread.Sleep(2000);
            float cartCount = Utilities.AccessUtilities.ExtractPriceValueFromString(CartButton, charArray, 1);
            return cartCount;
        }

        public float GetCartProductPrice()
        {         
            var charArray = new char[2] { '$', ' ' };
            Thread.Sleep(2000);
            float cartPrice = Utilities.AccessUtilities.ExtractPriceValueFromString(CartProductPrice, charArray, 1);
            return cartPrice;
        }

        public float GetCheckoutPrice()
        {
            var charArray = new char[2] { '$', ' ' };
            Thread.Sleep(2000);
            float checkoutPrice = Utilities.AccessUtilities.ExtractPriceValueFromString(CheckoutSubtotal, charArray, 1);
            return checkoutPrice;
        }

        //create reusable method to get all product prices!!!

        public override void GoToCart()
        {
            Thread.Sleep(4000);
            CartButton.Click();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }

        public override SeleniumCommon.Page.CartPage.ICartPage ModifyProductQuantity(IProduct product, int newQuant)
        {
            throw new NotImplementedException();
        }

        public override SeleniumCommon.Page.CartPage.ICartPage MoveProductToWishList(IProduct product)
        {
            throw new NotImplementedException();
        }

        public override SeleniumCommon.Page.CartPage.ICartPage RemoveProductFromCart(IProduct product)
        {
            throw new NotImplementedException();
        }

        public DesktopCartPage ClickBillingContinueButton()
        {
            WaitForVisibleElement(_driver, BillingContinueButton);
            BillingContinueButton.Click();
            return this;
        }

        public void ClickShippingContinueButton()
        {
            Thread.Sleep(1000);
            WaitForVisibleElement(_driver, ShippingContinueButton);
            ShippingContinueButton.Click();
        }

        public void ClickPlaceOrderButton()
        {
            WaitForInvisibilityOfElement(_driver, PayPalPreloaderSpinner);
            WaitForVisibleElement(_driver, PlaceOrderButton);
            PlaceOrderButton.Click();
        }

        public void EnterBillingInformationAndContinue(IAccessUsers user, IAddress address)
        {
            SetBillingFirstName(user.FirstName);
            SetBillingLastName(user.LastName);
            SetBillingEmailField(user.Email);
            SetBillingAddressField(address.AddressStreet);
            SetBillingCityField(address.City);
            SetBillingZipCodeField(address.Zip);
            SetBillingCountry(address.Country);
            SetBillingState(address.State);
            if(IsBillingShippingAddressSectionDisplayed() == true)
            {
                SetBillingShippingAddressField(address.AddressStreet);
                SetBillingShippingCityField(address.City);
                SetBillingShippingZipCodeField(address.State);
                SetBillingShippingCountryField(address.Country);
                SetBillingShippingStateField(address.State);
            }
            ClickBillingContinueButton();
        }

        private void SetBillingFirstName(string firstName)
        {
            WaitForVisibleElement(_driver, BillingFirstNameField);
            BillingFirstNameField.Clear();
            BillingFirstNameField.SendKeys(firstName);
        }

        private void SetBillingLastName(string lastName)
        {
            WaitForVisibleElement(_driver, BillingLastNameField);
            BillingLastNameField.Clear();
            BillingLastNameField.SendKeys(lastName);
        }

        private void SetBillingEmailField(string emailField)
        {
            WaitForVisibleElement(_driver, BillingEmailField);
            BillingEmailField.Clear();
            BillingEmailField.SendKeys(emailField);
        }

        private void SetBillingAddressField(string addressField)
        {
            WaitForVisibleElement(_driver, BillingAddressLine1);
            BillingAddressLine1.Clear();
            BillingAddressLine1.SendKeys(addressField);
        }

        private void SetBillingCityField(string city)
        {
            WaitForVisibleElement(_driver, BillingCityField);
            BillingCityField.Clear();
            BillingCityField.SendKeys(city);
        }

        private void SetBillingZipCodeField(string zipCode)
        {
            WaitForVisibleElement(_driver, BillingZipCodeField);
            BillingZipCodeField.Clear();
            BillingZipCodeField.SendKeys(zipCode);
        }

        private void SetBillingCountry(string country)
        {
            WaitForVisibleElement(_driver, BillingCountryField);
            SelectElement selectCountryValue = new SelectElement(BillingCountryField);
            selectCountryValue.SelectByText(country);
        }

        private void SetBillingState(string state)
        {
            WaitForVisibleElement(_driver, BillingStateField);
            SelectElement selectStateValue = new SelectElement(BillingStateField);
            selectStateValue.SelectByText(state);
        }

        private void SetBillingShippingAddressField(string addressField)
        {
            WaitForVisibleElement(_driver, BillingShippingAddressField1);
            BillingShippingAddressField1.Clear();
            BillingShippingAddressField1.SendKeys(addressField);
        }

        private void SetBillingShippingCityField(string city)
        {
            WaitForVisibleElement(_driver, BillingShippingCityField);
            BillingShippingCityField.Clear();
            BillingShippingCityField.SendKeys(city);
        }

        private void SetBillingShippingZipCodeField(string zipCode)
        {
            WaitForVisibleElement(_driver, BillingShippingZipCode);
            BillingShippingZipCode.Clear();
            BillingShippingZipCode.SendKeys(zipCode);
        }

        private void SetBillingShippingCountryField(string country)
        {
            WaitForVisibleElement(_driver, BillingShippingCountryField);
            SelectElement selectCountryValue = new SelectElement(BillingShippingCountryField);
            selectCountryValue.SelectByText(country);
        }

        private void SetBillingShippingStateField(string state)
        {
            WaitForVisibleElement(_driver, BillingShippingStateField);
            SelectElement selectStateValue = new SelectElement(BillingShippingStateField);
            selectStateValue.SelectByText(state);
        }

        public DesktopCartPage EnterShippingInformationAndContinue(IAccessUsers user, IAddress address)
        {
            if(IsShippingInformationNeeded())
            {
                EnterShippingEmail(user.Email);
                EnterShippingPhone(address.PhoneNumber);
                ClickShippingContinueButton();
            }
            else
            {
                ClickShippingContinueButton();
            }
            return this;
        }

        public bool IsShippingInformationNeeded()
        {
            return WaitForVisibleElement(_driver, ShippingEmailField);
        }

        private void EnterShippingEmail(string shippingEmail)
        {
            WaitForVisibleElement(_driver, ShippingEmailField);
            ShippingEmailField.Clear();
            ShippingEmailField.SendKeys(shippingEmail);
        }

        private void EnterShippingPhone(string shippingPhone)
        {
            WaitForVisibleElement(_driver, ShippingPhoneField);
            ShippingPhoneField.Clear();
            ShippingPhoneField.SendKeys(shippingPhone);
        }

        public void EnterCardDetails(IAccessUsers user, ICreditCard card)
        {
            EnterPaymentFirstName(user.FirstName);
            EnterPaymentLastName(user.LastName);
            EnterCreditCardNumber(EnumUtils.StringValueOf(card.CardNumber));
            SetCreditCard(EnumUtils.StringValueOf(card.Type));
            SetCVVField(EnumUtils.StringValueOf(card.Cvv));
            SetExpirationYear(card.ExpirationYear);
            SetExpirationMonth("1");
        }

        private void EnterPaymentFirstName(string paymentFirstName)
        {
            WaitForVisibleElement(_driver, CCFirstNameField);
            CCFirstNameField.Clear();
            CCFirstNameField.SendKeys(paymentFirstName);
        }

        private void EnterPaymentLastName(string paymentLastName)
        {
            WaitForVisibleElement(_driver, CCLastNameField);
            CCLastNameField.Clear();
            CCLastNameField.SendKeys(paymentLastName);
        }

        private void EnterCreditCardNumber(string paymentCreditCardNumber)
        {
            WaitForVisibleElement(_driver, CCNumberField);
            CCNumberField.Clear();
            CCNumberField.SendKeys(paymentCreditCardNumber);
        }

        private void SetCreditCard(string creditCard)
        {
            WaitForVisibleElement(_driver, SelectCCField);
            SelectElement selectCreditCard = new SelectElement(SelectCCField);
            selectCreditCard.SelectByText(creditCard);
        }

        private void SetCVVField(string cvvField)
        {
            WaitForVisibleElement(_driver, CVVField);
            CVVField.Clear();
            CVVField.SendKeys(cvvField);
        }

        private void SetExpirationYear(string expirationYear)
        {
            WaitForVisibleElement(_driver, ExpirationYear);
            SelectElement selectExpirationYear = new SelectElement(ExpirationYear);
            selectExpirationYear.SelectByText(expirationYear);
        }

        private void SetExpirationMonth(string expMonth)
        {
            WaitForVisibleElement(_driver, ExpirationMonth);
            SelectElement selectExpirationMonth = new SelectElement(ExpirationMonth);
            selectExpirationMonth.SelectByValue(expMonth);
        }

        public bool IsCheckoutSuccessMessageDisplayed()
        {
            OrderCompleteIndicator();
            WaitForPageToLoad();
            WaitForInvisibilityOfElement(_driver, CheckoutLoadingModal);
            return WaitForVisibleElement(_driver, CheckoutSuccessMessage);
        }

        public override SeleniumCommon.Page.CartPage.ICartPage EmptyCart()
        {
            IList<IWebElement> removeList = _driver.FindElements(By.XPath("//a[@ng-click='removeItem(cartItem)']"));
            GoToCart();
            foreach (IWebElement select in removeList)
            {
                ClickRemoveItemButton();
                Thread.Sleep(2000);
            }
            return this;
        }

        private bool IsBillingShippingAddressSectionDisplayed()
        {
            return WaitForVisibleElement(_driver, BillingShippingAddressField1);
        }

        private void ClickPayPalRadioButton()
        {
            WaitForVisibleElement(_driver, PayPalRadioButton);
            PayPalRadioButton.Click();
        }

        private void ClickContinueToPayPalButton()
        {
            WaitForVisibleElement(_driver, ContinueToPayPalButton);
            ContinueToPayPalButton.Click();
        }

        public DesktopCartPage StartPayPalFlow()
        {
            ClickPayPalRadioButton();
            ClickContinueToPayPalButton();
            return this;
        }

        public DesktopCartPage PayPalLogin(IAccessUsers user)
        {
            IsPayPalPreloaderSpinnerDisplayed();
            EnterPayPalEmail(user.Email);
            if(IsPayPalNextButtonDisplayed() == true)
            {
                ClickPayPalNextButton();
            }
            EnterPayPalPassword(user.Password);
            ClickPayPalLoginButton();
            return this;
        }

        private void EnterPayPalEmail(string email)
        {
            WaitForVisibleElement(_driver, PayPalEmailField);
            PayPalEmailField.Clear();
            PayPalEmailField.SendKeys(email);
        }

        private void EnterPayPalPassword(string password)
        {
            WaitForVisibleElement(_driver, PayPalPasswordField);
            PayPalPasswordField.Clear();
            PayPalPasswordField.SendKeys(password);
        }

        private void ClickPayPalLoginButton()
        {
            WaitForVisibleElement(_driver, PayPalLoginButton);
            PayPalLoginButton.Click();
        }

        private void ClickPayPalAlreadyHaveAnAccountButton()
        {
            WaitForVisibleElement(_driver, PayPalAlreadyHaveAnAccountButton);
            PayPalAlreadyHaveAnAccountButton.Click();
        }

        private void ClickPayPalNextButton()
        {
            WaitForVisibleElement(_driver, PayPalNextButton);
            PayPalNextButton.Click();
        }

        private bool IsPayPalNextButtonDisplayed()
        {
            return WaitForVisibleElement(_driver, PayPalNextButton);
        }

        public DesktopCartPage ClickPayPalContinuedButton()
        {
            IsPayPalPreloaderSpinnerDisplayed();
            Thread.Sleep(2000);
            WaitForVisibleElement(_driver, PayPalContinuedButton);
            PayPalContinuedButton.Click();
            return this;
        }

        public bool IsPayPalPreloaderSpinnerDisplayed()
        {
            return WaitForInvisibilityOfElement(_driver, PayPalPreloaderSpinner);
        }

        public bool OrderCompleteIndicator()
        {
            return WaitForInvisibilityOfElement(_driver, CheckoutLoadingModal);
        }
    }
}
