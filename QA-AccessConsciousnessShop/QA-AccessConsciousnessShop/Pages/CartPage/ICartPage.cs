﻿using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;
using SeleniumCommon.Objects.User.Address;
using SeleniumCommon.Objects.User.CreditCard;

namespace QA_AccessConsciousnessShop.Pages.CartPage
{
    interface ICartPage : SeleniumCommon.Page.CartPage.ICartPage
    {
        DesktopCartPage ClickBillingContinueButton();
        void ClickShippingContinueButton();
        void ClickPlaceOrderButton();
        DesktopCartPage EnterShippingInformationAndContinue(IAccessUsers user, IAddress address);
        void EnterCardDetails(IAccessUsers user, ICreditCard card);
        bool IsCheckoutSuccessMessageDisplayed();
        float GetCartButtonCount();
        float GetCartProductPrice();
        void EnterBillingInformationAndContinue(IAccessUsers user, IAddress address);
        DesktopCartPage StartPayPalFlow();
        DesktopCartPage PayPalLogin(IAccessUsers user);
        DesktopCartPage ClickPayPalContinuedButton();
        float GetCheckoutPrice();
        new void GoToCart();
        new void CheckoutNow();
    }
}
