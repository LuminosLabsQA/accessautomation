﻿using OpenQA.Selenium;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Threading;

namespace QA_AccessConsciousnessShop.Pages.ClassCalendarPage
{
    class DesktopClassCalendar : BasePage, IClassCalendar
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search Calendar']")]
        private IWebElement SearchInputField { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-click='searchByKeyword()']")]
        private IWebElement SearchButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//b[@class='tablesaw-cell-label ng-binding']")]
        private IWebElement rowCell { get; set; }

        [FindsBy(How = How.XPath, Using = "//tr//td//a[@class='ng-binding']")]
        private IList<IWebElement> classHyperList { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@ng-show='event.eventEndDate != event.eventStartDate']")]
        private IList<IWebElement> datesList { get; set; }

        public DesktopClassCalendar(IWebDriver driver) : base(driver)
        { 
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void GetAllSearchResults()
        {
            throw new NotImplementedException();
        }

        public void SearchByName(string keyWord)
        {
            WaitForPageToLoad();
            SearchInputField.SendKeys(keyWord);
            Thread.Sleep(2000);
            SearchButton.Click();
        }

        public void GoToClassPDP(string className, string date)
        {
            Thread.Sleep(2000);
            var list = new List<string>();
            foreach (var item in classHyperList)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }

            var datelist = new List<string>();
            foreach (var item in datesList)
            {
                string stringText = item.GetAttribute("innerText");
                datelist.Add(stringText);
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(className))
                {
                    if (datelist[i].Equals(date))
                    {
                        IList<IWebElement> el = _driver.FindElements(By.XPath("//tr//td//a[@class='ng-binding']"));
                        el[i].Click();
                        break;
                    }
                }
            }
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
