﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages
{
    interface IClassCalendar
    {
        void SearchByName(string keyWord);
        void GetAllSearchResults();
        void GoToClassPDP(string className, string date);
    }
}
