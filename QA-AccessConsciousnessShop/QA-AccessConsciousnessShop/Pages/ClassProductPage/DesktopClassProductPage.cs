﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using SeleniumCommon.Page.BasePage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.ClassProductPage
{
    public class DesktopClassProductPage : BasePage, IClassProductPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//span[@class='primary-text ng-binding']")]
        private IWebElement classPriceValue { get; set; }

        [FindsBy(How = How.Id, Using = "medium")]
        private IWebElement RegistrationButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'View POP Hosts')]")]
        private IWebElement PopHostButton { get; set; }

        public DesktopClassProductPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public double GetClassPrice(string currency)
        {
            if(currency == "EUR")
            {
                var charArray = new char[1] { 'E' };
                Thread.Sleep(1000);

                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(classPriceValue, charArray, 0);
                return (double)price;
            }
            if(currency == "AUD")
            {
                var charArray = new char[1] { 'A' };
                Thread.Sleep(1000);

                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(classPriceValue, charArray, 0);
                return (double)price;
            }
            if (currency == "USD")
            {
                var charArray = new char[1] { 'U' };
                Thread.Sleep(1000);

                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(classPriceValue, charArray, 0);
                return (double)price;
            }
            if (currency == "RON")
            {
                var charArray = new char[1] { 'R' };
                Thread.Sleep(1000);

                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(classPriceValue, charArray, 0);
                return (double)price;
            }
            return 0;
        }

        public void NavigateToRegistrationPage()
        {
            WaitForVisibleElement(_driver, RegistrationButton);
            RegistrationButton.Click();
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }

        public void GoToPOPHostPage()
        {
            WaitForVisibleElement(_driver, PopHostButton);
            PopHostButton.Click();
        }
    }
}
