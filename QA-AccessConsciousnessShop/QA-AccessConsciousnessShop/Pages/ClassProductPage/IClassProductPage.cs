﻿
namespace QA_AccessConsciousnessShop.Pages.ClassProductPage
{
    interface IClassProductPage
    {
        double GetClassPrice(string currency);
        void NavigateToRegistrationPage();
        void GoToPOPHostPage();
    }
}
