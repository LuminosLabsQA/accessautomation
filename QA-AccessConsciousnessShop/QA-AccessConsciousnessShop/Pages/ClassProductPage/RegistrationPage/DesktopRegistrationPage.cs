﻿using OpenQA.Selenium;
using QA_AccessConsciousnessShop.Pages.ClassProductPage.RegistrationPage;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;

namespace QA_AccessConsciousnessShop.Pages.ClassProductPage.DesktopRegistrationPage
{
    class DesktopRegistrationPage : BasePage, IRegistrationPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//input[@name='agree']")]
        IWebElement AgreePrivacyPolicyCheckbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='agree2']")]
        IWebElement AgreeAudioVisualCheckbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-click='registerClass(false)']")]
        IWebElement AddToMyClassesButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//h2[@class='registerSuccess']")]
        IWebElement RegistrationSuccessMessage { get; set; }

        public DesktopRegistrationPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void AgreePolicies()
        {
            AgreePrivacyPolicy();
            AgreeAudioRelease();
        }

        public void ClickAddToMyClassesButton()
        {
            WaitForVisibleElement(_driver, AddToMyClassesButton);
            AddToMyClassesButton.Click();
        }

        private void AgreePrivacyPolicy()
        {
            WaitForVisibleElement(_driver, AgreePrivacyPolicyCheckbox);
            AgreePrivacyPolicyCheckbox.Click();
        }

        private void AgreeAudioRelease()
        {
            WaitForVisibleElement(_driver, AgreeAudioVisualCheckbox);
            AgreeAudioVisualCheckbox.Click();
        }

        public bool? IsRegistrationSuccessMessageDisplayed()
        {
            WaitForVisibleElement(_driver, RegistrationSuccessMessage);
            return RegistrationSuccessMessage.Displayed;
        }

        public override void GoToCart()
        {
            throw new System.NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new System.NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new System.NotImplementedException();
        }
    }
}
