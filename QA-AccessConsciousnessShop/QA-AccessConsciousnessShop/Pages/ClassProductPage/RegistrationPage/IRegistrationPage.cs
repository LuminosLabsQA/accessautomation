﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.ClassProductPage.RegistrationPage
{
    interface IRegistrationPage
    {
        void AgreePolicies();
        void ClickAddToMyClassesButton();
        bool? IsRegistrationSuccessMessageDisplayed();
    }
}
