﻿using OpenQA.Selenium;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.ClassesHomePAge
{
    class DesktopClassesHomePage : BasePage, IClassesHomePage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/facilitators/']")]
        IWebElement FacilitatorMenu;

        public DesktopClassesHomePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void GoToFacilitatorPage()
        {
            WaitForVisibleElement(_driver, FacilitatorMenu);
            FacilitatorMenu.Click();
            FacilitatorMenu.Click();
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
