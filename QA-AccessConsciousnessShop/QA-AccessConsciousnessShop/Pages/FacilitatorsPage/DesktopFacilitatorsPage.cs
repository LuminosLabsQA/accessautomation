﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.FacilitatorsPage
{
    class DesktopFacilitatorsPage : BasePage, IFacilitatorsPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='facilitatorTypeaheadSelected']")]
        IWebElement FacilitatorSearchByName;

        [FindsBy(How = How.XPath, Using = "//strong[@class='ng-binding']")]
        IWebElement FacilitatorName;

        [FindsBy(How = How.XPath, Using = "//a[@tabindex='-1']")]
        IWebElement searchResult;

        [FindsBy(How = How.XPath, Using = "//notifications-bar[@class='notifications']")]
        IWebElement notificationsBar;

        public DesktopFacilitatorsPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public string GetFacilitatorName()
        {
            WaitForVisibleElement(_driver, FacilitatorName);

            string facilitatorName = FacilitatorName.Text;
            return facilitatorName;
        }

        public void GoToFacilitatorPublicProfile()
        {
            FacilitatorName.Click();
        }

        public void SearchByFacilitator(string name)
        {
            WaitForVisibleElement(_driver, FacilitatorSearchByName);
            ScrollToElement(FacilitatorSearchByName);
            FacilitatorSearchByName.SendKeys(name);
            WaitForInvisibilityOfElement(_driver, notificationsBar);
            WaitForVisibleElement(_driver, searchResult);
            searchResult.Click();
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
