﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.FacilitatorsPage
{
    interface IFacilitatorsPage
    {
        void SearchByFacilitator(string name);
        string GetFacilitatorName();
        void GoToFacilitatorPublicProfile();
    }
}
