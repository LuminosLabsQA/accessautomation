﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumCommon.Objects.User.Address;
using SeleniumCommon.Objects.User;
using SeleniumExtras.PageObjects;
using QA_AccessConsciousnessShop.Objects;

namespace QA_AccessConsciousnessShop.Pages.LoginPage
{
    public class DesktopLoginPage : SeleniumCommon.Page.LoginPage.LoginPage, ILoginPage
    {
        Random rnd = new Random();
        
        public int randomAddress()
        {
            int randomAddress = rnd.Next(0, 2);
            return randomAddress;
        }

        IWebDriver _driver;

        public DesktopLoginPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        [FindsBy(How = How.XPath, Using = ("//li[@ng-hide='isAuthenticated']"))]
        private IWebElement LoginLinkHeader { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-sm-6']//button[@type='submit']")]
        public IWebElement LoginButton { get; set; }

        [FindsBy(How = How.XPath, Using =("//a[@href='/en/create-account/']"))]
        private IWebElement RegisterButton { get; set; }

        [FindsBy(How = How.Id, Using =("pass"))]
        private IWebElement SetPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = ("confirmPass"))]
        private IWebElement ConfirmPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = ("username"))]
        private IWebElement UsernameField { get; set; }

        [FindsBy(How = How.XPath, Using = ("//div[@ng-show='errorMessage']"))]
        private IWebElement FailedLoginErrorMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/my-account/digital-download/']")]
        private IWebElement DigitalDownloadLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='splashModal']//button[@class='close']")]
        public IWebElement CloseSplashModalButton { get; set; }

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement EmailField { get; set; }

        [FindsBy(How = How.Id, Using = "firstName")]
        public IWebElement FirstNameField { get; set; }

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement LastNameField { get; set; }

        [FindsBy(How = How.Id, Using = "address1")]
        public IWebElement Address1Field { get; set; }

        [FindsBy(How = How.Id, Using = "address2")]
        public IWebElement Address2Field { get; set; }

        [FindsBy(How = How.Id, Using = "countrySelect")]
        public IWebElement CountryField { get; set; }

        [FindsBy(How = How.Id, Using = "stateSelect")]
        public IWebElement StateField { get; set; }

        [FindsBy(How = How.Id, Using = "city")]
        public IWebElement CityField { get; set; }

        [FindsBy(How = How.Id, Using = "postalCode")]
        public IWebElement PostalCode { get; set; }

        [FindsBy(How = How.Id, Using = "phone")]
        public IWebElement PhoneField { get; set; }

        [FindsBy(How = How.Id, Using = "AgreeToTerms")]
        public IWebElement TermsOfAgreement { get; set; }

        [FindsBy(How = How.Id, Using = "RecieveEmails")]
        public IWebElement EmailAgree { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-sm-12 martop15']//button[@type='submit']")]
        public IWebElement CreateAccountButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//h2[@class='marbottom30 registerSuccess']")]
        public IWebElement CreateAccountSuccess { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='inputUsername']")]
        private IWebElement UsernameLoginField { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='inputPassword']")]
        private IWebElement PasswordLoginField { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@ng-click='logout()']")]
        private IWebElement LogoutLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-body']//a[@data-target='#loginModal']")]
        public IWebElement SignInHereShopPopUp { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='login-form']//a[@href='/en/create-account/']")]
        public IWebElement CreateAccountLogInLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/forgot-password/']")]
        public IWebElement ForgotPasswordLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@loading-button='isLoadingEnabled']")]
        public IWebElement ForgotPasswordSubmitButtonOne { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@loading-button='isSecondLoadingEnabled']")]
        public IWebElement ForgotPasswordSubmitButtonTwo { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='displaySecondForm()']")]
        public IWebElement ExpandForgotPasswordForm { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='pad15 text-center']")]
        public IWebElement ForgotPasswordSuccessMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@ng-show='errorMessage']")]
        public IWebElement LoginErrorMessage { get; set; }

        [FindsBy(How = How.Id, Using = "epi-quickNavigator-clickHandler")]
        public IWebElement AdminButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='loginModel.Remember']")]
        public IWebElement KeepMeLoggedIn { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@data-target='#termsOfServiceModal']")]
        public IWebElement TermsOfServicesLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@data-target='#privacyPolicyModal']")]
        public IWebElement PrivacyPolicyLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@data-target='#cookiePolicyModal']")]
        public IWebElement CookiePolicyLink { get; set; }

        [FindsBy(How = How.Id, Using = "termsOfServiceModal")]
        public IWebElement TermsOfServiceModal { get; set; }

        [FindsBy(How = How.Id, Using = "privacyPolicyModal")]
        public IWebElement PrivacyPolicyModal { get; set; }

        [FindsBy(How = How.Id, Using = "cookiePolicyModal")]
        public IWebElement CookiePolicyModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@style='float: none; padding-left: 20px;']//a[@href='/en/dashboard/my-account/change-password/']")]
        public IWebElement ChangeCredentialsLink { get; set; }

        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement ChangeCredentialsField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value='Change Username']")]
        public IWebElement ChangeCredentialsButton { get; set; }

        [FindsBy(How = How.Id, Using = "oldpassword")]
        public IWebElement OldPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = "newpassword")]
        public IWebElement NewPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = "newpasswordconfirmation")]
        public IWebElement ConfirmNewPasswordField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value='Change Password']")]
        public IWebElement ChangePasswordButton { get; set; }

        public override void ClickLoginButton()
        {
            LoginButton.Click();
            WaitForPageToLoad();
        }

        public override void ClickRegisterButton()
        {
            CreateAccountButton.Click();
        }

        public new void Login(IUser user)
        {
            SetLoginUsername(user.UserName);
            SetLoginPassword(user.Password);
            ClickLoginButton();
            WaitForPageToLoad();
        }

        private void SetLoginPassword(string loginPassword)
        {
            WaitForVisibleElement(_driver, PasswordLoginField);
            PasswordLoginField.Clear();
            PasswordLoginField.SendKeys(loginPassword);
        }

        private void SetLoginUsername(string loginUserName)
        {
            WaitForVisibleElement(_driver, UsernameLoginField);
            UsernameLoginField.Clear();
            UsernameLoginField.SendKeys(loginUserName);
        }

        public new void Register(IUser user)
        {
            SetUserFirstName(user.FirstName);
            SetUserLastName(user.LastName);
            SetUserEmail(user.Email);         
            ClickTermsOfAgreement();
            ClickAgreeEmails();
            SetUserAddress(user.Addresses[randomAddress()]);
            ClickRegisterButton();
            WaitForPageToLoad();
        }

        private void SetUserAddress(IAddress address)
        {
            SetBuilding(address.AddressStreet, address.AddressUnit);
            SetCountry(address.Country);
            SetState(address.State);
            SetCity(address.City);
            SetPostalCode(address.Zip);
            SetPhoneNumber(address.PhoneNumber);
        }

        private void SetPhoneNumber(string phoneNumber)
        {
            WaitForVisibleElement(_driver, PhoneField);
            PhoneField.Clear();
            PhoneField.SendKeys(phoneNumber);
        }

        private void SetPostalCode(string zip)
        {
            WaitForVisibleElement(_driver, PostalCode);
            PostalCode.Clear();
            PostalCode.SendKeys(zip);
        }

        private void SetCity(string city)
        {
            WaitForVisibleElement(_driver, CityField);
            CityField.Clear();
            CityField.SendKeys(city);
        }

        private void SetCountry(string country)
        {
            WaitForVisibleElement(_driver, CountryField);
            SelectElement selectCountry = new SelectElement(CountryField);
            selectCountry.SelectByText(country);
        }

        private void SetState(string state)
        {
            WaitForVisibleElement(_driver, StateField);
            SelectElement selectState = new SelectElement(StateField);
            selectState.SelectByText(state); 
        }

        private void SetBuilding(string addressStreet, string addressUnit)
        {
            WaitForVisibleElement(_driver, Address1Field);
            Address1Field.Clear();
            Address1Field.SendKeys(addressStreet);

            //if (!string.IsNullOrEmpty(addressUnit))
            //{
                Address2Field.Clear();
                Address2Field.SendKeys(addressUnit);
            //}
        }

        private void SetUserLastName(string lastName)
        {
            WaitForVisibleElement(_driver, LastNameField);
            LastNameField.Clear();
            LastNameField.SendKeys(lastName);
        }

        private void SetUserFirstName(string firstName)
        {
            WaitForVisibleElement(_driver, FirstNameField);
            FirstNameField.Clear();
            FirstNameField.SendKeys(firstName);
        }

        private void SetUserEmail(string email)
        {
            WaitForVisibleElement(_driver, EmailField);
            EmailField.Clear();
            EmailField.SendKeys(email);
        }

        public override SeleniumCommon.Page.LoginPage.ILoginPage ConfirmPassword(string password)
        {
            WaitForVisibleElement(_driver, ConfirmPasswordField);
            ConfirmPasswordField.Clear();
            ConfirmPasswordField.SendKeys(password);
            return this;
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            WaitForPageToLoad();
            LoginLinkHeader.Click();
        }

        public override bool IsLogginErrorMessageDisplayed()
        {
            WaitForPageToLoad();
            return WaitForVisibleElement(_driver, FailedLoginErrorMessage);
        }

        public override SeleniumCommon.Page.LoginPage.ILoginPage GoToRegistrationForm()
        {
            WaitForVisibleElement(_driver, RegisterButton);
            RegisterButton.Click();
            return this;
        }

        public override SeleniumCommon.Page.LoginPage.ILoginPage GoToLoginForm()
        {
            throw new NotImplementedException();
        }

        public override SeleniumCommon.Page.LoginPage.ILoginPage SetPassword(string password)
        {
            WaitForVisibleElement(_driver, SetPasswordField);
            SetPasswordField.Clear();
            SetPasswordField.SendKeys(password);
            return this;
        }

        public override SeleniumCommon.Page.LoginPage.ILoginPage SetUserName(string userName)
        {
            WaitForVisibleElement(_driver, UsernameField);
            UsernameField.Clear();
            UsernameField.SendKeys(userName);
            return this;
        }

        public void ClickTermsOfAgreement()
        {
            TermsOfAgreement.Click();
        }

        public void ClickAgreeEmails()
        {
            EmailAgree.Click();
        }

        bool ILoginPage.IsCreateAccountSuccessMessagesDisplayed()
        {
            WaitForPageToLoad();
            return WaitForVisibleElement(_driver, CreateAccountSuccess);
        }

        public void ClickLogoutLink()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, LogoutLink);
            LogoutLink.Click();
            WaitForPageToLoad();
        }

        public void ClickSignInHereFromShopPopup()
        {
            WaitForPageToLoad();
            SignInHereShopPopUp.Click();
        }

        public void ClickForgotPassword()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, ForgotPasswordLink);
            ForgotPasswordLink.Click();
        }

        public void ClickForgotPasswordSubmitButtonUsername()
        {
            ForgotPasswordSubmitButtonOne.Click();
        }

        public bool IsForgotPasswordSuccessMessageDisplayed()
        {
            return WaitForVisibleElement(_driver, ForgotPasswordSuccessMessage);
        }

        public void ClickExpandForgotPasswordForm()
        {
            ExpandForgotPasswordForm.Click();
        }

        public void ClickForgotPasswordSubmitButtonEmail()
        {
            ForgotPasswordSubmitButtonTwo.Click();
        }

        public void ResetPasswordUsername(IAccessUsers user)
        {
            WaitForPageToLoad();
            SetUserName(user.UserName);
            ClickForgotPasswordSubmitButtonUsername();
            WaitForPageToLoad();
        }
        public void ResetPasswordEmail(IAccessUsers user)
        {
            WaitForPageToLoad();
            ClickExpandForgotPasswordForm();
            SetUserEmail(user.Email);
            ClickForgotPasswordSubmitButtonEmail();
            WaitForPageToLoad();
        }

        public bool IsLoginErrorMessageDisplayed()
        {
            return WaitForVisibleElement(_driver, LoginErrorMessage);
        }

        public bool isAdminButtonDisplayed()
        {
            return WaitForVisibleElement(_driver, AdminButton);
        }

        public void ClickKeepMeLoggedIn()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, KeepMeLoggedIn);
            KeepMeLoggedIn.Click();
        }

        public override bool IsLogginPageLoaded()
        {
            throw new NotImplementedException();
        }

        public ILoginPage ClickTermsOfServiceLink()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, TermsOfServicesLink);
            TermsOfServicesLink.Click();
            return this;
        }

        public ILoginPage ClickPrivacyPolicyLink()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, PrivacyPolicyLink);
            PrivacyPolicyLink.Click();
            return this;
        }

        public ILoginPage ClickCookiePolicyLink()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, CookiePolicyLink);
            CookiePolicyLink.Click();
            return this;
        }

        public bool IsTermsOfServicesModalDisplayed()
        {
            return WaitForVisibleElement(_driver, TermsOfServiceModal);
        }

        public bool IsPrivacyPolicyModalDisplayed()
        {
            return WaitForVisibleElement(_driver, PrivacyPolicyModal);
        }

        public bool IsCookiePolicyModalDisplayed()
        {
            return WaitForVisibleElement(_driver, CookiePolicyModal);
        }

        public void ClickCreateAccountLoginLink()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, CreateAccountLogInLink);
            CreateAccountLogInLink.Click();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
