﻿using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;

namespace QA_AccessConsciousnessShop.Pages.LoginPage
{
    public interface ILoginPage : SeleniumCommon.Page.LoginPage.ILoginPage
    {
        void ClickTermsOfAgreement();
        void ClickAgreeEmails();
        bool IsCreateAccountSuccessMessagesDisplayed();
        void ClickLogoutLink();
        void ClickSignInHereFromShopPopup();
        void ClickForgotPassword();
        void ClickForgotPasswordSubmitButtonUsername();
        void ClickExpandForgotPasswordForm();
        void ClickForgotPasswordSubmitButtonEmail();
        void ResetPasswordUsername(IAccessUsers user);
        void ResetPasswordEmail(IAccessUsers user);
        bool IsLoginErrorMessageDisplayed();
        bool isAdminButtonDisplayed();
        void ClickKeepMeLoggedIn();
        new void Login(IUser user);
        ILoginPage ClickTermsOfServiceLink();
        ILoginPage ClickPrivacyPolicyLink();
        ILoginPage ClickCookiePolicyLink();
        bool IsTermsOfServicesModalDisplayed();
        bool IsPrivacyPolicyModalDisplayed();
        bool IsCookiePolicyModalDisplayed();
        void ClickCreateAccountLoginLink();
        new void GoToLogin();
       }
}
