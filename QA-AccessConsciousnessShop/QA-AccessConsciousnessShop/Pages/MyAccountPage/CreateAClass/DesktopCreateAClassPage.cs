﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using QA_AccessConsciousnessShop.Objects;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass;
using SeleniumCommon.Page.BasePage;
using System;
using System.Threading;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClassScreen
{
    class DesktopCreateAClassPage : BasePage, ICreateAClassPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.Id, Using = "selectClassGroup")]
        private IWebElement ClassGroupDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "selectClassTitle")]
        private IWebElement ClassTitleDropdown { get; set; }

        [FindsBy(How = How.XPath, Using = "//multiple-selection[@info='medium']//button[@ng-click='displayDropdown(true)']")]
        private IWebElement ClassMediumButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//multiple-selection[@info='medium']//select[@ng-model='model.SelectedItem']")]
        private IWebElement ClassMediumDropdown { get; set; }

        [FindsBy(How = How.XPath, Using = "//multiple-selection[@info='medium']//button[@ng-click='add(model)']")]
        private IWebElement MediumSaveButton { get; set; }

        [FindsBy(How = How.Id, Using = "timezoneDropdown")]
        private IWebElement TimeZoneDropdown { get; set; }

        [FindsBy(How = How.Name, Using = "startDate")]
        private IWebElement StartDateField { get; set; }

        [FindsBy(How = How.Name, Using = "endDate")]
        private IWebElement EndDateField { get; set; }

        [FindsBy(How = How.XPath, Using = "//multiple-selection[@info='language']//button[@ng-click='displayDropdown(true)']")]
        private IWebElement ClassLanguageButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//multiple-selection[@info='language']//select[@ng-model='model.SelectedItem']")]
        private IWebElement ClassLanguageDropdown { get; set; }

        [FindsBy(How = How.XPath, Using = "//multiple-selection[@info='language']//button[@ng-click='add(model)']")]
        private IWebElement LanguageSaveButton { get; set; }

        [FindsBy(How = How.Id, Using = "classCost")]
        private IWebElement ClassCostField { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@ng-model='model.PrimaryCurrency']")]
        private IWebElement ClassCurrencyDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "venue")]
        private IWebElement VenueNameField { get; set; }

        [FindsBy(How = How.Id, Using = "address1")]
        private IWebElement ClassAddressField1 { get; set; }

        [FindsBy(How = How.Id, Using = "address2")]
        private IWebElement ClassAddressField2 { get; set; }

        [FindsBy(How = How.Id, Using = "city")]
        public IWebElement CityField { get; set; }

        [FindsBy(How = How.Id, Using = "countrySelect")]
        private IWebElement CountryDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "city")]
        private IWebElement ClassCityField { get; set; }

        [FindsBy(How = How.Id, Using = "paymentCompany")]
        private IWebElement PaymentCompanyDropdown { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-hide='hidePostToPublicCalendarButton(isInEditMode) || !model.FacilitatorHasActivePublicProfile']")]
        private IWebElement PostToPublicCalendarButton { get; set; }

        [FindsBy(How = How.Id, Using = "successMessage")]
        private IWebElement PostToPublicCalendarSuccessMessage { get; set; }

        [FindsBy(How = How.Id, Using = "totalNumberOfCalls")]
        private IWebElement NumberOfCallsField { get; set; }

        [FindsBy(How = How.Id, Using = "callNumbersToDisplay")]
        private IWebElement NumbersOfCallsToDisplayField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='callDate0']")]
        private IWebElement CallOneField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='callDate1']")]
        private IWebElement CallTwoField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='model.Discounts.LocationDiscountsAllowed']")]
        private IWebElement CountryPriceDiscountCheckbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='model.Discounts.AgeDiscountsAllowed']")]
        private IWebElement AgePriceDiscountCheckbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='model.Discounts.RepeatClassDiscountsAllowed']")]
        private IWebElement RepeatPriceDiscountCheckbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//h1[@class='header']")]
        private IWebElement EditEventPageTitle { get; set; }

        public DesktopCreateAClassPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void CreateAdminOccurrence(IOccurrence occurrence)
        {
            occurrence.CreateOccurrence(this);
        }

        public void CreateFacilitatorOccurrence(IOccurrence occurrence)
        {
            occurrence.CreateFacilitatorOccurrence(this);
        }

        public void SelectCountryDiscount()
        {
            CountryPriceDiscountCheckbox.Click();
        }

        public void SelectAgeDiscount()
        {
            AgePriceDiscountCheckbox.Click();
        }

        public void SelectRepeatDiscount()
        {
            RepeatPriceDiscountCheckbox.Click();
        }

        public void SetClassGroup(string classGroup)
        {
            SelectElement element = new SelectElement(ClassGroupDropdown);
            element.SelectByText(classGroup);
        }

        public void SetClassTitle(string classTitle)
        {
            SelectElement element = new SelectElement(ClassTitleDropdown);
            element.SelectByText(classTitle);
        }

        public void SetClassMedium(string classMedium)
        {
            Thread.Sleep(8000);
            if (ClassMediumButton.Displayed)
            {
                ClassMediumButton.Click();
                WaitForVisibleElement(_driver, ClassMediumDropdown);
                SelectElement element = new SelectElement(ClassMediumDropdown);
                element.SelectByText(classMedium);
                MediumSaveButton.Click();
            }
        }

        public void SetClassLanguage(string classLanguage)
        {
            ClassLanguageButton.Click();
            WaitForVisibleElement(_driver, ClassLanguageDropdown);
            SelectElement element = new SelectElement(ClassLanguageDropdown);
            element.SelectByText(classLanguage);
            LanguageSaveButton.Click();
        }

        public void SetClassTimeZone(string timeZone)
        {
            SelectElement element = new SelectElement(TimeZoneDropdown);
            element.SelectByText(timeZone);
        }

        public void SetClassStartDate(string date)
        {
            StartDateField.SendKeys(date);
        }

        public void SetClassEndDate(string date)
        {
            if (EndDateField.Displayed)
            {
                EndDateField.SendKeys(date);
            }
        }

        public void SetClassPrice(string classPrice)
        {
            ClassCostField.Clear();
            ClassCostField.SendKeys(classPrice);
        }

        public void SetClassCurrency(string classCurrency)
        {
            SelectElement element = new SelectElement(ClassCurrencyDropdown);
            element.SelectByText(classCurrency);
        }

        public void SetClassAddress(string venueName, string AddressOne, string AddressTwo, string classCountry, string city)
        {
            VenueNameField.SendKeys(venueName);
            ClassAddressField1.SendKeys(AddressOne);
            ClassAddressField2.SendKeys(AddressTwo);
            SelectElement element = new SelectElement(CountryDropdown);
            element.SelectByText(classCountry);
            CityField.SendKeys(city);
        }

        public void SetClassPaymentCompany(string classPaymentCompany)
        {
            SelectElement element = new SelectElement(PaymentCompanyDropdown);
            element.SelectByText(classPaymentCompany);
        }

        public void ClickPostToPublicCalendarButton()
        {
            ScrollToElement(PostToPublicCalendarButton);
            PostToPublicCalendarButton.Click();
        }

        public bool IsPostToPublicCalendarSuccessMessageDisplayed()
        {
            WaitForInvisibilityOfElement(_driver, PostToPublicCalendarButton);
            return WaitForVisibleElement(_driver, PostToPublicCalendarSuccessMessage);
        }

        public void SetTelecallNumbersOfCalls(string noOfCalls, string noOfCallsToDisplay)
        {
            NumberOfCallsField.SendKeys(noOfCalls);
            NumbersOfCallsToDisplayField.SendKeys(noOfCallsToDisplay);
        }

        public void SetTelecallDates(string dateOne, string dateTwo)
        {
            CallOneField.SendKeys(dateOne);
            CallTwoField.SendKeys(dateTwo);
        }

        public bool? IsEditEventPageDisplayed()
        {
            WaitForVisibleElement(_driver, EditEventPageTitle);
            return EditEventPageTitle.Displayed;
        }


        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
