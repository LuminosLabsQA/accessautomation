﻿using QA_AccessConsciousnessShop.Objects;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass
{
    public interface ICreateAClassPage
    {
        void CreateAdminOccurrence(IOccurrence occurrence);
        void CreateFacilitatorOccurrence(IOccurrence occurrence);
        void ClickPostToPublicCalendarButton();
        bool IsPostToPublicCalendarSuccessMessageDisplayed();
        void SetClassGroup(string classGroup);
        void SetClassTitle(string classTitle);
        void SetClassMedium(string classMedium);
        void SetClassLanguage(string classLanguage);
        void SetClassTimeZone(string timeZone);
        void SetClassStartDate(string date);
        void SetClassEndDate(string date);
        void SetClassPrice(string classPrice);
        void SetClassCurrency(string classCurrency);
        void SetClassPaymentCompany(string classPaymentCompany);
        void SetClassAddress(string venueName, string AddressOne, string AddressTwo, string classCountry, string city);
        void SetTelecallNumbersOfCalls(string noOfCalls, string noOfCallsToDisplay);
        void SetTelecallDates(string dateOne, string dateTwo);
        void SelectCountryDiscount();
        void SelectAgeDiscount();
        void SelectRepeatDiscount();
        bool? IsEditEventPageDisplayed();
    }
}
