﻿using QA_AccessConsciousnessShop.Pages.LoginPage;
using SeleniumCommon.Objects.User;
using QA_AccessConsciousnessShop.Objects;
using System.Threading;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using SeleniumCommon.Objects.User.CreditCard;
using SeleniumCommon.Utilities;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage
{
    public class DesktopMyAccountPage :  DesktopLoginPage, IMyAccountPage
    {
        IWebDriver _driver;

        public DesktopMyAccountPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//li[@ng-show='isAuthenticated']")]
        private IWebElement UserDashboard { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/my-account/contact-info/']")]
        private IWebElement ContactInfo { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/my-account/public-profile/']")]
        private IWebElement PublicProfileLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/my-account/memberships/']")]
        private IWebElement MembershipsLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/my-account/dependents/']")]
        private IWebElement DependentsLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='#passwordTab']")]
        private IWebElement PasswordTab { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='pad15 text-center']")]
        private IWebElement ChangeCredentialsSuccessMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-click='showModalDialogForAdd()']")]
        private IWebElement AddDependentButton { get; set; }

        [FindsBy(How = How.Id, Using ="email")]
        private IWebElement contactInfoEmailField { get; set; }

        [FindsBy(How = How.Id, Using = "firstName")]
        private IWebElement ContactInfoFirstNameField { get; set; }

        [FindsBy(How = How.Id, Using = "lastName")]
        private IWebElement ContactInfoLastNameField { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@data-phone='contactInfo.Phone']//input[@name='phone']")]
        private new IWebElement PhoneField { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@data-phone='contactInfo.HomePhone']//input[@name='phone']")]
        private IWebElement HomePhoneField { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@data-phone='contactInfo.MobilePhone']//input[@name='phone']")]
        private IWebElement MobilePhoneField { get; set; }

        [FindsBy(How = How.Id, Using = "preferredCurrency")]
        private IWebElement CurrencyField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value='Save']")]
        private IWebElement ContactInfoSave { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='block bordered pad15 col-sm-6 text-center']")]
        private IWebElement ContactInfoSuccessMessage { get; set; }

        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement DependentUsernameField { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement DependentPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = "confirmPassword")]
        public IWebElement DependentConfirmPasswordField { get; set; }

        [FindsBy(How = How.Id, Using = "firstName")]
        public IWebElement DependentFirstNameField { get; set; }

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement DependentLastNameField { get; set; }

        [FindsBy(How = How.Id, Using = "Relationship")]
        public IWebElement DependentRelationShipDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "Relationship")]
        private IWebElement RelationshipDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "monthSelect")]
        private IWebElement DependentSelectMonth { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@ng-options='day for day in days']")]
        private IWebElement DependentSelectDay { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@ng-options='year for year in years']")]
        private IWebElement DependentSelectYear { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-footer']//button[@class='btn btn-green']")]
        private IWebElement DependentSaveButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//td[@class='depName ng-binding']")]
        private IList<IWebElement> SavedDependentFullName { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@class='nav nav-pills nav-stacked']//a[@href='/en/dashboard/my-classes/billing-payment/']")]
        private IWebElement BillingAndPaymentsLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/facilitator-tools/create-a-class/']")]
        private IWebElement CreateClassLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/my-classes/upcoming-classes/']")]
        private IWebElement UpcomingClassesLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/dashboard/facilitator-tools/facilitating-schedule/']")]
        private IWebElement FacilitatingSchedulLink { get; set; }

        public DesktopMyAccountPage GoToContactInfo()
        {
            WaitForVisibleElement(_driver, UserDashboard);
            WaitForInvisibilityOfElement(_driver, LoginButton);
            UserDashboard.Click();
            ContactInfo.Click();
            return this;
        }

        public void GoToPublicProfile()
        {
            UserDashboard.Click();
            PublicProfileLink.Click();
        }

        public void GoToUpcomingClasses()
        {
            ClickUserDashboard();
            UpcomingClassesLink.Click();
        }

        public void GoToMemberships()
        {
            UserDashboard.Click();
            MembershipsLink.Click();
        }

        private void ClickUserDashboard()
        {
            WaitForVisibleElement(_driver, UserDashboard);
            UserDashboard.Click();
        }

        public DesktopMyAccountPage ClickDependentsLink()
        {
            ClickUserDashboard();
            WaitForVisibleElement(_driver, DependentsLink);
            DependentsLink.Click();
            return this;
        }

        public DesktopMyAccountPage GoToChangeCredentials()
        {
            ClickUserDashboard();
            WaitForVisibleElement(_driver, ChangeCredentialsLink);
            ChangeCredentialsLink.Click();
            return this;
        }

        public bool IsUserLoggedIn()
        {
            WaitForPageToLoad();
            WaitForInvisibilityOfElement(_driver, LoginButton);
            WaitForVisibleElement(_driver, UserDashboard);
            return WaitForVisibleElement(_driver, UserDashboard);
        }

        public void ChangePassword(IAccessUsers user)
        {
            ChangeToPasswordTab();
            EnterOldPassword(user.Password);
            EnterNewPassword(user.Password);
            EnterConfirmNewPassword(user.Password);
            ClickChangePasswordButton();
        }

        private void EnterOldPassword(string oldPassword)
        {
            WaitForVisibleElement(_driver, OldPasswordField);
            OldPasswordField.SendKeys(oldPassword);
        }

        private void EnterNewPassword(string newPassword)
        {
            WaitForVisibleElement(_driver, NewPasswordField);
            NewPasswordField.SendKeys(newPassword);
        }

        private void EnterConfirmNewPassword(string newPasswordConfirm)
        {
            WaitForVisibleElement(_driver, ConfirmNewPasswordField);
            ConfirmNewPasswordField.SendKeys(newPasswordConfirm);
        }

        private void ClickChangePasswordButton()
        {
            WaitForPageToLoad();
            ChangePasswordButton.Click();
        }

        public void ChangeToPasswordTab()
        {
            WaitForVisibleElement(_driver, PasswordTab);
            PasswordTab.Click();
        }

        public bool IsChangeCredentialsSuccessMessageDisplayed()
        {
            WaitForPageToLoad();
            return WaitForVisibleElement(_driver, ChangeCredentialsSuccessMessage);
        }

        public void ChangeUsername(IUser user)
        {
            InputChangeCredentialsUsername(user.UserName);
            ClickChangeCredentialsbutton();
        }

        private void InputChangeCredentialsUsername(string changeUsername)
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, ChangeCredentialsField);
            Thread.Sleep(1000);
            ChangeCredentialsField.Clear();
            Thread.Sleep(1000);
            ChangeCredentialsField.SendKeys(changeUsername);
        }

        private void ClickChangeCredentialsbutton()
        {
            ChangeCredentialsButton.Click();
        }

        public DesktopMyAccountPage ClickAddDependentsButton()
        {
            WaitForVisibleElement(_driver, AddDependentButton);
            AddDependentButton.Click();
            return this;
        }

        public DesktopMyAccountPage AddNewRandomDependentInformation(IAccessUsers user)
        {
            //EnterDependentUsername(user.UserName);
            //EnterDependentPassword(user.Password);
            //EnterDependentConfirmPassword(user.ConfirmationPassword);
            EnterDependentFirstName(user.FirstName);
            EnterDependentLastName(user.LastName);
            SelectDependentRelationship();
            SelectDependentBirthDate();
            return this;
        }

        public DesktopMyAccountPage ClickSaveDependentButton()
        {
            DependentSaveButton.Click();
            return this;
        }

        //private void EnterDependentUsername(string depUsername)
        //{
        //    WaitForVisibleElement(_driver, DependentUsernameField);
        //    DependentUsernameField.SendKeys(depUsername);
        //}

        //private void EnterDependentPassword(string depPassword)
        //{
        //    WaitForVisibleElement(_driver, DependentPasswordField);
        //    DependentPasswordField.SendKeys(depPassword);
        //}

        //private void EnterDependentConfirmPassword(string depPasswordConfirm)
        //{
        //    WaitForVisibleElement(_driver, DependentConfirmPasswordField);
        //    DependentConfirmPasswordField.SendKeys(depPasswordConfirm);
        //}

        private void EnterDependentFirstName(string depFirstName)
        {
            WaitForVisibleElement(_driver, DependentFirstNameField);
            DependentFirstNameField.SendKeys(depFirstName);
        }

        private void EnterDependentLastName(string depLastName)
        {
            WaitForVisibleElement(_driver, DependentLastNameField);
            DependentLastNameField.SendKeys(depLastName);
        }

        private void SelectDependentRelationship()
        {
            SelectElement select = new SelectElement(RelationshipDropdown);
            select.SelectByText("Step-Child");
        }

        private void SelectDependentBirthDate()
        {
            SelectElement monthSelect = new SelectElement(DependentSelectMonth);
            monthSelect.SelectByText("10");
            SelectElement daySelect = new SelectElement(DependentSelectDay);
            daySelect.SelectByText("10");
            SelectElement yearSelect = new SelectElement(DependentSelectYear);
            yearSelect.SelectByText("2010");
        }

        public DesktopMyAccountPage ChangeUserContactInfo(IAccessUsers user)
        {
            Thread.Sleep(2000);
            GetEmailField().Clear();
            GetEmailField().SendKeys(user.Email);
            GetFirstNameField().Clear();
            GetFirstNameField().SendKeys(user.FirstName);
            GetLastNameField().Clear();
            GetLastNameField().SendKeys(user.LastName);
            GetPhoneField().Clear();
            GetPhoneField().SendKeys(user.PhoneField);
            GetHomePhoneField().Clear();
            GetHomePhoneField().SendKeys(user.HomePhone);
            GetMobilePhoneField().Clear();
            GetMobilePhoneField().SendKeys(user.MobilePhone);
            GetCurrencyField(user.Currency);
            return this;
        }

        internal IWebElement GetEmailField()
        {
            return contactInfoEmailField;
        }

        internal IWebElement GetFirstNameField()
        {
            return ContactInfoFirstNameField;
        }

        internal IWebElement GetLastNameField()
        {
            return ContactInfoLastNameField;
        }

        internal IWebElement GetPhoneField()
        {
            return PhoneField;
        }

        internal IWebElement GetHomePhoneField()
        {
            return HomePhoneField;
        }

        internal IWebElement GetMobilePhoneField()
        {
            return MobilePhoneField;
        }

        internal void GetCurrencyField(string currency)
        {
            SelectElement select = new SelectElement(CurrencyField);
            select.SelectByText(currency);
        }

        public void ClickContactInfoSave()
        {
            ContactInfoSave.Click();
        }

        public bool? IsContactInfoSaveSuccessMessageDisplayed()
        {
            WaitForVisibleElement(_driver, ContactInfoSuccessMessage);
            WaitForInvisibilityOfElement(_driver, GetEmailField());
            return ContactInfoSuccessMessage.Displayed;
        }

        public bool? IsAddedDependentNameCorrect(string dependentFullName)
        {
            WaitForInvisibilityOfElement(_driver, DependentSaveButton);
            foreach (var item in SavedDependentFullName)
            {
                if (item.Text.Equals(dependentFullName))
                {
                    return true;
                }
            }
            return false;
        }

        public void GoToPaymentsDue()
        {
            Thread.Sleep(2000);
            GoToContactInfo();
            BillingAndPaymentsLink.Click();
        }

        public void GoToCreateAClass()
        {
            ClickUserDashboard();
            WaitForVisibleElement(_driver, CreateClassLink);
            CreateClassLink.Click();
        }

        public void GoToFacilitatingSchedule()
        {
            ClickUserDashboard();
            WaitForVisibleElement(_driver, FacilitatingSchedulLink);
            FacilitatingSchedulLink.Click();
        }
    }
}
