﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using QA_AccessConsciousnessShop.Utilities;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.FacilitatingSchedule
{
    class DesktopFacilitatingSchedulePage : BasePage, IFacilitatingSchedulePage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//span[@class='glyphicon glyphicon-cog']")]
        IWebElement gearIcon { get; set; }

        [FindsBy(How = How.XPath, Using = "//td[@class='dropdown open']//li[1]")]
        IWebElement editLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//td[@class='ng-binding']")]
        IList<IWebElement> classNameText { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='attendeeName ng-binding']")]
        IList<IWebElement> attendeesName { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='glyphicon glyphicon-th-list']")]
        IWebElement rosterButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn btn-default rosterAttendeesAdd']")]
        IWebElement addAttendeeButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@ng-model='searchCustomerKeyword']")]
        IWebElement searchByEmailField { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-click='selectCustomer(customer)']")]
        IWebElement selectUserButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='dropFromClass(attendant)']")]
        IList<IWebElement> dropAttendeeButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-header ng-binding']")]
        IWebElement addAttendeeModal { get; set; }

        public DesktopFacilitatingSchedulePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void GoToClassEdit(string className)
        {
            var list = new List<string>();
            foreach (var item in classNameText)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(className)) 
                {
                    gearIcon.Click();
                    editLink.Click();
                    break;
                }
            }
        }

        public void GoToClassRoster(string className)
        {
            var list = new List<string>();
            foreach (var item in classNameText)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(className))
                {
                    rosterButton.Click();
                    break;
                }
            }
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }

        public void AddAttendee(string email)
        {
            WaitForVisibleElement(_driver, addAttendeeButton);
            addAttendeeButton.Click();
            WaitForVisibleElement(_driver, searchByEmailField);
            searchByEmailField.SendKeys(email);
            searchByEmailField.Click();
            Actions action = new Actions(_driver);
            action.SendKeys(Keys.Enter).Perform();
            selectUserButton.Click();
            WaitForInvisibilityOfElement(_driver, addAttendeeModal);
            Thread.Sleep(1000);
        }

        public bool? IsAttendeeRegistered(string attendeeName)
        {
            var list = new List<string>();
            foreach (var item in attendeesName)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }
            if (list.Contains(attendeeName))
            {
                return true;
            }
            return false;
        }

        public void DropAttendee(string attendeeName)
        {
            var list = new List<string>();
            foreach (var item in attendeesName)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }

            var listDrop = new List<IWebElement>();
            foreach (var item in dropAttendeeButton)
            {
                listDrop.Add(item);
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(attendeeName))
                {
                    ScrollToElement(listDrop[i]);
                    listDrop[i].Click();
                    AcceptAlert();
                    Thread.Sleep(5000);
                }
            }
        }
    }
}
