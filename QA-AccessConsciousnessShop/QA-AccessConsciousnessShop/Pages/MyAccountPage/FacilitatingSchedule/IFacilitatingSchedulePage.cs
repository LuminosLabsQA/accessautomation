﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.FacilitatingSchedule
{
    interface IFacilitatingSchedulePage
    {
        void GoToClassEdit(string className);
        void GoToClassRoster(string className);
        void AddAttendee(string email);
        bool? IsAttendeeRegistered(string attendeeName);
        void DropAttendee(string attendeeName);
    }
}
