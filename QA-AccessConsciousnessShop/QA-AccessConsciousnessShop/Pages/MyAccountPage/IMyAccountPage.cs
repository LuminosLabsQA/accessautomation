﻿using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;
using SeleniumCommon.Objects.User.CreditCard;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage
{
    public interface IMyAccountPage
    {
        void GoToPublicProfile();
        void GoToMemberships();
        DesktopMyAccountPage GoToChangeCredentials();
        bool IsUserLoggedIn();
        DesktopMyAccountPage ClickDependentsLink();
        void ChangeToPasswordTab();
        bool IsChangeCredentialsSuccessMessageDisplayed();
        void ChangeUsername(IUser user);
        DesktopMyAccountPage ClickAddDependentsButton();
        DesktopMyAccountPage AddNewRandomDependentInformation(IAccessUsers user);
        DesktopMyAccountPage GoToContactInfo();
        DesktopMyAccountPage ChangeUserContactInfo(IAccessUsers user);
        bool? IsContactInfoSaveSuccessMessageDisplayed();
        void ClickContactInfoSave();
        DesktopMyAccountPage ClickSaveDependentButton();
        bool? IsAddedDependentNameCorrect(string dependentFullName);
        void GoToPaymentsDue();
        void GoToCreateAClass();
        void GoToUpcomingClasses();
        void GoToFacilitatingSchedule();
    }
}
