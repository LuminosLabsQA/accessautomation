﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User.CreditCard;
using SeleniumCommon.Page.BasePage;
using SeleniumCommon.Utilities;
using SeleniumExtras.PageObjects;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.PaymentsDue
{
    class DesktopPaymentsDuePage : BasePage, IPaymentsDuePage
    {
        IWebDriver _driver;

        [FindsBy(How = How.Name, Using = "AmountToPay")]
        private IWebElement AmountToPayField { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@ng-bind='class.DueAmountFormatted']")]
        private IWebElement DueAmountField { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-click='goToCheckout(paymentsDueForm)']")]
        private IWebElement GoToCheckoutButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='hidePaymentSelection()']")]
        private IWebElement CreditCardPaymentSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='payPalSelected()']")]
        private IWebElement PayPalMethodSelect { get; set; }

        [FindsBy(How = How.Id, Using = "cardType")]
        private IWebElement CardTypeDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "nameOnCard")]
        private IWebElement NameOnCard { get; set; }

        [FindsBy(How = How.Id, Using = "cardNumber")]
        private IWebElement CardNumber { get; set; }

        [FindsBy(How = How.Id, Using = "ccCVV")]
        private IWebElement CVVField { get; set; }

        [FindsBy(How = How.Id, Using = "cardExpirationMonth")]
        private IWebElement ExpirationMonthDropdown { get; set; }

        [FindsBy(How = How.Id, Using = "cardExpirationYear")]
        private IWebElement ExpirationYearDropdown { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-xs-12 col-md-9 col-md-push-3']//button[@type='submit']")]
        private IWebElement ClickPaymentInfoContinueButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@ng-click='paymentConfirmationModel.submitAndPay()']")]
        private IWebElement SubmitAndPayButton { get; set; }

        [FindsBy(How = How.Id, Using = "paymentConfirmation")]
        private IWebElement PaymentConfirmationMessage { get; set; }

        [FindsBy(How = How.Id, Using = "processingPaymentModal")]
        private IWebElement PaymentProcessingOverlay { get; set; }

        public DesktopPaymentsDuePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void EnterAmount(string amount)
        {
            AmountToPayField.SendKeys(amount);
        }

        public float GetClassDuePrice()
        {
            var charArray = new char[1] { ' ' };
            float price = Utilities.AccessUtilities.ExtractPriceValueFromString(DueAmountField, charArray, 0);
            return price;
        }

        public void ClickGoToCheckout()
        {
            GoToCheckoutButton.Click();
        }

        public void SelectCreditCardPaymentMethod()
        {
            WaitForVisibleElement(_driver, CreditCardPaymentSelect);
            CreditCardPaymentSelect.Click();
        }

        public void SelectPayPalPaymentMethod()
        {
            WaitForVisibleElement(_driver, PayPalMethodSelect);
            PayPalMethodSelect.Click();
        }

        public void EnterPaymentInformation(IAccessUsers user, ICreditCard card)
        {
            SelectCardType(EnumUtils.StringValueOf(card.Type));
            SetNameOnCard(user.FirstName + " " + user.LastName);
            SetCardNumber(EnumUtils.StringValueOf(card.CardNumber));
            SetCVV(EnumUtils.StringValueOf(card.Cvv));
            SetExpirationMonth("10");
            SetExpirationYear(card.ExpirationYear);
        }

        private void SelectCardType(string cardType)
        {
            SelectElement select = new SelectElement(CardTypeDropdown);
            select.SelectByText(cardType);
        }

        private void SetNameOnCard(string nameOnCard)
        {
            NameOnCard.SendKeys(nameOnCard);
        }

        private void SetCardNumber(string cardNumber)
        {
            CardNumber.SendKeys(cardNumber);
        }

        private void SetCVV(string cvv)
        {
            CVVField.SendKeys(cvv);
        }

        private void SetExpirationMonth(string expMonth)
        {
            SelectElement select = new SelectElement(ExpirationMonthDropdown);
            select.SelectByText(expMonth);
        }

        private void SetExpirationYear(string expYear)
        {
            SelectElement select = new SelectElement(ExpirationYearDropdown);
            select.SelectByText(expYear);
        }

        public void ClickContinue()
        {
            ClickPaymentInfoContinueButton.Click();
        }

        public void ClickSubmitAndPay()
        {
            WaitForVisibleElement(_driver, SubmitAndPayButton);
            SubmitAndPayButton.Click();
        }

        public bool? IsPaymentSuccessMessageDisplayed()
        {
            WaitForInvisibilityOfElement(_driver, PaymentProcessingOverlay);
            WaitForVisibleElement(_driver, PaymentConfirmationMessage);
            return PaymentConfirmationMessage.Displayed;
        }

        public void WaitForPaymentSuccessMessageHidden()
        {
            WaitForInvisibilityOfElement(_driver, PaymentProcessingOverlay);
            WaitForInvisibilityOfElement(_driver, PaymentConfirmationMessage);
        }

        public override void GoToCart()
        {
            throw new System.NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new System.NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new System.NotImplementedException();
        }
    }
}
