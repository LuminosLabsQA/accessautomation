﻿using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User.CreditCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.PaymentsDue
{
    interface IPaymentsDuePage
    {
        void EnterAmount(string amount);
        void ClickGoToCheckout();
        void SelectCreditCardPaymentMethod();
        void EnterPaymentInformation(IAccessUsers user, ICreditCard card);
        void ClickContinue();
        void ClickSubmitAndPay();
        float GetClassDuePrice();
        bool? IsPaymentSuccessMessageDisplayed();
        void WaitForPaymentSuccessMessageHidden();
        void SelectPayPalPaymentMethod();
    }
}
