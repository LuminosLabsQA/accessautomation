﻿using OpenQA.Selenium;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;
using System;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.PublicProfilePage
{
    class DesktopPublicProfilePage : BasePage, IPublicProfilePage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//div[@class='col-sm-6']//h2")]
        IWebElement PublicProfilePageTitle;

        public DesktopPublicProfilePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(driver, this);
        }

        public string GetPublicProfilePageTitle()
        {
            WaitForVisibleElement(_driver, PublicProfilePageTitle);
            return PublicProfilePageTitle.Text;
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
