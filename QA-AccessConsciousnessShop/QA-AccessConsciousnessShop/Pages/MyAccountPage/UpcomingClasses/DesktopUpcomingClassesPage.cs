﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumCommon.Page.BasePage;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.UpcomingClasses
{
    class DesktopUpcomingClassesPage : BasePage, IUpcomingClassesPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='cancelClass(class, $index)']")]
        IList<IWebElement> cancelButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//td[@class='col-sm-3']")]
        IList<IWebElement> classNameCells { get; set; }

        [FindsBy(How = How.XPath, Using = "//h4[@ng-show='noPrerequsitesClasses.length + prerequisitesClasses.length <= 0']")]
        IWebElement noClassesMessage { get; set; }


        public DesktopUpcomingClassesPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public void CancelClass(string className)
        {
            Thread.Sleep(2000);
            var list = new List<string>();
            foreach (var item in classNameCells)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(className))
                {
                    IList<IWebElement> el = _driver.FindElements(By.XPath("//a[@ng-click='cancelClass(class, $index)']"));
                    el[i].Click();
                    break;
                }
            }
            AcceptAlert();
        }

        public string NoClassesMessageDisplayed()
        {
            WaitForVisibleElement(_driver, noClassesMessage);
            string message = noClassesMessage.Text;
            return message;
        }

        public bool? ClassNoLongerInList(string className)
        {
            Thread.Sleep(2000);
            var list = new List<string>();
            foreach (var item in classNameCells)
            {
                string stringText = item.GetAttribute("innerText");
                list.Add(stringText);
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].Equals(className))
                {
                    return true;
                }
            }
            return false;
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }
    }
}
