﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_AccessConsciousnessShop.Pages.MyAccountPage.UpcomingClasses
{
    interface IUpcomingClassesPage
    {
        void CancelClass(string className);
        string NoClassesMessageDisplayed();
        bool? ClassNoLongerInList(string className);
    }
}
