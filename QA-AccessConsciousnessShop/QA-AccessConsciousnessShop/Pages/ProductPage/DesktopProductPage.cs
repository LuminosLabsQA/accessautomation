﻿using System;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Threading;

namespace QA_AccessConsciousnessShop.Pages.ProductPage
{
    class DesktopProductPage : SeleniumCommon.Page.ProductPage.ProductPage, IProductPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//input[@class='btn btn-lg submitButton']")]
        private IWebElement AddToCartButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='btn btn-lg']")]
        private IWebElement AddToCartButtonCertification { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='buyingOptions']/form/input")]
        private IWebElement AlternateAddToCartButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@ng-click='addToWishList()']")]
        private IWebElement AddToWishListButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='productCol1']//h2")]
        private IWebElement ProductName { get; set;}

        [FindsBy(How = How.XPath, Using = "//td[@class='ng-binding']//span")]
        private IWebElement ProductPrice { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@ng-options='q for q in availableQuantities']")]
        private IWebElement ProductQTY { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='hidden-xs ng-scope ng-binding']")]
        private IWebElement CartButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='success']")]
        private IWebElement ATCSuccessMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='productCol1']//div//span")]
        private IWebElement AlternatePageSaleBadge { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='final-price']")]
        private IWebElement AlternatePageDiscountedPriceLabel { get; set; }

        [FindsBy(How = How.XPath, Using = "//tbody//tr//td[2]//span//div//div")]
        private IWebElement RegularPageDiscountedPriceLabel { get; set; }

        [FindsBy(How = How.XPath, Using = "//strong[@class='text-red']")]
        private IWebElement DiscountPrice { get; set; }

        [FindsBy(How = How.XPath, Using = "//strong[@class='ng-binding ng-scope']")]
        private IWebElement RetailPrice { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='text-red ng-binding']")]
        private IWebElement WholeSalePrice { get; set; }


        public DesktopProductPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public override void AddToCart()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, AddToCartButton);
            AddToCartButton.Click();
            WaitForPageToLoad();
        }

        public void AddToCartCertification()
        {
            WaitForPageToLoad();
            ScrollToElement(AddToCartButtonCertification);
            WaitForVisibleElement(_driver, AddToCartButtonCertification);
            AddToCartButtonCertification.Click();
            WaitForPageToLoad();
        }

        public void AlternateAddToCart()
        {
            WaitForVisibleElement(_driver, AlternateAddToCartButton);
            AlternateAddToCartButton.Click();
            WaitForPageToLoad();
        }

        public override void AddToWishList()
        {
            WaitForVisibleElement(_driver, AddToWishListButton);
            AddToWishListButton.Click();
        }

        public override string GetProductCode()
        {
            throw new NotImplementedException();
        }

        public override string GetProductName()
        {
            WaitForVisibleElement(_driver, ProductName);
            return ProductName.Text;
        }


        public override float GetProductPrice()
        {
            var charArray = new char[2] { '$', ' ' };
            Thread.Sleep(1000);
            if (IsDiscountPrice() == true)
            {
                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(DiscountPrice, charArray, 1);
                return price;
            }
            else if (IsRetailPrice() == true)
            {
                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(RetailPrice, charArray, 1);
                return price;
            }
            else
            {
                float price = Utilities.AccessUtilities.ExtractPriceValueFromString(WholeSalePrice, charArray, 1);
                return price;
            }
        }

        public override int GetQuantity()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, ProductQTY);
            return int.Parse(ProductQTY.GetAttribute("value"));
        }

        public override string GetStockStatus()
        {
            throw new NotImplementedException();
        }

        public override void GoToCart()
        {
            Thread.Sleep(4000);
            CartButton.Click();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }

        public override void SetQuantity(int quantity)
        {
            WaitForVisibleElement(_driver, ProductQTY);
            ProductQTY.SendKeys(quantity.ToString());
        }

        public bool IsATCSuccessMessageDisplayed()
        {
            WaitForPageToLoad();
            return WaitForVisibleElement(_driver, ATCSuccessMessage);
        }

        public bool IsAlternateSaleBadgeDisplayed()
        {
            WaitForPageToLoad();
            return WaitForVisibleElement(_driver, AlternatePageSaleBadge);
        }

        public string GetAlternateDiscountedPriceLabel()
        {
            WaitForVisibleElement(_driver, AlternatePageDiscountedPriceLabel);
            return AlternatePageDiscountedPriceLabel.Text;
        }

        public string GetRegularDiscountedPriceLabel()
        {
            WaitForVisibleElement(_driver, RegularPageDiscountedPriceLabel);
            return RegularPageDiscountedPriceLabel.Text;
        }

        public bool IsDiscountedPriceLableDisplayed()
        {
            return WaitForVisibleElement(_driver, RegularPageDiscountedPriceLabel);
        }

        public bool IsDiscountPrice()
        {
            return WaitForVisibleElement(_driver, DiscountPrice);
        }

        public bool IsRetailPrice()
        {
            return WaitForVisibleElement(_driver, RetailPrice);
        }

        public bool IsWholeSalePrice()
        {
            return WaitForVisibleElement(_driver, WholeSalePrice);
        }

    }
}
