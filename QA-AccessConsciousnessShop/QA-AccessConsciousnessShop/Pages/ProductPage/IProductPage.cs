﻿using OpenQA.Selenium;

namespace QA_AccessConsciousnessShop.Pages.ProductPage
{
    interface IProductPage : SeleniumCommon.Page.ProductPage.IProductPage
    {
        bool IsATCSuccessMessageDisplayed();
        void AlternateAddToCart();
        bool IsAlternateSaleBadgeDisplayed();
        string GetAlternateDiscountedPriceLabel();
        string GetRegularDiscountedPriceLabel();
        bool IsDiscountedPriceLableDisplayed();
        void AddToCartCertification();
    }
}
