﻿using System;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;


namespace QA_AccessConsciousnessShop.Pages.LandingPage
{
    class DesktopLandingPage : SeleniumCommon.Page.LandingPage.LandingPage, ILandingPage
    {
        IWebDriver _driver;

        [FindsBy(How = How.XPath, Using = "//p[@class='text-center']")]
        private IWebElement SplashModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@class='btn btn-green btn-block btn-lg']")]
        private IWebElement ContinueSplashModalButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='hidden-xs ng-scope ng-binding']")]
        private IWebElement CartButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//h1[@class='header']")]
        private IWebElement EmptyShoppingCartMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='splashModal']//button[@class='close']")]
        public IWebElement CloseSplashModalButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='loginModal']//button[@aria-label='Close']")]
        public IWebElement CloseLoginModal { get; set; }

        [FindsBy(How = How.XPath, Using = ("//a[@li='isAuthenticated']"))]
        private IWebElement LoginLinkHeader { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-md-9']//a[@href='/en/']")]
        private IWebElement BackToAccessLinkHeader { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-md-3']//input[@id='search']")]
        private IWebElement ContenSearchHeaderField { get; set; }

        [FindsBy(How = How.XPath, Using = "//form[@class='search ng-pristine ng-valid']//input[@id='search']")]
        private IWebElement ContentSearchFooterField { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='footerCol1']")]
        private IWebElement IsFooterLogoAndTextDisplayed { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-md-5 col-sm-8']")]
        private IWebElement IsFooterSocialMediaSearchFieldAndTextDisplayed { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-md-2 col-sm-4 col-sm-offset-4 col-md-offset-0']")]
        private IWebElement AreHelpInfoLinksDisplayed { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-md-2 col-sm-4']")]
        private IWebElement AreAccountLinksDisplayed { get; set; }

        [FindsBy(How = How.XPath, Using = "//img[@class='shopLogo']")]
        private IWebElement HeaderAccessLogo { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@class='utilityNav navMobile']//a[@href='#']")]
        private IWebElement ViewTopics { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@class='utilityNav navMobile']//a[@href='']")]
        private IWebElement ViewAuthors { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/en/access-shop/wish-list/']")]
        private IWebElement Wishlist { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='shopSearchInput']//input[@id='search']")]
        private IWebElement MainSearchField { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@class='nav navbar-nav sm']")]
        private IWebElement NavigationMenuBlock { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='MainContent']//div[@class='block heroimagesliderblock  ']")]
        private IWebElement TopContainer { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='MainContent']//div[@class='container']//div[@class='row']")]
        private IWebElement CenterContainer { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-sm-12']//div[@class='row']")]
        private IWebElement BottomContainer { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@class='open']//a[@href='/en/access-shop/are-you-happy-topic-page/']")]
        private IWebElement SelectTopic { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@class='open']//a[@href='/en/access-shop/search-products/#?PageSize=10&SortBy=_score&Descending&CurrentPageNumber=1&AuthorNames=Gary%20%20M.%20Douglas&AuthorNames=Gary%20M.%20Douglas']")]
        private IWebElement SelectAuthor { get; set; }


        public DesktopLandingPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            SeleniumExtras.PageObjects.PageFactory.InitElements(_driver, this);
        }

        public bool IsSpashModalVisible()
        {
            return WaitForVisibleElement(_driver, SplashModal);
        }

        public void ClickContinueToShopButton()
        {
            WaitForVisibleElement(_driver, ContinueSplashModalButton);
            ContinueSplashModalButton.Click();
            WaitForInvisibilityOfElement(_driver, SplashModal);
        }

        public void ClickCartButton()
        {
           CartButton.Click();
        }

        public string VisitEmptyCart()
        {
            return EmptyShoppingCartMessage.Text;
        }

        public ILandingPage CloseSplashModal()
        {
            WaitForPageToLoad();
            WaitForVisibleElement(_driver, SplashModal);
            CloseSplashModalButton.Click();
            WaitForInvisibilityOfElement(_driver, SplashModal);
            return this;
        }

        public void ClickBackToAccessHeaderLink()
        {
            BackToAccessLinkHeader.Click();
        }

        public void SearchForTerm(bool headerSearchField, string searchTerm)
        {
            IWebElement searchField;
            if (headerSearchField) searchField = ContenSearchHeaderField;
            else searchField = ContentSearchFooterField;

            searchField.Clear();
            searchField.SendKeys(searchTerm);
            searchField.SendKeys(Keys.Return);
            WaitForPageToLoad();
        }
        
        public ILandingPage LoginCredentials(IWebElement credentialsField, string credentialsInput)
        {
            credentialsField.Clear();
            credentialsField.SendKeys(credentialsInput);
            return this;
        }

        public bool IsFooterSectionContaingExpectedElements()
        {
            return WaitForVisibleElement(_driver, IsFooterSocialMediaSearchFieldAndTextDisplayed)
                && WaitForVisibleElement(_driver, IsFooterSocialMediaSearchFieldAndTextDisplayed)
                && WaitForVisibleElement(_driver, AreAccountLinksDisplayed)
                && WaitForVisibleElement(_driver, AreHelpInfoLinksDisplayed);

        }

        public bool IsHeaderSectionContaingExpectedElements()
        {
            return WaitForVisibleElement(_driver, HeaderAccessLogo)
                && WaitForVisibleElement(_driver, ViewTopics)
                && WaitForVisibleElement(_driver, ViewAuthors)
                && WaitForVisibleElement(_driver, Wishlist)
                && WaitForVisibleElement(_driver, MainSearchField)
                && WaitForVisibleElement(_driver, NavigationMenuBlock);
        }

        public bool AreExpectedElementsDisplayed()
        {
            return WaitForVisibleElement(_driver, TopContainer)
                && WaitForVisibleElement(_driver, CenterContainer)
                && WaitForVisibleElement(_driver, BottomContainer);
        }

        public void ClickWishlist()
        {
            Wishlist.Click();
        }
        public ILandingPage ClickViewTopics()
        {
            WaitForPageToLoad();
            WaitForInvisibilityOfElement(_driver, SplashModal);
            ViewTopics.Click();
            return this;
        }
        public ILandingPage ClickViewAuthors()
        {
            WaitForPageToLoad();
            WaitForInvisibilityOfElement(_driver, SplashModal);
            ViewAuthors.Click();
            return this;
        }

        public void ClickSelectTopics()
        {
            SelectTopic.Click();
        }

        public void ClickSelectAuthor()
        {
            SelectAuthor.Click();
        }

        public override void GoToLogin()
        {
            WaitForInvisibilityOfElement(_driver, CloseSplashModalButton);
            WaitForVisibleElement(_driver, LoginLinkHeader);
            LoginLinkHeader.Click();
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }

        public string GetHelpAndInfoFooterText()
        {
            return AreHelpInfoLinksDisplayed.Text;
        }
        public string GetAccountFooterText()
        {
            return AreAccountLinksDisplayed.Text;
        }
    }
}
