﻿using OpenQA.Selenium;

namespace QA_AccessConsciousnessShop.Pages.LandingPage
{
    interface ILandingPage : SeleniumCommon.Page.LandingPage.ILandingPage
    {
        bool IsSpashModalVisible();
        ILandingPage CloseSplashModal();
        void ClickContinueToShopButton();
        void ClickCartButton();
        string VisitEmptyCart();
        void ClickBackToAccessHeaderLink();
        void SearchForTerm(bool headerSearchField, string searchTerm);
        bool IsFooterSectionContaingExpectedElements();
        bool IsHeaderSectionContaingExpectedElements();
        bool AreExpectedElementsDisplayed();
        void ClickWishlist();
        ILandingPage ClickViewTopics();
        ILandingPage ClickViewAuthors();
        void ClickSelectTopics();
        void ClickSelectAuthor();
        ILandingPage LoginCredentials(IWebElement credentialsField, string credentialsInput);
        string GetHelpAndInfoFooterText();
        string GetAccountFooterText();
    }
}
