﻿using OpenQA.Selenium;
using System;

namespace QA_AccessConsciousnessShop.PageFactory.LandingPage
{
    class MobileLandingPage : SeleniumCommon.Page.LandingPage.LandingPage
    {
        IWebDriver _driver;

        public MobileLandingPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public void ClickCartButton()
        {
            throw new NotImplementedException();
        }

        public void ClickContinueToShopButton()
        {
            throw new NotImplementedException();
        }

        public void ClickSignInHere()
        {
            throw new NotImplementedException();
        }

        public override void GoToCart()
        {
            throw new NotImplementedException();
        }

        public override void GoToLogin()
        {
            throw new NotImplementedException();
        }

        public override void GoToMyAccount()
        {
            throw new NotImplementedException();
        }

        public bool IsLoginModalDisplayed()
        {
            throw new NotImplementedException();
        }

        public bool? IsSpashModalVisible()
        {
            throw new NotImplementedException();
        }
    }
}
