﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using RelevantCodes.ExtentReports;
using SeleniumCommon.ConfigManager;
using SeleniumCommon.Logging;
using SeleniumCommon.Utilities.Snapshot;

namespace QA_AccessConsciousnessShop
{
    public class SeleniumBase
    {
        protected IWebDriver Driver;

        protected ExtentTest ExtentLogger;

        [SetUp]
        public void SetUp()
        {
            Driver = new BrowserFactory().SetupDriver();
            Driver.Navigate().GoToUrl(TestConfig.BaseApplicationUrl + TestConfig.ResourcePathUrl);

            StartLogger(TestContext.CurrentContext.Test.Name, TestContext.CurrentContext.Test.ClassName);
        }

        [TearDown]
        public void TearDown()
        {          
            Console.WriteLine("===============================================================================");
            Console.WriteLine("==============================  TEAR DOWN  ====================================");
            Console.WriteLine("===============================================================================");

            LogTestResult(TestContext.CurrentContext.Result.Outcome);

            Driver.Close();
            Driver.Quit();

            ReportingHandler.Reporter.EndTest(ExtentLogger);
        }

        [OneTimeTearDown]
        public void FinalTearDown()
        {
            Console.WriteLine("===============================================================================");
            Console.WriteLine("============================  ONE TIME TEAR DOWN  =============================");
            Console.WriteLine("===============================================================================");

            ReportFactory.CloseReporter();
        }

        private void StartLogger(string testName, string className)
        {
            string name = className;
            name = name.Substring(testName.IndexOf('.') + 1);
            name = name + " - " + testName;


            ExtentLogger = ReportingHandler.Reporter.StartTest(name, testName);
            ExtentLogger.Log(LogStatus.Info, "Starting test " + testName);
        }

        private void LogTestResult(ResultState outcome)
        {
            if (!Equals(outcome, ResultState.Success))
            {
                ExtentLogger.Log(LogStatus.Info, TestContext.CurrentContext.Result.Message);
            }

            if (Equals(outcome, ResultState.Success))
            {
                ExtentLogger.Log(LogStatus.Pass, "Test " + TestContext.CurrentContext.Test.Name + " passed");
            }
            else if (Equals(outcome, ResultState.Failure))
            {
                LogResultWithImage(LogStatus.Fail);
            }
            else if (Equals(outcome, ResultState.Inconclusive))
            {
                LogResultWithImage(LogStatus.Unknown);
            }
            else
            {
                LogResultWithImage(LogStatus.Error);
            }
        }

        private void LogResultWithImage(LogStatus logStatus)
        {
            string file = new Snapshot().Snap(Driver, TestContext.CurrentContext.Test.Name);
            string image = ExtentLogger.AddScreenCapture(file);
            ExtentLogger.Log(logStatus, "Test " + TestContext.CurrentContext.Test.Name + " " + logStatus, image);
        }
    }   
}
