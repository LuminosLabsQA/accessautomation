﻿using NUnit.Framework;
using OpenQA.Selenium;
using QA_AccessConsciousnessShop.Factories;
using QA_AccessConsciousnessShop.Objects;
using QA_AccessConsciousnessShop.Objects.OccurrenceInstances;
using QA_AccessConsciousnessShop.Pages;
using QA_AccessConsciousnessShop.Pages.CartPage;
using QA_AccessConsciousnessShop.Pages.ClassCalendarPage;
using QA_AccessConsciousnessShop.Pages.ClassesHomePAge;
using QA_AccessConsciousnessShop.Pages.ClassProductPage;
using QA_AccessConsciousnessShop.Pages.ClassProductPage.DesktopRegistrationPage;
using QA_AccessConsciousnessShop.Pages.ClassProductPage.RegistrationPage;
using QA_AccessConsciousnessShop.Pages.FacilitatorsPage;
using QA_AccessConsciousnessShop.Pages.LoginPage;
using QA_AccessConsciousnessShop.Pages.MyAccountPage;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClass;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.CreateAClassScreen;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.FacilitatingSchedule;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.PaymentsDue;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.PublicProfilePage;
using QA_AccessConsciousnessShop.Pages.MyAccountPage.UpcomingClasses;
using SeleniumCommon.ConfigManager;
using SeleniumCommon.Objects.User.CreditCard;
using System;
using System.Threading;

namespace QA_AccessConsciousnessShop.Tests
{
    [TestFixture]
    [Category("ClassesPre_ReleaseRegressionTests")]
    class ClassesPre_ReleaseRegressionTests : SeleniumBase
    {
        public object IFacilitatorPage { get; private set; }

        [Test, Order(1)]
        public void WhenTheUserReachesTheAccessSite_HeLogsInAndThenLogsOut()
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);

            login.GoToLogin();
            login.Login(user);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            Assert.True(myAccount.IsUserLoggedIn());
            login.ClickLogoutLink();
            Assert.False(myAccount.IsUserLoggedIn());
        }

        [Test, Order(2)]
        public void WhenUserLogsIn_HeChangesHisUsername()
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            IAccessUsers userThatExists = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            IAccessUsers userThatDoesNotExist = new UserBuilder().GetUser(UserBuilder.UserTypes.UserThatDoesNotExist);

            login.GoToLogin();
            login.Login(userThatExists);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToChangeCredentials();
            myAccount.ChangeUsername(userThatDoesNotExist);
            Driver.Navigate().Refresh();
            myAccount.ChangeUsername(userThatExists); //this step changes back the user credentials to an existing one to be used in order automated tests
            Assert.True(myAccount.IsChangeCredentialsSuccessMessageDisplayed());
        }

        [Test, Order(3)]
        public void WhenUserLogsIn_HeChangesHisPassword()
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);

            login.GoToLogin();
            login.Login(user);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToChangeCredentials()
                .ChangePassword(user);
            Assert.True(myAccount.IsChangeCredentialsSuccessMessageDisplayed());
        }

        [Test, Order(4)]
        public void WhenUserLogsIn_HeCreatesARandomDependent()
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            IAccessUsers userWithDependent = new UserBuilder().GetUser(UserBuilder.UserTypes.UserWithDependents);
            IAccessUsers newRandomDependent = new UserBuilder().GetUser(UserBuilder.UserTypes.NewRandomUser);

            login.GoToLogin();
            login.Login(userWithDependent);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.ClickDependentsLink()
                .ClickAddDependentsButton()
                .AddNewRandomDependentInformation(newRandomDependent)
                .ClickSaveDependentButton();
            string dependentFullName = newRandomDependent.FirstName + " " + newRandomDependent.LastName;
            Assert.True(myAccount.IsAddedDependentNameCorrect(dependentFullName));
        }

        [Test, Order(5)]
        public void WhenUserLogsIn_HeChangesHisContactInfo()
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            login.GoToLogin();
            IAccessUsers randomUser = new UserBuilder().GetUser(UserBuilder.UserTypes.NewRandomUser);
            IAccessUsers regularUser = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            login.Login(regularUser);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToContactInfo()
                .ChangeUserContactInfo(randomUser)
                .ClickContactInfoSave();
            Assert.True(myAccount.IsContactInfoSaveSuccessMessageDisplayed());
            Driver.Navigate().Refresh();
            myAccount.ChangeUserContactInfo(regularUser)
                .ClickContactInfoSave();
            Assert.True(myAccount.IsContactInfoSaveSuccessMessageDisplayed());
        }

        [TestCase(UserBuilder.UserTypes.ExistingRegularUserEurope), Order(16)]
        [TestCase(UserBuilder.UserTypes.ExistingRegularUserUSA)]
        [TestCase(UserBuilder.UserTypes.ExistingRegularUserAUS)]
        public void WhenUserLogsIn_PerformsACreditCardPayment(UserBuilder.UserTypes getUser)
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            login.GoToLogin();
            IAccessUsers regularUser = new UserBuilder().GetUser(getUser);
            login.Login(regularUser);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToPaymentsDue();
            IPaymentsDuePage paymentsDue = new DesktopPaymentsDuePage(Driver);
            paymentsDue.EnterAmount("100.00");
            float dueAmmountBeforPayment = paymentsDue.GetClassDuePrice();
            paymentsDue.ClickGoToCheckout();
            paymentsDue.SelectCreditCardPaymentMethod();
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.VISA);
            paymentsDue.EnterPaymentInformation(regularUser, card);
            paymentsDue.ClickContinue();
            paymentsDue.ClickSubmitAndPay();
            Assert.True(paymentsDue.IsPaymentSuccessMessageDisplayed());
            paymentsDue.WaitForPaymentSuccessMessageHidden();
            myAccount.GoToPaymentsDue();
            float dueAmountAfterPayment = paymentsDue.GetClassDuePrice();
            Assert.True(dueAmountAfterPayment == dueAmmountBeforPayment - 100);
        }

        [TestCase(UserBuilder.UserTypes.ExistingRegularUserEurope), Order(17)]
        [TestCase(UserBuilder.UserTypes.ExistingRegularUserUSA)]
        [TestCase(UserBuilder.UserTypes.ExistingRegularUserAUS)]
        public void WhenUserLogsIn_PerformsAPayPalClassPayment(UserBuilder.UserTypes getUser)
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            login.GoToLogin();
            IAccessUsers regularUser = new UserBuilder().GetUser(getUser);
            login.Login(regularUser);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToPaymentsDue();
            IPaymentsDuePage paymentsDue = new DesktopPaymentsDuePage(Driver);
            paymentsDue.EnterAmount("10.00");
            float dueAmmountBeforPayment = paymentsDue.GetClassDuePrice();
            paymentsDue.ClickGoToCheckout();
            paymentsDue.SelectPayPalPaymentMethod();
            IAccessUsers payPalUser = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingPayPalUser);
            ICartPage payPal = new DesktopCartPage(Driver);
            payPal.PayPalLogin(payPalUser)
                .ClickPayPalContinuedButton();
            if (getUser == UserBuilder.UserTypes.ExistingRegularUserEurope || getUser == UserBuilder.UserTypes.ExistingRegularUserAUS)
            {
                payPal.ClickPayPalContinuedButton();
            }
            paymentsDue.ClickSubmitAndPay();
            Assert.True(paymentsDue.IsPaymentSuccessMessageDisplayed());
            paymentsDue.WaitForPaymentSuccessMessageHidden();
            myAccount.GoToPaymentsDue();
            float dueAmountAfterPayment = paymentsDue.GetClassDuePrice();
            Assert.True(dueAmountAfterPayment == dueAmmountBeforPayment - 10);
        }

        [TestCase(OccurrenceFactory.OccurrenceType.ClassOccurrence), Order(6)]
        [TestCase(OccurrenceFactory.OccurrenceType.Event)]
        [TestCase(OccurrenceFactory.OccurrenceType.Teleseries)]
        public void WhenUserLogsIn_HeCreatesAClassOccurrence(OccurrenceFactory.OccurrenceType getType)
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            login.GoToLogin();
            IAccessUsers adminUser = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingAdminUser);
            login.Login(adminUser);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToCreateAClass();
            ICreateAClassPage createClass = new DesktopCreateAClassPage(Driver);
            IOccurrence occurrenceType = new OccurrenceFactory().GetOccurrence(getType);
            createClass.CreateAdminOccurrence(occurrenceType);
            createClass.ClickPostToPublicCalendarButton();
            Assert.True(createClass.IsPostToPublicCalendarSuccessMessageDisplayed());
        }

        [Test, Order(7)]
        public void WhenUserLogsIn_HeCreatesAFacilitatorClassOccurrence()
        {
            ILoginPage login = new DesktopLoginPage(Driver);
            login.GoToLogin();
            IAccessUsers facilitatorUser = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
            login.Login(facilitatorUser);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToCreateAClass();
            ICreateAClassPage createClass = new DesktopCreateAClassPage(Driver);
            IOccurrence occurrenceType = new OccurrenceFactory().GetOccurrence(OccurrenceFactory.OccurrenceType.FacilitatorClassOccurrence);
            createClass.CreateFacilitatorOccurrence(occurrenceType);
            createClass.ClickPostToPublicCalendarButton();
            IPaymentsDuePage paymentsDue = new DesktopPaymentsDuePage(Driver);
            paymentsDue.SelectCreditCardPaymentMethod();
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.VISA);
            paymentsDue.EnterPaymentInformation(facilitatorUser, card);
            paymentsDue.ClickContinue();
            paymentsDue.ClickSubmitAndPay();
            Assert.True(paymentsDue.IsPaymentSuccessMessageDisplayed());
        }

        [Test, Order(8)]
        public void WhenUserLogsIn_HeRegistersToAClassOccurrenceWithLocationDiscountAndCancelsIt()
        {
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            loginPage.GoToLogin();
            IAccessUsers registrationUser = new UserBuilder().GetUser(UserBuilder.UserTypes.RegistrationUser);
            loginPage.Login(registrationUser);

            Driver.Navigate().GoToUrl(TestConfig.ClassCalendarPage);

            IClassCalendar calendar = new DesktopClassCalendar(Driver);
            calendar.SearchByName(TestConfig.ClassName);
            calendar.GoToClassPDP(TestConfig.ClassName, " - " + DateTime.Today.AddYears(1).ToString("dd MMM yyyy"));

            IClassProductPage classProductPage = new DesktopClassProductPage(Driver);
            double classPrice = classProductPage.GetClassPrice("EUR");

            IOccurrence classOccurrence = new OccurrenceFactory().GetOccurrence(OccurrenceFactory.OccurrenceType.ClassOccurrence);
            double locationDiscount = classOccurrence.Discount;
            double classOriginalPrice = classOccurrence.ClassPrice;

            Assert.AreEqual(classPrice, classOriginalPrice - (classOriginalPrice * locationDiscount));

            classProductPage.NavigateToRegistrationPage();
            IRegistrationPage registrationPage = new DesktopRegistrationPage(Driver);
            registrationPage.AgreePolicies();
            registrationPage.ClickAddToMyClassesButton();
            Assert.True(registrationPage.IsRegistrationSuccessMessageDisplayed());

            IMyAccountPage myAccountPage = new DesktopMyAccountPage(Driver);
            myAccountPage.GoToUpcomingClasses();

            IUpcomingClassesPage upcomingClasses = new DesktopUpcomingClassesPage(Driver);
            upcomingClasses.CancelClass(TestConfig.ClassName);
            Assert.IsTrue(upcomingClasses.ClassNoLongerInList(TestConfig.ClassName), "The class was not deleted correctly.");
        }

        [Test, Order(9)]
        public void WhenUserLogsIn_HeRegistersToAClassTeleseriestWithAgeDiscountAndCancelsIt()
        {
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            loginPage.GoToLogin();
            IAccessUsers registrationUser = new UserBuilder().GetUser(UserBuilder.UserTypes.RegistrationUser);
            loginPage.Login(registrationUser);

            Driver.Navigate().GoToUrl(TestConfig.ClassCalendarPage);

            IClassCalendar calendar = new DesktopClassCalendar(Driver);
            calendar.SearchByName(TestConfig.TeleseriesName);
            calendar.GoToClassPDP(TestConfig.TeleseriesName, " - " + DateTime.Today.AddYears(1).ToString("dd MMM yyyy"));

            IClassProductPage classProductPage = new DesktopClassProductPage(Driver);
            double teleseriesPrice = classProductPage.GetClassPrice("AUD");

            IOccurrence teleseries = new OccurrenceFactory().GetOccurrence(OccurrenceFactory.OccurrenceType.Teleseries);
            double ageDiscount = teleseries.Discount;
            double teleseriesOriginalPrice = teleseries.ClassPrice;

            Assert.AreEqual(teleseriesPrice, teleseriesOriginalPrice - (teleseriesOriginalPrice * ageDiscount));

            classProductPage.NavigateToRegistrationPage();
            IRegistrationPage registrationPage = new DesktopRegistrationPage(Driver);
            registrationPage.AgreePolicies();
            registrationPage.ClickAddToMyClassesButton();
            Assert.True(registrationPage.IsRegistrationSuccessMessageDisplayed());

            IMyAccountPage myAccountPage = new DesktopMyAccountPage(Driver);
            myAccountPage.GoToUpcomingClasses();

            IUpcomingClassesPage upcomingClasses = new DesktopUpcomingClassesPage(Driver);
            upcomingClasses.CancelClass(TestConfig.TeleseriesName);
            Assert.IsTrue(upcomingClasses.ClassNoLongerInList(TestConfig.TeleseriesName), "The teleseries was not deleted correctly.");
        }

        [Test, Order(10)]
        public void WhenUserLogsIn_HeRegistersToAEventtWithRepeatDiscountAndCancelsIt()
        {
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            loginPage.GoToLogin();
            IAccessUsers registrationUser = new UserBuilder().GetUser(UserBuilder.UserTypes.RegistrationUser);
            loginPage.Login(registrationUser);

            Driver.Navigate().GoToUrl(TestConfig.ClassCalendarPage);

            IClassCalendar calendar = new DesktopClassCalendar(Driver);
            calendar.SearchByName(TestConfig.EventName);
            calendar.GoToClassPDP(TestConfig.EventName, " - " + DateTime.Today.AddYears(1).ToString("dd MMM yyyy"));

            IClassProductPage classProductPage = new DesktopClassProductPage(Driver);
            double eventPrice = classProductPage.GetClassPrice("USD");

            IOccurrence eventOccasion = new OccurrenceFactory().GetOccurrence(OccurrenceFactory.OccurrenceType.Event);
            double repeatDiscount = eventOccasion.Discount;
            double eventOriginalPrice = eventOccasion.ClassPrice;

            Assert.AreEqual(eventPrice, eventOriginalPrice - (eventOriginalPrice * repeatDiscount));

            classProductPage.NavigateToRegistrationPage();
            IRegistrationPage registrationPage = new DesktopRegistrationPage(Driver);
            registrationPage.AgreePolicies();
            registrationPage.ClickAddToMyClassesButton();
            Assert.True(registrationPage.IsRegistrationSuccessMessageDisplayed());

            IMyAccountPage myAccountPage = new DesktopMyAccountPage(Driver);
            myAccountPage.GoToUpcomingClasses();

            IUpcomingClassesPage upcomingClasses = new DesktopUpcomingClassesPage(Driver);
            upcomingClasses.CancelClass(TestConfig.EventName);
            Assert.IsTrue(upcomingClasses.ClassNoLongerInList(TestConfig.EventName), "The event was not deleted correctly.");
        }


        [Test, Order(11)]
        public void WhenUserLogsIn_HeOpensAClassInEditMode()
        {
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            loginPage.GoToLogin();
            IAccessUsers adminUser = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingAdminUser);
            loginPage.Login(adminUser);

            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToFacilitatingSchedule();

            IFacilitatingSchedulePage facilitatingSchedule = new DesktopFacilitatingSchedulePage(Driver);
            facilitatingSchedule.GoToClassEdit(TestConfig.ClassName);

            ICreateAClassPage createAClass = new DesktopCreateAClassPage(Driver);
            Assert.True(createAClass.IsEditEventPageDisplayed(), "Class was not opened in edit mode.");
        }

        [Test, Order(12)]
        public void WhenUserLogsIn_HeAddsAUserToAClassUsingTheClassRosterAndDropsHim()
        {
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            loginPage.GoToLogin();
            IAccessUsers adminUser = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingAdminUser);
            loginPage.Login(adminUser);

            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            myAccount.GoToFacilitatingSchedule();

            IFacilitatingSchedulePage facilitatingSchedule = new DesktopFacilitatingSchedulePage(Driver);
            facilitatingSchedule.GoToClassRoster(TestConfig.ClassName);
            facilitatingSchedule.AddAttendee("automationeurope@mail.com");

            Assert.True(facilitatingSchedule.IsAttendeeRegistered("Automation Europe"), "The user was not registered.");

            facilitatingSchedule.DropAttendee("Automation Europe");
            Driver.Navigate().Refresh();
            Assert.False(facilitatingSchedule.IsAttendeeRegistered("Automation Europe"));
        }

        [Test, Order(13)]
        public void WhenUserLogsIn_HeRegistersToAFacilitatorGlobalPriceClassAndDropsIt()
        {
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            loginPage.GoToLogin();
            IAccessUsers registrationUser = new UserBuilder().GetUser(UserBuilder.UserTypes.RegistrationUser);
            loginPage.Login(registrationUser);

            Driver.Navigate().GoToUrl(TestConfig.ClassCalendarPage);

            IClassCalendar calendar = new DesktopClassCalendar(Driver);
            calendar.SearchByName(TestConfig.FacilitatorClass);
            calendar.GoToClassPDP(TestConfig.FacilitatorClass, " - " + DateTime.Today.AddYears(1).ToString("dd MMM yyyy"));

            IClassProductPage classProductPage = new DesktopClassProductPage(Driver);
            double facilitatorClassPrice = classProductPage.GetClassPrice("RON");

            ClassOccurrence classOccurrence = new ClassOccurrence();

            IOccurrence facilitatorOccasion = new OccurrenceFactory().GetOccurrence(OccurrenceFactory.OccurrenceType.FacilitatorClassOccurrence);
            double discountLocation = facilitatorOccasion.Discount;
            double ageDiscount = facilitatorOccasion.SecondDiscount;
            double facilitatorOriginalPrice = facilitatorOccasion.ClassPrice;

            double classLocationDiscount = facilitatorOriginalPrice * discountLocation;
            double classAgeDiscount = (facilitatorOriginalPrice - classLocationDiscount) * ageDiscount;

            double discountedPrice = facilitatorOriginalPrice - classLocationDiscount - classAgeDiscount;
            int discountedPriceINT;
            discountedPriceINT = (int)discountedPrice;

            Assert.AreEqual(facilitatorClassPrice, discountedPriceINT);

            classProductPage.NavigateToRegistrationPage();
            IRegistrationPage registrationPage = new DesktopRegistrationPage(Driver);
            registrationPage.AgreePolicies();
            registrationPage.ClickAddToMyClassesButton();
            Assert.True(registrationPage.IsRegistrationSuccessMessageDisplayed());

            IMyAccountPage myAccountPage = new DesktopMyAccountPage(Driver);
            myAccountPage.GoToUpcomingClasses();

            IUpcomingClassesPage upcomingClasses = new DesktopUpcomingClassesPage(Driver);
            upcomingClasses.CancelClass(TestConfig.FacilitatorClass);
            Assert.IsTrue(upcomingClasses.ClassNoLongerInList(TestConfig.EventName), "The event was not deleted correctly.");
        }

        [Test, Order(14)]
        public void WhenUserAccessesTheWebsite_NavigatesToAPopHostPageAndVerifiesHostAreDisplayed()
        {
            Driver.Navigate().GoToUrl(TestConfig.POPHostPDP);

            IClassProductPage classProductPage = new DesktopClassProductPage(Driver);
            classProductPage.GoToPOPHostPage();

            IFacilitatorsPage facilitatorsPage = new DesktopFacilitatorsPage(Driver);
            facilitatorsPage.SearchByFacilitator("Pamela Gadson");

            string facilitatorName = facilitatorsPage.GetFacilitatorName();

            facilitatorsPage.GoToFacilitatorPublicProfile();

            IPublicProfilePage publicProfilePage = new DesktopPublicProfilePage(Driver);
            string publicProfileTitle = publicProfilePage.GetPublicProfilePageTitle();

            Assert.AreEqual(facilitatorName, publicProfileTitle, "Public profile page is not displayed.");
        }

        [Test, Order(15)]
        public void WhenUserAccessesTheWebsite_NavigatesToATheFacilitatorSearchPageAndAPublicProfile()
        {
            IClassesHomePage classesHomePage = new DesktopClassesHomePage(Driver);
            classesHomePage.GoToFacilitatorPage();

            IFacilitatorsPage facilitatorPage = new DesktopFacilitatorsPage(Driver);
            facilitatorPage.SearchByFacilitator("Automation Admin");
            string facilitatorName = facilitatorPage.GetFacilitatorName();
            facilitatorPage.GoToFacilitatorPublicProfile();

            IPublicProfilePage publicProfilePage = new DesktopPublicProfilePage(Driver);
            string publicProfileTitle = publicProfilePage.GetPublicProfilePageTitle();

            Assert.AreEqual(facilitatorName, publicProfileTitle, "Public profile page is not displayed.");

        }
    }
}
