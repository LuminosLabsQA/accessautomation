﻿using NUnit.Framework;
using QA_AccessConsciousnessShop.Pages.LandingPage;


namespace QA_AccessConsciousnessShop.Tests
{
    [TestFixture]
    [Category("LandingPage")]
    class LandingPageTest : SeleniumBase
    {
        [Test]
        public void WhenAUserReachesLandingPage_AllContentContainersAreDisplayed()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            Assert.That(landingPage.GetPageTitle(), Does.Contain("Shop | Access Consciousness"));
            landingPage.CloseSplashModal();
            Assert.True(landingPage.AreExpectedElementsDisplayed());
        }

        [Test]
        public void WhenAUserReachesLandingPage_TheShopPopupIsDisplayed()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            Assert.True(landingPage.IsSpashModalVisible());
        }

        [Test]
        public void WhenAUserReachesLandingPage_CanCloseShopPopupAndBeLeftOnShopStartPage()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.ClickContinueToShopButton();
            Assert.False(landingPage.IsSpashModalVisible());
        }

        [Test]
        public void WhenAUserReachesLandingPage_TheSplashModalIsDisplayedOnlyOnFirstVisit()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.RefreshPage();
            Assert.False(landingPage.IsSpashModalVisible());
        }

        [Test]
        public void WhenAUserReachesLandingPage_CanNavigateToCartWhileCartIsEmpty()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .ClickCartButton();
            Assert.That(landingPage.GetPageUrl(), Does.Contain("shopping-cart"));
        }

        [Test]
        public void WhenAUserReachesLandingPage_CanNavigateToWishlistWhileListIsEmpty()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .ClickWishlist();
            landingPage.WaitForPageToLoad();
            Assert.That(landingPage.GetPageUrl(), Does.Contain("wish-list"));
        }

        [Test]
        public void WhenAUserReachesLandingPage_CanNavigateBackToAccessClasses()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .ClickBackToAccessHeaderLink();
            Assert.That(landingPage.GetPageTitle(), Does.Contain("title | Access Consciousness"));
        }

        [Test]
        public void WhenAUserReachesLandingPage_AllHeaderElementsAreDisplayedProperly()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal();
            Assert.True(landingPage.IsHeaderSectionContaingExpectedElements());
        }

        [Test]
        public void WhenAUserReachesLandingPage_AllFooterElementsAreDisplayedProperly()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal();
            Assert.True(landingPage.IsFooterSectionContaingExpectedElements());
        }

        [Test]
        public void WhenAUserReachesLandingPage_CanNavigateToATopicPage()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .ClickViewTopics()
                .ClickSelectTopics();
            Assert.That(landingPage.GetPageUrl(), Does.Contain("are-you-happy-topic-page"));
        }

        [Test]
        public void WhenAUserReachesLandingPage_CanNavigateToAnAuthorSearch()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .ClickViewAuthors()
                .ClickSelectAuthor();
            Assert.That(landingPage.GetPageUrl(), Does.Contain("Gary%20M.%20Douglas"));
        }

        [Test]
        public void WhenAUserReachesLandingPage_HelpAndAccountFooterTextIsDisplayedProperly()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal();
            Assert.That(landingPage.GetHelpAndInfoFooterText(), Does.Contain("Help & Info"));
            Assert.That(landingPage.GetAccountFooterText(), Does.Contain("Your Account"));
        }
    }
}
