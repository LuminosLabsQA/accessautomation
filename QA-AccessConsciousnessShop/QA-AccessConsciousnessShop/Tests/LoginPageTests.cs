﻿using NUnit.Framework;
using QA_AccessConsciousnessShop.Factories;
using QA_AccessConsciousnessShop.Objects;
using QA_AccessConsciousnessShop.Pages.LandingPage;
using QA_AccessConsciousnessShop.Pages.LoginPage;
using QA_AccessConsciousnessShop.Pages.MyAccountPage;
using SeleniumCommon.ConfigManager;
using SeleniumCommon.Objects.User;
using System.Collections.Generic;
using System.Threading;

namespace QA_AccessConsciousnessShop.Tests
{
    [TestFixture]
    [Category("LoginPage")]
    class LoginPageTests : SeleniumBase
    {
        [Test]
        public void WhenUserReachesShopPage_HeCanCreateAnAccount()
        {
            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.NewRandomUser);

            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();

            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.GoToRegistrationForm()
                .Register(user);

            Assert.True(loginPage.IsCreateAccountSuccessMessagesDisplayed());
        }

        [TestCase(UserBuilder.UserTypes.ExistingRegularUserEurope, true)]
        [TestCase(UserBuilder.UserTypes.ExistingAdminUser, false)]
        [TestCase(UserBuilder.UserTypes.ExistingFacilitatorUser, true)]
        public void WhenUserReachesShopPage_HeLogsIntoExistingAccountFromLogInHeaderLink(UserBuilder.UserTypes userType, bool typeOfUser)
        {
            IUser user = new UserBuilder().GetUser(userType); //TODO - create user type and implementation - DONE

            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.Login(user);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            Assert.True(myAccount.IsUserLoggedIn());
            if (typeOfUser)
            {
                Assert.False(loginPage.isAdminButtonDisplayed());
            }
            else
            {
                Assert.True(loginPage.isAdminButtonDisplayed());
            }

        }

        [Test]
        public void WhenUserReachesShopPage_HeLogsIntoAnAccountAndLogsOut()
        {
            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.Login(user);
            loginPage.ClickLogoutLink();
            Assert.That(loginPage.GetPageUrl(), Does.Match("http://access.techromix.com/en/"));
        }

        [Test]
        public void WhenUserReachedShopPage_HeOpensLoginModalAndTriesToLoginUsingInvalidCredentials()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.NewRandomUser);
            loginPage.Login(user);
            Assert.True(loginPage.IsLoginErrorMessageDisplayed());
        }

        [Test]
        public void WhenUserReachesShopPage_CreatesAnAccountAndLogsOut()
        {
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            WhenUserReachesShopPage_HeCanCreateAnAccount();
            loginPage.ClickLogoutLink();
            Assert.That(loginPage.GetPageUrl(), Does.Match("http://access.techromix.com/en/"));
        }

        [TestCase(UserBuilder.UserTypes.ExistingRegularUserEurope)]
        public void WhenUserReachesShopPage_LogsInDirectlyFromShopPopup(UserBuilder.UserTypes userType)
        {
            IUser user = new UserBuilder().GetUser(userType); //TODO - create user type and implementation - DONE

            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.ClickSignInHereFromShopPopup();

            loginPage.Login(user);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            Assert.True(myAccount.IsUserLoggedIn());
        }

        [TestCase(UserBuilder.UserTypes.NewRandomUser, true)]
        [TestCase(UserBuilder.UserTypes.NewRandomUser, false)]
        public void WhenUserReachesShopPage_HeNavigatesToTheForgotPasswordPageAndTriggersBothValidationMessages(UserBuilder.UserTypes userType, bool flowType)
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();

            IAccessUsers user = new UserBuilder().GetUser(userType);
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            if (flowType)
            {
                loginPage.ResetPasswordUsername(user);
            }
            else
            {
                loginPage.ResetPasswordEmail(user);
            }
        }

        [Test]
        public void WhenUserReachesShopPage_HeLogsInWithKeepMeLoggedInFunctionalityChecked()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();

            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.ClickKeepMeLoggedIn();

            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            loginPage.Login(user);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            Assert.True(myAccount.IsUserLoggedIn());

            IList<OpenQA.Selenium.Cookie> cookies = new List<OpenQA.Selenium.Cookie>();
            foreach (var cookie in Driver.Manage().Cookies.AllCookies)
            {
                if (cookie.Domain.Equals("access.techromix.com")) cookies.Add(cookie);
            }

            Driver.Quit();

            Driver = new BrowserFactory().SetupDriver();
            Driver.Navigate().GoToUrl(TestConfig.BaseApplicationUrl + TestConfig.ResourcePathUrl);
           foreach (var cookie in cookies)
            {
                Driver.Manage().Cookies.AddCookie(cookie);
                Thread.Sleep(500);
            }

            landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.RefreshPage();

            loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            Assert.True(myAccount.IsUserLoggedIn());
        }

        [Test]
        public void WhenUserReachesShopPage_HeNavigatesToCreateAccountPageAndVerifiesTheTermsAndPolicyModals()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();

            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.GoToRegistrationForm();
            loginPage.ClickTermsOfServiceLink();
            Assert.True(loginPage.IsTermsOfServicesModalDisplayed());
            loginPage.RefreshPage();
            loginPage.ClickPrivacyPolicyLink();
            Assert.True(loginPage.IsPrivacyPolicyModalDisplayed());
            loginPage.RefreshPage();
            loginPage.ClickCookiePolicyLink();
            Assert.True(loginPage.IsCookiePolicyModalDisplayed());
        }

        [Test]
        public void WhenUserReachesShopPage_HeNavigateToCreateAccountPageAndLogsInUsingTheLoginLink()
        {
            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            landingPage.CloseSplashModal()
                .GoToLogin();

            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            loginPage.GoToRegistrationForm();
            loginPage.ClickCreateAccountLoginLink();

            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
            loginPage.Login(user);
            IMyAccountPage myAccount = new DesktopMyAccountPage(Driver);
            Assert.True(myAccount.IsUserLoggedIn());
        }
    }
}

