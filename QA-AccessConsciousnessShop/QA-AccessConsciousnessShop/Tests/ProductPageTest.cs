﻿using NUnit.Framework;
using QA_AccessConsciousnessShop.Pages.ProductPage;
using QA_AccessConsciousnessShop.Factories;
using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;
using QA_AccessConsciousnessShop.Pages.LoginPage;
using QA_AccessConsciousnessShop.Pages.LandingPage;

namespace QA_AccessConsciousnessShop.Tests
{
    [TestFixture]
    [Category("ProductPageTests")]
    class ProductPageTest : SeleniumBase
    {
        [TestCase(ProductType.AlternateDigitalProductOnSale)]
        [TestCase(ProductType.RegularPhysicalProductRetail)]
        public void WhenAGuestNavigatesToAProductPage_HeAddsToCartWithQTY1(ProductType productType)
        {
            Products product = new ProductTypeFactory().GetProduct(productType);

            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            Driver.Navigate().GoToUrl(product.Url);
            if(productType == ProductType.AlternateDigitalProductOnSale)
            {
                productPage.AddToCart();
            }
            else
            {
                productPage.AlternateAddToCart();
            }
            Assert.True(productPage.IsATCSuccessMessageDisplayed());
        }

        [TestCase(ProductType.AlternateDigitalProductOnSale)]
        [TestCase(ProductType.RegularPhysicalProductRetail)]
        public void WhenAGuestNavigatesToAProductPage_HeAddsToCartWithQTYAbove1(ProductType  productType)
        {
            Products product = new ProductTypeFactory().GetProduct(productType);

            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            Driver.Navigate().GoToUrl(product.Url);
            productPage.SetQuantity(3);
            productPage.AddToCart();
            Assert.True(productPage.IsATCSuccessMessageDisplayed());
        }

        [TestCase(ProductType.AlternateDigitalProductOnSale)]
        [TestCase(ProductType.RegularPhysicalProductRetail)]
        public void WhenACustomerNavigatesToAProductPAge_HeAddsToCartWithQTY1 (ProductType productType)
        {
            Products product = new ProductTypeFactory().GetProduct(productType);
            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);

            ILandingPage landingPage = new Factories.PageFactory().GetLandingPage(Driver);
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            
            
            landingPage.CloseSplashModal()
                .GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().Refresh();
            Driver.Navigate().GoToUrl(product.Url);
            if (productType == ProductType.AlternateDigitalProductOnSale)
            {
                productPage.AddToCart();
            }
            else
            {
                productPage.AlternateAddToCart();
            }
            Assert.True(productPage.IsATCSuccessMessageDisplayed());
        }
    }
}
