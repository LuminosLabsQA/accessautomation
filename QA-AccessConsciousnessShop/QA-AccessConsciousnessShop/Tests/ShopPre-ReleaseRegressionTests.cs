﻿using NUnit.Framework;
using QA_AccessConsciousnessShop.Pages.ProductPage;
using QA_AccessConsciousnessShop.Factories;
using QA_AccessConsciousnessShop.Objects;
using SeleniumCommon.Objects.User;
using QA_AccessConsciousnessShop.Pages.LoginPage;
using QA_AccessConsciousnessShop.Pages.CartPage;
using SeleniumCommon.Objects.User.Address;
using SeleniumCommon.Objects.User.CreditCard;

namespace QA_AccessConsciousnessShop.Tests
{
    [TestFixture]
    [Category("ShopPre_ReleaseRegressionTests")]
    class ShopPre_ReleaseRegressionTests : SeleniumBase
    {
        [Test]
        public void WhenUserReachesTheAccessShop_HeAddsMultipleProductToCartAndCheckTheCheckoutTotals()
        {
            Products alternateDigitalProductOnSale = new ProductTypeFactory().GetProduct(ProductType.AlternateDigitalProductOnSale);
            Products alternateDigitalProductOnSaleAndCountryPrice = new ProductTypeFactory().GetProduct(ProductType.AlternateDigitalProductOnSaleAndCountryPrice);
            Products regularPhysicalProductCountryPrice = new ProductTypeFactory().GetProduct(ProductType.RegularPhysicalProductCountryPrice);
            Products regularPhysicalProductRetail = new ProductTypeFactory().GetProduct(ProductType.RegularPhysicalProductRetail);
            Products regularPhysicalProductWholeSale = new ProductTypeFactory().GetProduct(ProductType.RegularPhysicalProductWholeSale);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.USAAddress);

            loginPage.GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().GoToUrl(alternateDigitalProductOnSale.Url);
            Driver.Navigate().Refresh();
            if (cartPage.GetCartButtonCount() == 0)
            {
                productPage.AddToCart();
            }
            else
            {
                productPage.GoToCart();
                cartPage.EmptyCart();
                Driver.Navigate().GoToUrl(alternateDigitalProductOnSale.Url);
                productPage.AddToCart();
            }
            float p1 = productPage.GetProductPrice();
            Driver.Navigate().GoToUrl(alternateDigitalProductOnSaleAndCountryPrice.Url);
            float p2 = productPage.GetProductPrice();
            productPage.AddToCart();
            Driver.Navigate().GoToUrl(regularPhysicalProductCountryPrice.Url);
            float p3 = productPage.GetProductPrice();
            productPage.AddToCart();
            Driver.Navigate().GoToUrl(regularPhysicalProductRetail.Url);
            float p4 = productPage.GetProductPrice();
            productPage.AddToCart();
            Driver.Navigate().GoToUrl(regularPhysicalProductWholeSale.Url);
            productPage.SetQuantity(2);
            float p5 = productPage.GetProductPrice();
            productPage.AddToCart();
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.ClickBillingContinueButton();
            cartPage.EnterShippingInformationAndContinue(user, addressUSA);
            float productsPrice = p5 * 2 + p1 + p2 + p3 + p4;
            float checkoutPrice = cartPage.GetCheckoutPrice();
            Assert.True(productsPrice == checkoutPrice);
            
        }

        [TestCase(ProductType.AlternateDigitalProductOnSale)]
        [TestCase(ProductType.AlternateDigitalProductOnSaleAndCountryPrice)]
        [TestCase(ProductType.RegularPhysicalProductRetail)]
        [TestCase(ProductType.RegularPhysicalProductCountryPrice)]
        [TestCase(ProductType.RegularPhysicalProductWholeSale)]
        public void WhenUserReachesTheAccessShop_HeLogsInAndVerifiesProductPrices(ProductType productType)
        {
            Products product = new ProductTypeFactory().GetProduct(productType);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IUser user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);

            loginPage.GoToLogin();
            loginPage.Login(user);
            productPage.RefreshPage();
            Driver.Navigate().GoToUrl(product.Url);
            if (productType == ProductType.RegularPhysicalProductWholeSale)
            {
                productPage.SetQuantity(2);
            }
            switch (productType)
            {
                case ProductType.AlternateDigitalProductOnSale:
                    {
                        Assert.That(productPage.GetAlternateDiscountedPriceLabel(), Does.Contain("Sale price:"));
                        float pdpProductPrice = productPage.GetProductPrice();
                        productPage.AddToCart();
                        productPage.GoToCart();
                        float cartProductPrice = cartPage.GetCartProductPrice();
                        Assert.True(pdpProductPrice == cartProductPrice);
                        cartPage.EmptyCart();
                        break;
                    }
                case ProductType.AlternateDigitalProductOnSaleAndCountryPrice:
                    {
                        Assert.That(productPage.GetAlternateDiscountedPriceLabel(), Does.Contain("Final price:"));
                        float pdpProductPrice = productPage.GetProductPrice();
                        productPage.AddToCart();
                        productPage.GoToCart();
                        float cartProductPrice = cartPage.GetCartProductPrice();
                        Assert.True(pdpProductPrice == cartProductPrice);
                        cartPage.EmptyCart();
                        break;
                    }
                case ProductType.RegularPhysicalProductRetail:
                    {
                        Assert.False(productPage.IsDiscountedPriceLableDisplayed());
                        float pdpProductPrice = productPage.GetProductPrice();
                        productPage.AddToCart();
                        productPage.GoToCart();
                        float cartProductPrice = cartPage.GetCartProductPrice();
                        Assert.True(pdpProductPrice == cartProductPrice);
                        cartPage.EmptyCart();
                        break;
                    }
                case ProductType.RegularPhysicalProductCountryPrice:
                    {
                        Assert.That(productPage.GetRegularDiscountedPriceLabel(), Does.Contain("Country price:"));
                        float pdpProductPrice = productPage.GetProductPrice();
                        productPage.AddToCart();
                        productPage.GoToCart();
                        float cartProductPrice = cartPage.GetCartProductPrice();
                        Assert.True(pdpProductPrice == cartProductPrice);
                        cartPage.EmptyCart();
                        break;
                    }
                case ProductType.RegularPhysicalProductWholeSale:
                    {
                        Assert.That(productPage.GetAlternateDiscountedPriceLabel(), Does.Contain("Wholesale:"));
                        float pdpProductPrice = productPage.GetProductPrice();
                        productPage.AddToCart();
                        productPage.GoToCart();
                        float cartProductPrice = cartPage.GetCartProductPrice();
                        Assert.True(pdpProductPrice == cartProductPrice);
                        cartPage.EmptyCart();
                        break;
                    }
            }
        }

        [Test]
        public void WhenUserReachesTheAccessShop_HeLogsInAndPurchasesADigitalProductUsingVISACard()
        {
            Products digitalProduct = new ProductTypeFactory().GetProduct(ProductType.AlternateDigitalProductOnSale);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress address = new AddressFactory().GetAddress(AddressFactory.AddressType.AlaskaAddress);
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.VISA);

            loginPage.GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().GoToUrl(digitalProduct.Url);
            Driver.Navigate().Refresh();
            Assert.True(productPage.IsAlternateSaleBadgeDisplayed());
            if (cartPage.GetCartButtonCount() == 0)
            {
                productPage.AddToCart();
            }
            else
            {
                productPage.GoToCart();
                cartPage.EmptyCart();
                Driver.Navigate().GoToUrl(digitalProduct.Url);
                productPage.AddToCart();
            }
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.ClickBillingContinueButton();
            cartPage.EnterShippingInformationAndContinue(user, address);
            cartPage.EnterCardDetails(user, card);
            cartPage.ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }

        [Test]
        public void WhenUserReachesTheAccessShop_HeLogsInAndPurchasesADigitalAndPhysicalProductUsingMasterCard()
        {
            Products digitalProduct = new ProductTypeFactory().GetProduct(ProductType.AlternateDigitalProductOnSale);
            Products physicalProduct = new ProductTypeFactory().GetProduct(ProductType.RegularPhysicalProductRetail);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            ILoginPage loginPage = new Factories.PageFactory().GetLoginPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress address = new AddressFactory().GetAddress(AddressFactory.AddressType.USAAddress);
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.MasterCard);

            loginPage.GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().GoToUrl(digitalProduct.Url);
            Driver.Navigate().Refresh();
            if (cartPage.GetCartButtonCount() == 0)
            {
                productPage.AddToCart();
            }
            else
            {
                productPage.GoToCart();
                cartPage.EmptyCart();
                Driver.Navigate().GoToUrl(digitalProduct.Url);
                productPage.AddToCart();
            }
            Driver.Navigate().GoToUrl(physicalProduct.Url);
            productPage.AddToCart();
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.ClickBillingContinueButton();
            cartPage.EnterShippingInformationAndContinue(user, address);
            cartPage.EnterCardDetails(user, card);
            cartPage.ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }

        [Test]
        public void WhenUserReachesTheAccessShop_PurchasesAPhysicalProductAsAGuest()
        {
            Products physicalProductWholeSale = new ProductTypeFactory().GetProduct(ProductType.RegularPhysicalProductWholeSale);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingRegularUserEurope);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.USAAddress);
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.AMEX);

            Driver.Navigate().GoToUrl(physicalProductWholeSale.Url);
            productPage.SetQuantity(2);
            productPage.AddToCart();
            productPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.EnterBillingInformationAndContinue(user, addressUSA);
            cartPage.EnterShippingInformationAndContinue(user, addressUSA);
            cartPage.EnterCardDetails(user, card);
            cartPage.ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }

        [Test]
        public void WhenFacilitatorUserReachesTheAccessShop_HeLogsInAndPurchsesTwoDigitalProducts()
        {
            Products digitalProductOnSale = new ProductTypeFactory().GetProduct(ProductType.AlternateDigitalProductOnSale);
            Products digitalProductOnSaleAndCountryPrice = new ProductTypeFactory().GetProduct(ProductType.AlternateDigitalProductOnSaleAndCountryPrice);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.AustraliaAddress);
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.VISA);

            loginPage.GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().GoToUrl(digitalProductOnSale.Url);
            Driver.Navigate().Refresh();
            if (cartPage.GetCartButtonCount() == 0)
            {
                productPage.AddToCart();
            }
            else
            {
                productPage.GoToCart();
                cartPage.EmptyCart();
                Driver.Navigate().GoToUrl(digitalProductOnSale.Url);
                productPage.AddToCart();
            }
            Driver.Navigate().GoToUrl(digitalProductOnSaleAndCountryPrice.Url);
            productPage.AddToCart();
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.ClickBillingContinueButton();
            cartPage.EnterShippingInformationAndContinue(user, addressUSA);
            cartPage.EnterCardDetails(user, card);
            cartPage.ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }

        //[Test]
        //public void WhenFacilitatorUserReachesTheAccessShop_HePurchasesAMembershipProduct()
        //{
        //    Products membership = new ProductTypeFactory().GetProduct(ProductType.Membership);
        //    IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
        //    ILoginPage loginPage = new DesktopLoginPage(Driver);
        //    IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
        //    ICartPage cartPage = new DesktopCartPage(Driver);
        //    IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.AustraliaAddress);
        //    ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.VISA);

        //    loginPage.GoToLogin();
        //    loginPage.Login(user);
        //    Driver.Navigate().GoToUrl(membership.Url);
        //    Driver.Navigate().Refresh();
        //    if (cartPage.GetCartButtonCount() == 0)
        //    {
        //        productPage.AddToCart();
        //    }
        //    else
        //    {
        //        productPage.GoToCart();
        //        cartPage.EmptyCart();
        //        Driver.Navigate().GoToUrl(membership.Url);
        //        productPage.AddToCart();
        //    }
        //    cartPage.GoToCart();
        //    cartPage.CheckoutNow();
        //    cartPage.ClickBillingContinueButton();
        //    cartPage.EnterShippingInformationAndContinue(user, addressUSA);
        //    cartPage.EnterCardDetails(user, card);
        //    cartPage.ClickPlaceOrderButton();
        //    Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        //}

        //[Test]
        //public void WhenFacilitatorUserReachesTheAccessShop_HePurchasesAPublicProfile()
        //{
        //    Products publicProfile = new ProductTypeFactory().GetProduct(ProductType.PublicProfile);
        //    IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
        //    ILoginPage loginPage = new DesktopLoginPage(Driver);
        //    IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
        //    ICartPage cartPage = new DesktopCartPage(Driver);
        //    IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.AustraliaAddress);
        //    ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.Discover);

        //    loginPage.GoToLogin();
        //    loginPage.Login(user);
        //    Driver.Navigate().GoToUrl(publicProfile.Url);
        //    Driver.Navigate().Refresh();
        //    if (cartPage.GetCartButtonCount() == 0)
        //    {
        //        productPage.AddToCart();
        //    }
        //    else
        //    {
        //        productPage.GoToCart();
        //        cartPage.EmptyCart();
        //        Driver.Navigate().GoToUrl(publicProfile.Url);
        //        productPage.AddToCart();
        //    }
        //    cartPage.GoToCart();
        //    cartPage.CheckoutNow();
        //    cartPage.ClickBillingContinueButton();
        //    cartPage.EnterShippingInformationAndContinue(user, addressUSA);
        //    cartPage.EnterCardDetails(user, card);
        //    cartPage.ClickPlaceOrderButton();
        //    Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        //}

        [Test]
        public void WhenFacilitatorUserReachesTheAccessShop_HePurchasesABarsCertification()
        {
            Products barsCertification = new ProductTypeFactory().GetProduct(ProductType.Certification);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingFacilitatorUser);
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.AustraliaAddress);
            ICreditCard card = new CreditCardFactory().GetCard(CreditCardFactory.CardType.MasterCard);

            loginPage.GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().GoToUrl(barsCertification.Url);
            Driver.Navigate().Refresh();
            if (cartPage.GetCartButtonCount() == 0)
            {
                productPage.AddToCartCertification();
            }
            else
            {
                productPage.GoToCart();
                cartPage.EmptyCart();
                Driver.Navigate().GoToUrl(barsCertification.Url);
                productPage.AddToCartCertification();
            }
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.ClickBillingContinueButton();
            cartPage.EnterShippingInformationAndContinue(user, addressUSA);
            cartPage.EnterCardDetails(user, card);
            cartPage.ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }

        [Test]
        public void WhenUserReachesTheAccessShop_HePurchasesAProductUsingPayPalAsACustomer()
        {
            Products barsCertification = new ProductTypeFactory().GetProduct(ProductType.Certification);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingPayPalUser);
            ILoginPage loginPage = new DesktopLoginPage(Driver);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.USAAddress);

            loginPage.GoToLogin();
            loginPage.Login(user);
            Driver.Navigate().GoToUrl(barsCertification.Url);
            Driver.Navigate().Refresh();
            if (cartPage.GetCartButtonCount() == 0)
            {
                productPage.AddToCartCertification();
            }
            else
            {
                productPage.GoToCart();
                cartPage.EmptyCart();
                Driver.Navigate().GoToUrl(barsCertification.Url);
                productPage.AddToCartCertification();
            }
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.ClickBillingContinueButton()
                .EnterShippingInformationAndContinue(user, addressUSA)
                .StartPayPalFlow()
                .PayPalLogin(user)
                .ClickPayPalContinuedButton()
                .ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }

        [Test]
        public void WhenUserReachesTheAccessShop_HePurchasesAProductUsingPayPalAsAGuest()
        {
            Products regularPhysicalProduct = new ProductTypeFactory().GetProduct(ProductType.RegularPhysicalProductRetail);
            IAccessUsers user = new UserBuilder().GetUser(UserBuilder.UserTypes.ExistingPayPalUser);
            IProductPage productPage = new Factories.PageFactory().GetProductPage(Driver);
            ICartPage cartPage = new DesktopCartPage(Driver);
            IAddress addressUSA = new AddressFactory().GetAddress(AddressFactory.AddressType.USAAddress);

            Driver.Navigate().GoToUrl(regularPhysicalProduct.Url);
            Driver.Navigate().Refresh();
            productPage.AddToCart();
            cartPage.GoToCart();
            cartPage.CheckoutNow();
            cartPage.EnterBillingInformationAndContinue(user, addressUSA);
            cartPage.EnterShippingInformationAndContinue(user, addressUSA);
            cartPage.StartPayPalFlow();
            cartPage.PayPalLogin(user);
            cartPage.ClickPayPalContinuedButton();
            cartPage.ClickPlaceOrderButton();
            Assert.True(cartPage.IsCheckoutSuccessMessageDisplayed());
        }
    }
}

