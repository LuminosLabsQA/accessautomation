﻿using OpenQA.Selenium;

namespace QA_AccessConsciousnessShop.Utilities
{
    public static class AccessUtilities
    {
        public static float ExtractPriceValueFromString(IWebElement element, char[] charArray, int positionFromArray)
        {
            int indexOfNumber = positionFromArray;
             
            string stringElement = element.Text;
            var numberOfParts = stringElement.Split(charArray);
            var stringPosition = numberOfParts[indexOfNumber];
            float floatValue = float.Parse(stringPosition);
            return floatValue;
        }
    }
}
