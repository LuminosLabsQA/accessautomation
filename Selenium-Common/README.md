## Selenium-Common

Set up a new project
	- Create a new repo for the project
	- Create a new Class Library project in Visula Studio
	- Add Selenium-Common in project as a submodule
	- Create a new branch, using the project name, in Selenium-Common and use it in the project
	- From Selenium-Common copy in the project folder:
		- SeleniumBase.cs
		- ReportingHandler.cs
		- from ConfigFiles, the config file you need, and rename it to "App.Config"