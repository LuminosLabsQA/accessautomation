﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace SeleniumCommon.ConfigManager
{
    public class BrowserFactory
    {
        public IWebDriver SetupDriver()
        {
            if (TestConfig.Remote)
            {
                if (TestConfig.Desktop)
                {
                    return new RemoteDriverManager().CreateDriver();
                }
                else return null; //ToDo
            }
            else
            {
                if (TestConfig.Desktop)
                {
                    return new LocalDriverManager().CreateDriver();
                }
                else return null; //ToDo
            }
        }      
    }
}
