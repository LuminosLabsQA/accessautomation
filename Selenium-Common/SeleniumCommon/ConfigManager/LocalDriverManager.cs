using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumCommon.ConfigManager;

namespace SeleniumCommon
{
    public class LocalDriverManager
    {
        private IWebDriver _driver;
        private static readonly TimeSpan ImplicitTimeoutSec = TimeSpan.FromSeconds(10);

        public IWebDriver CreateDriver()
        {
            switch (TestConfig.Browser)
            {
                case "chrome":
                    StartChromeDriver();
                    break;

                case "internet explorer":
                    StartInternetExplorerDriver();
                    break;

                case "firefox":
                default:
                    StartFirefoxDriver();              
                    break;
            }

            SetImplicitWait();
            SetWindowSize();
            return GetDriver();
        }

        private void StartChromeDriver()
        {
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("disable-infobars");


            _driver = new ChromeDriver(options);
        }

        private void StartFirefoxDriver()
        {
            _driver = new FirefoxDriver();
        }

        private void StartInternetExplorerDriver()
        {
            var options = new InternetExplorerOptions
            {
                EnsureCleanSession = true,
                EnableNativeEvents = false,
                IgnoreZoomLevel = true,
                IntroduceInstabilityByIgnoringProtectedModeSettings = true
            };
            _driver = new InternetExplorerDriver(@"C:\Selenium\Drivers\InternetExplorerDriver", options);
        }
        
        private void SetImplicitWait()
        {
            _driver.Manage().Timeouts().ImplicitWait = ImplicitTimeoutSec;
        }
        
        private void SetWindowSize()
        {
            _driver.Manage().Window.Maximize();
        }
        
        private IWebDriver GetDriver()
        {
            return _driver;
        }

    }
}