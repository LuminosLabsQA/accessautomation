using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using SeleniumCommon.ConfigManager;

namespace SeleniumCommon
{
    public class RemoteDriverManager
    {
        private IWebDriver _driver;
        private static readonly TimeSpan ImplicitTimeoutSec = TimeSpan.FromSeconds(10);
        private string _remoteServer;

        public IWebDriver CreateDriver()
        {
            //Will need to construct the remoteServerUri so it can be passed to the remoteWebDriver.
            _remoteServer = BuildRemoteServer(TestConfig.SeleniumHubUrl, TestConfig.SeleniumHubPort);

            switch (TestConfig.Browser)
            {
                case "chrome":
                    StartChromeDriver();
                    break;

                case "internet explorer":
                    StartInternetExplorerDriver();
                    break;

                //If a string isn't matched, it will default to FireFoxDriver
                case "firefox":
                default:
                    StartFirefoxDriver();              
                    break;
            }

            SetImplicitWait();
            SetWindowSize();
            return GetDriver();
        }

        private void StartChromeDriver()
        {
            var options = new ChromeOptions
            {
                PlatformName = TestConfig.Platform.ToString(),
                BrowserVersion = TestConfig.BrowserVersion
            };

            options.AddArgument("disable-infobars");

            _driver = new RemoteWebDriver(new Uri(_remoteServer), options);
        }

        private IWebDriver StartInternetExplorerDriver()
        {
            var options = new InternetExplorerOptions
            {
                PlatformName = TestConfig.Platform.ToString(),
                BrowserVersion = TestConfig.BrowserVersion
            };
            
            return new RemoteWebDriver(new Uri(_remoteServer), options);
        }

        private void StartFirefoxDriver()
        {
            var options = new FirefoxOptions
            {
                PlatformName = TestConfig.Platform.ToString(),
                //BrowserVersion = TestConfig.BrowserVersion,
                LogLevel = FirefoxDriverLogLevel.Trace
            };
           
            _driver = new RemoteWebDriver(new Uri(_remoteServer), options);
        }

        private void SetImplicitWait()
        {
            _driver.Manage().Timeouts().ImplicitWait = ImplicitTimeoutSec;
        }
        
        private void SetWindowSize()
        {
            _driver.Manage().Window.Maximize();
        }
        
        private IWebDriver GetDriver()
        {
            return _driver;
        }

        private static string BuildRemoteServer(string remoteServer, string remoteServerPort)
        {
            return string.Format("{0}:{1}/wd/hub", remoteServer, remoteServerPort);
        }
    }
}