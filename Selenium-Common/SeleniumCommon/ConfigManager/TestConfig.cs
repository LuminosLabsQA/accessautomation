﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using SeleniumCommon.Utilities;

namespace SeleniumCommon.ConfigManager
{
    public static class TestConfig
    {
        public static bool Remote
        {
            get
            {
                var reader = new AppSettingsReader();
                return (bool)reader.GetValue("Remote", typeof(bool));
            }
        }

        public static bool Desktop
        {
            get
            {
                var reader = new AppSettingsReader();
                return (bool)reader.GetValue("Desktop", typeof(bool));
            }
        }

        public static string Browser => ConfigurationManager.AppSettings["Browser"];
        public static string DriverPath => ConfigurationManager.AppSettings["DriverPath"];
        public static string BrowserVersion => ConfigurationManager.AppSettings["BrowserVersion"];

        public static PlatformType Platform => GetPlatformType();
        public static string MobilePlatform => ConfigurationManager.AppSettings["MobilePlatform"];
        public static string DeviceName => ConfigurationManager.AppSettings["DeviceName"];
        public static string PlatformVersion => ConfigurationManager.AppSettings["PlatformVersion"];

        public static string SeleniumHubUrl => ConfigurationManager.AppSettings["SeleniumHubUrl"];
        public static string SeleniumHubPort => ConfigurationManager.AppSettings["SeleniumHubPort"];

        public static string AppiumServerUrl => ConfigurationManager.AppSettings["AppiumServerUrl"];
        public static string AppiumServerPort => ConfigurationManager.AppSettings["AppiumServerPort"];
        
        public static string BaseApplicationUrl => ConfigurationManager.AppSettings["BaseApplicationUrl"];
        public static string ClassCalendarPage => ConfigurationManager.AppSettings["ClassCalendarPage"];
        public static string POPHostPDP => ConfigurationManager.AppSettings["POPHostPDP"];
        public static string ResourcePathUrl => ConfigurationManager.AppSettings["ResourcePathUrl"];

        public static string ReportPath => ConfigurationManager.AppSettings["ReportPath"];
        public static string ReportConfigPath => ConfigurationManager.AppSettings["ReportConfigPath"];
        public static string ReportName => ConfigurationManager.AppSettings["ReportName"];
        public static string ScreenShotPath => ConfigurationManager.AppSettings["ScreenShotPath"];
        public static string ClassName => ConfigurationManager.AppSettings["ClassName"];
        public static string EventName => ConfigurationManager.AppSettings["EventName"];
        public static string TeleseriesName => ConfigurationManager.AppSettings["TeleseriesName"];
        public static string FacilitatorClass => ConfigurationManager.AppSettings["FacilitatorClass"];
        
        /// <summary>
        /// This method converts the OS string in the app.config to the matching value in the PlatformType Enum.
        /// In order to match the enum will have to Title case the OS string value.
        /// This is more protection for when someone enters the OS all lowercase
        /// </summary>
        /// <returns>A PlatformType instance</returns>
        private static PlatformType GetPlatformType()
        {
            var reader = new AppSettingsReader();
            var platformValue = (string)reader.GetValue("Platform", typeof(string));

            PlatformType platform;
            return Enum.TryParse(StringExtensions.FirstCharToUpper(platformValue), out platform) ? platform : PlatformType.Windows;
        }  
    }
}
