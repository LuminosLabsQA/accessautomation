﻿using System;
using System.Collections.Generic;
using System.Threading;
using RelevantCodes.ExtentReports;
using SeleniumCommon.ConfigManager;

namespace SeleniumCommon.Logging
{
     public class ReportFactory
    {
        private static ExtentReports _reporter;

        private static readonly IList<long?> ThreadList = new List<long?>(); 

        public static ExtentReports GetReporter()
        {
            lock (typeof(ReportFactory))
            {
                AddCurrentThreadToThreadList();
                if (_reporter == null)
                {
                    DateTime date = DateTime.Now;
                    string currentDate = date.ToString("yyyy-MM-dd_hh-mm_");
                    string reportName = TestConfig.ReportPath + currentDate + TestConfig.ReportName;
                    _reporter = new ExtentReports(reportName, true, DisplayOrder.NewestFirst);
                    _reporter.LoadConfig(TestConfig.ReportConfigPath);
                }
                return _reporter;
            }
        }

        private static void AddCurrentThreadToThreadList()
        {
            long? threadId = Thread.CurrentThread.ManagedThreadId;

            if (ThreadList.Count == 0)
            {
                ThreadList.Add(threadId);
            }
            else
            {
                bool notInThredList = true;
                foreach (var thread in ThreadList)
                {
                    if (threadId == thread)
                    {
                        notInThredList = false;
                        break;
                    }
                }
                if (notInThredList)
                {
                    ThreadList.Add(threadId);
                }
            }
        }

        //        public static void CloseReporter()
        //        {
        //            lock (typeof(ReportFactory))
        //            {
        //                RemoveCurrentThreadFromThreadList();
        //
        //                if (ThreadList.Count == 0)
        //                {
        //                    _reporter.Flush();
        //                    _reporter.Close();
        //                }
        //                
        //            }
        //        }
        
        public static void CloseReporter()
        {
            lock (typeof(ReportFactory))
            {
                _reporter.Flush();
                _reporter.Close();
            }
        }

        private static void RemoveCurrentThreadFromThreadList()
        {
            long? threadId = Thread.CurrentThread.ManagedThreadId;

            foreach (var thread in ThreadList)
            {
                if (thread == threadId)
                {
                    ThreadList.RemoveAt(ThreadList.IndexOf(thread));
                    break;
                }
            }
        }
    }
}
