﻿using OpenQA.Selenium;

namespace SeleniumCommon.Objects.Product
{
    public interface IProduct
    {
        string Name { get; set; }
        string Id { get; set; }
        string Category { get; set; }
        string Model { get; set; }
        float Price { get; set; }
        int Quantity { get; set; }
        string StockStatus { get; set; }
        string Url { get; set; }

        void SelectProductAttributes(IWebDriver driver);

        void SelectRandomProductAttributes(IWebDriver driver);

        void GetProductDetials(IWebDriver driver);

        bool IsSelectionValidated(IWebDriver driver);
    }
}