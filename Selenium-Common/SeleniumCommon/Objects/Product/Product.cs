﻿using OpenQA.Selenium;

namespace SeleniumCommon.Objects.Product
{
    public abstract class Product : IProduct
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Category { get; set; }
        public string Model { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        public string StockStatus { get; set; }
        public string Url { get; set; }

        public abstract void SelectProductAttributes(IWebDriver driver);

        public abstract void SelectRandomProductAttributes(IWebDriver driver);

        public abstract void GetProductDetials(IWebDriver driver);

        public abstract bool IsSelectionValidated(IWebDriver driver);
    }
}
