﻿namespace SeleniumCommon.Objects.User.Address
{
    public class Address : IAddress
    {
        public bool DefaultAddress { set; get; }
        public string Country { set; get; }
        public string AddressStreet { set; get; }
        public string AddressUnit { set; get; }
        public string Zip { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public string PhoneNumber { set; get; }
    }
}
