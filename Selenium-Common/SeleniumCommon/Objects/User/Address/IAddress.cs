﻿namespace SeleniumCommon.Objects.User.Address
{
    public interface IAddress
    {
        bool DefaultAddress { set; get; }
        string Country { set; get; }
        string AddressStreet { set; get; }
        string AddressUnit { set; get; }
        string Zip { set; get; }
        string City { set; get; }
        string State { set; get; }
        string PhoneNumber { set; get; }
    }
}