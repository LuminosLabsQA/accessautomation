﻿namespace SeleniumCommon.Objects.User.CreditCard
{
    public class CreditCard : ICreditCard
    {
        public CreditCardType.CardType Type { get; set; }
        public bool Default { set; get; }
        public CreditCardNumber.CardNumber CardNumber { set; get; }
        public CreditCardCvv.Cvv Cvv { set; get; }
        public CreditCardExpirationMonth.ExpirationMonth ExpirationMonth { set; get; }
        public string ExpirationYear { set; get; }
        public string CardNickname { set; get; }
    }
}
