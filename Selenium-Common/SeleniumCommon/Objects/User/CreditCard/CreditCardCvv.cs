﻿using System.ComponentModel;

namespace SeleniumCommon.Objects.User.CreditCard
{
    public class CreditCardCvv
    {
        public enum Cvv
        {
            [Description("123")]
            VisaCVV,
            [Description("1234")]
            AmexCVV
        }
    }
}
