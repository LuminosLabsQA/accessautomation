﻿using System.ComponentModel;

namespace SeleniumCommon.Objects.User.CreditCard
{
    public class CreditCardExpirationMonth
    {
        public enum ExpirationMonth
        {
            [Description("01-Jan")]
            January,
            [Description("02-Feb")]
            February,
            [Description("03-Mar")]
            March,
            [Description("04-Apr")]
            April,
            [Description("05-May")]
            May,
            [Description("06-Jun")]
            June,
            [Description("07-Jul")]
            July,
            [Description("08-Aug")]
            August,
            [Description("09-Sep")]
            September,
            [Description("10-Oct")]
            October,
            [Description("11-Nov")]
            November,
            [Description("12-Dec")]
            December
        }
    }
}
