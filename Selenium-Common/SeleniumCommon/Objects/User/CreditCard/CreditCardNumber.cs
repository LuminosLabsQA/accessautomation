﻿using System.ComponentModel;

namespace SeleniumCommon.Objects.User.CreditCard
{
    public class CreditCardNumber
    {
        public enum CardNumber
        {
            [Description("4111111111111111")]
            Visa,
            [Description("5555555555554444")]
            Mastercard,
            [Description("6011000990139424")]
            Discover,
            [Description("36259600000012")]
            DiscoverJCB,
            [Description("371449635398431")]
            AmEx,
            [Description("555555555555444")]
            IncompleteNumber
        }
    }
}
