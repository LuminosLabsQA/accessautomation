﻿namespace SeleniumCommon.Objects.User.CreditCard
{
    public class CreditCardType
    {
        public enum CardType
        {
            Visa,
            MasterCard,
            Discover,
            DiscoverJCB,
            AmEx,
            AmericanExpress, //Added this value in SeleniumCommon as "AmEx" did not exist in my project. Needed this value.
            FourDigitCCV
        }
    }
}
