﻿namespace SeleniumCommon.Objects.User.CreditCard
{
    public interface ICreditCard
    {
        CreditCardType.CardType Type { set; get; }
        bool Default { set; get; }
        CreditCardNumber.CardNumber CardNumber { set; get; }
        CreditCardCvv.Cvv Cvv { set; get; }
        CreditCardExpirationMonth.ExpirationMonth ExpirationMonth { set; get; }
        string ExpirationYear { set; get; }
    }
}