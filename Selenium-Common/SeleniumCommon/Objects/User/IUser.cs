﻿using System.Collections.Generic;
using SeleniumCommon.Objects.User.Address;
using SeleniumCommon.Objects.User.CreditCard;

namespace SeleniumCommon.Objects.User
{
    public interface IUser
    {
        string Email { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string ConfirmationPassword { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        IList<IAddress> Addresses { get; set; }
        IList<ICreditCard> Cards { get; set; }
        string GetUserFullName();
    }
}