﻿using System.Collections.Generic;
using SeleniumCommon.Objects.User.Address;
using SeleniumCommon.Objects.User.CreditCard;

namespace SeleniumCommon.Objects.User
{
    public class User : IUser
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmationPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IList<IAddress> Addresses { get; set; }
        public IList<ICreditCard> Cards { get; set; }

        public User()
        {
            Addresses = new List<IAddress>();
            Cards = new List<ICreditCard>();
        }

        public string GetUserFullName()
        {
            return FirstName + " " + LastName;
        }
    }
}
