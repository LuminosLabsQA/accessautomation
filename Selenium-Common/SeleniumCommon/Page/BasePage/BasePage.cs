﻿using System;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;

namespace SeleniumCommon.Page.BasePage
{
    public abstract class BasePage
    {
        private readonly IWebDriver _driver;

        private const int DefaultWaitTime = 30;
        protected const int ShortWaitTime = 5;

        protected BasePage(IWebDriver driver)
        {
            _driver = driver;
        }

        protected void WaitForPageToLoad()
        {
            IWait<IWebDriver> wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(DefaultWaitTime));
            wait.Until(driver1 => ((IJavaScriptExecutor)_driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public void GoToUrl(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        public string GetPageTitle()
        {
            return _driver.Title;
        }

        public string GetPageUrl()
        {
            return _driver.Url;
        }

        protected void RefreshPage()
        {
            _driver.Navigate().Refresh();
        }

        //protected IWebElement WaitForVisibleElement(By by)
        //{
        //    return WaitForVisibleElement(by, DefaultWaitTime);
        //}

        //protected IWebElement WaitForVisibleElement(By by, int time)
        //{
        //    WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(time));
        //    return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
        //}

        protected bool WaitForInvisibilityOfElement(By by)
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(DefaultWaitTime));
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(by));
        }

        protected bool WaitForVisibleElement(IWebDriver driver, IWebElement webElement)
        {
            try
            {
                Func<IWebDriver, bool> visibilityCondition = (x) => webElement.Displayed;

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                try
                {
                    wait.Until(visibilityCondition);
                }
                catch (WebDriverTimeoutException)
                {
                    return false;
                }

                return visibilityCondition.Invoke(null);
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        protected bool WaitForInvisibilityOfElement(IWebDriver driver, IWebElement webElement)
        {
            if (WaitForVisibleElement(driver, webElement))
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                try
                {
                    Func<IWebDriver, bool> invisibilityCondition = (x) => !webElement.Displayed;
                    wait.Until(invisibilityCondition);
                    return invisibilityCondition.Invoke(null);
                }
                catch (Exception)
                {
                    return true;
                }

            }
            return true;
        }

        protected bool WaitForAttribute(IWebDriver driver, IWebElement webElement, string attribute, string value)
        {
            try
            {
                // Build a function with a signature compatible with the WebDriverWait.Until method
                Func<IWebDriver, bool> testCondition = (x) => webElement.GetAttribute(attribute).Equals(value);

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                try
                {
                    wait.Until(testCondition);
                }
                catch (WebDriverTimeoutException) { }

                // Return a value to indicate if our wait was successful
                return testCondition.Invoke(null);
            }
            catch (Exception)
            {
                return true;
            }
        }

        protected void ScrollToElement(By by)
        {
            IWebElement element = _driver.FindElement(by);
            ScrollToElement(element);
        }

        protected void ScrollToTop()
        {
            ((IJavaScriptExecutor)_driver).ExecuteScript("window.scrollTo(0, 0);");
        }

        protected void ScrollUp(IWebElement element)
        {
            int x = element.Location.X;
            int y = element.Location.Y;
            if (y > 200) y -= 150;

            var js = string.Format("window.scrollTo({0}, {1})", x, y);
            ((IJavaScriptExecutor)_driver).ExecuteScript(js);
        }

        protected void ScrollToElement(IWebElement element)
        {
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        protected void MouseOver(IWebElement element)
        {
            OpenQA.Selenium.Interactions.Actions action = new OpenQA.Selenium.Interactions.Actions(_driver);
            action.MoveToElement(element).Perform();
        }

        protected bool IsElementVisible(IWebElement element)
        {
            if (element != null)
                return element.Displayed && element.Enabled;
            return false;
        }

        protected bool IsAlertDisplayed()
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(2));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void AcceptAlert()
        {
            if (IsAlertDisplayed())
            {
                _driver.SwitchTo().Alert().Accept();
            }
            WaitForPageToLoad();
        }

        public abstract void GoToLogin();
        public abstract void GoToCart();
        public abstract void GoToMyAccount();
    }
}
