﻿namespace SeleniumCommon.Page.BasePage
{
    public interface IBasePage
    {
        void GoToUrl(string url);
        string GetPageTitle();
        string GetPageUrl();
        void GoToLogin();
        void GoToCart();
        void GoToMyAccount();
    }
}