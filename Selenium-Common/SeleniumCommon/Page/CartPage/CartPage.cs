﻿using OpenQA.Selenium;
using SeleniumCommon.Objects.Product;

namespace SeleniumCommon.Page.CartPage
{
    public abstract class CartPage : BasePage.BasePage, ICartPage
    {
        protected CartPage(IWebDriver driver) : base(driver) { }

        public new ICartPage RefreshPage()
        {
            base.RefreshPage();
            return this;
        }

        public new ICartPage WaitForPageToLoad()
        {
            base.WaitForPageToLoad();
            return this;
        }

        public abstract ICartPage EmptyCart();

        public abstract ICartPage RemoveProductFromCart(IProduct product);

        public abstract ICartPage ModifyProductQuantity(IProduct product, int newQuant);

        public abstract void CheckoutNow();

        public abstract ICartPage MoveProductToWishList(IProduct product);
    }
}
