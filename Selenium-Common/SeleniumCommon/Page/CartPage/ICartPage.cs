﻿using SeleniumCommon.Objects.Product;
using SeleniumCommon.Page.BasePage;

namespace SeleniumCommon.Page.CartPage
{
    public interface ICartPage : IBasePage
    {
        ICartPage RefreshPage();
        ICartPage WaitForPageToLoad();
        ICartPage EmptyCart();
        ICartPage RemoveProductFromCart(IProduct product);
        ICartPage ModifyProductQuantity(IProduct product, int newQuant);
        void CheckoutNow();
        ICartPage MoveProductToWishList(IProduct product);
    }
}