﻿using SeleniumCommon.Page.BasePage;

namespace SeleniumCommon.Page.LandingPage
{
    public interface ILandingPage : IBasePage
    {
        ILandingPage RefreshPage();
        ILandingPage WaitForPageToLoad();
    }
}