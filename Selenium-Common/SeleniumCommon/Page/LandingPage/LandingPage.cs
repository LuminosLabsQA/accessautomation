﻿using OpenQA.Selenium;

namespace SeleniumCommon.Page.LandingPage
{
    public abstract class LandingPage : BasePage.BasePage, ILandingPage
    {
        protected readonly IWebDriver Driver;

        protected LandingPage(IWebDriver driver) : base(driver)
        {
            Driver = driver;
        }

        public new ILandingPage WaitForPageToLoad()
        {
            base.WaitForPageToLoad();
            return this;
        }

        ILandingPage ILandingPage.RefreshPage()
        {
            base.RefreshPage();
            return this;
        }
    }

}
