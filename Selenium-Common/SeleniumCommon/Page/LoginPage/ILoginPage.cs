﻿using SeleniumCommon.Objects.User;
using SeleniumCommon.Page.BasePage;

namespace SeleniumCommon.Page.LoginPage
{
    public interface ILoginPage : IBasePage
    {
        ILoginPage RefreshPage();
        ILoginPage WaitForPageToLoad();
        ILoginPage GoToRegistrationForm();
        ILoginPage GoToLoginForm();
        ILoginPage SetUserName(string userName);
        ILoginPage SetPassword(string password);
        ILoginPage ConfirmPassword(string password);
        void ClickLoginButton();
        void ClickRegisterButton();
        void Login(IUser user);
        void Register(IUser user);
        bool IsLogginErrorMessageDisplayed();
        bool IsLogginPageLoaded();
    }
}