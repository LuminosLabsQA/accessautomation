﻿using OpenQA.Selenium;
using SeleniumCommon.Objects.User;

namespace SeleniumCommon.Page.LoginPage
{
    public abstract class LoginPage : BasePage.BasePage, ILoginPage
    {
        protected readonly IWebDriver Driver;

        protected LoginPage(IWebDriver driver) : base(driver)
        {
            Driver = driver;
        }

        public new ILoginPage RefreshPage()
        {
            base.RefreshPage();
            return this;
        }

        public new ILoginPage WaitForPageToLoad()
        {
            base.WaitForPageToLoad();
            return this;
        }
        
        public abstract ILoginPage GoToRegistrationForm();

        public abstract ILoginPage GoToLoginForm();
        
        public abstract ILoginPage SetUserName(string userName);

        public abstract ILoginPage SetPassword(string password);

        public abstract ILoginPage ConfirmPassword(string password);

        public abstract void ClickLoginButton();

        public abstract void ClickRegisterButton();

        public void Login(IUser user)
        {
            SetUserName(user.UserName);
            SetPassword(user.Password);
            ClickLoginButton();
        }

        public void Register(IUser user)
        {
            GoToRegistrationForm();
            SetUserName(user.Email);
            SetPassword(user.Password);
            ConfirmPassword(user.ConfirmationPassword);
            ClickLoginButton();
        }

        public abstract bool IsLogginErrorMessageDisplayed();
        public abstract bool IsLogginPageLoaded();
    }
}
