﻿using SeleniumCommon.Page.BasePage;

namespace SeleniumCommon.Page.ProductPage
{
    public interface IProductPage : IBasePage
    {
        IProductPage RefreshPage();
        IProductPage WaitForPageToLoad();
        string GetProductName();
        float GetProductPrice();
        string GetProductCode();
        string GetStockStatus();
        void SetQuantity(int quantity);
        int GetQuantity();
        void AddToCart();
        void AddToWishList();
    }
}