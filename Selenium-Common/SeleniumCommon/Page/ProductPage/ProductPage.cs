﻿using OpenQA.Selenium;

namespace SeleniumCommon.Page.ProductPage
{
    public abstract class ProductPage : BasePage.BasePage, IProductPage
    {
        protected readonly IWebDriver Driver;

        protected ProductPage(IWebDriver driver) : base(driver)
        {
            Driver = driver;
        }

        public new IProductPage RefreshPage()
        {
            base.RefreshPage();
            return this;
        }

        public new IProductPage WaitForPageToLoad()
        {
            base.WaitForPageToLoad();
            return this;
        }

        public abstract string GetProductName();

        public abstract float GetProductPrice();

        public abstract string GetProductCode();

        public abstract string GetStockStatus();

        public abstract void SetQuantity(int quantity);

        public abstract int GetQuantity();

        public abstract void AddToCart();

        public abstract void AddToWishList();
    }
}
