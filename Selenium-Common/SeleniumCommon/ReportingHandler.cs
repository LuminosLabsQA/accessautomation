﻿using NUnit.Framework;
using RelevantCodes.ExtentReports;
using SeleniumCommon.Logging;

namespace SeleniumCommon
{
    [SetUpFixture]
    class ReportingHandler
    {
        public static ExtentReports Reporter;

        [OneTimeSetUp]
        public static void SetUpReporter()
        {
            Reporter = ReportFactory.GetReporter();
        }

        [OneTimeTearDown]
        public static void FinalTearDown()
        {
            ReportFactory.CloseReporter();
        }
    }
}
