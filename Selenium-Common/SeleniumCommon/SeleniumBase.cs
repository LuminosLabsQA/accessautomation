﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using RelevantCodes.ExtentReports;
using SeleniumCommon.ConfigManager;
using SeleniumCommon.Utilities.Snapshot;

namespace SeleniumCommon
{
    public class SeleniumBase
    {
        protected IWebDriver Driver;

        protected ExtentTest TestLogger;
        private ExtentTest _testSuiteLogger;

        [OneTimeSetUp]
        public void ReporterCreateTestSuite()
        {
            _testSuiteLogger = ReportingHandler.Reporter.StartTest(GetType().Name, GetType().Namespace);
        }

        [OneTimeTearDown]
        public void ReporterCloseTestSuite()
        {
            ReportingHandler.Reporter.EndTest(_testSuiteLogger);
        }

        [SetUp]
        public void SetUp()
        {
            Driver = new BrowserFactory().SetupDriver();
            Driver.Navigate().GoToUrl(TestConfig.BaseApplicationUrl + TestConfig.ResourcePathUrl);

            StartLogger(TestContext.CurrentContext.Test.Name, TestContext.CurrentContext.Test.ClassName);
        }

        [TearDown]
        public void TearDown()
        {          
            LogTestResult(TestContext.CurrentContext.Result.Outcome);

            Driver.Close();
            Driver.Quit();

            _testSuiteLogger.AppendChild(TestLogger);
            ReportingHandler.Reporter.EndTest(TestLogger);
        }

        private void StartLogger(string testName, string className)
        {
            string name = className;
            name = name.Substring(testName.IndexOf('.') + 1);
            name = name + " - " + testName;


            TestLogger = ReportingHandler.Reporter.StartTest(name, testName);
            TestLogger.Log(LogStatus.Info, "Starting test " + testName);
        }

        private void LogTestResult(ResultState outcome)
        {
            if (!Equals(outcome, ResultState.Success))
            {
                TestLogger.Log(LogStatus.Info, TestContext.CurrentContext.Result.Message);
            }

            if (Equals(outcome, ResultState.Success))
            {
                TestLogger.Log(LogStatus.Pass, "Test " + TestContext.CurrentContext.Test.Name + " passed");
            }
            else if (Equals(outcome, ResultState.Failure))
            {
                LogResultWithImage(LogStatus.Fail);
            }
            else if (Equals(outcome, ResultState.Inconclusive))
            {
                LogResultWithImage(LogStatus.Unknown);
            }
            else
            {
                LogResultWithImage(LogStatus.Error);
            }
        }

        private void LogResultWithImage(LogStatus logStatus)
        {
            string file = new Snapshot().Snap(Driver, TestContext.CurrentContext.Test.Name);
            string image = TestLogger.AddScreenCapture(file);
            TestLogger.Log(logStatus, "Test " + TestContext.CurrentContext.Test.Name + " " + logStatus, image);
        }
    }   
}
