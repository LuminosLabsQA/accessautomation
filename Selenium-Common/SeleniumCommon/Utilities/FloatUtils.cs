﻿using System;

namespace SeleniumCommon.Utilities
{
    class FloatUtils
    {
        public static bool AreEqual(float firstValue, float secondValue)
        {
            float tolerance = (float) Math.Abs(firstValue * .00001);
            float difference = Math.Abs(firstValue - secondValue);

            return difference <= tolerance;
        }
    }
}
