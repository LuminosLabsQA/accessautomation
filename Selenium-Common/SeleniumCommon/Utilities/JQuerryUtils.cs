﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumCommon.Utilities
{
    public class JQuerryUtils
    {
        public static string GetValue(IWebDriver driver, IWebElement element)
        {
            return GetValue(driver, element, 0);
        }

        /*
         * In some cases the value form the input fields is not set in object value attribut
         * Instead the JQuerry handles the data from the input field
         * In order to get the value from the field we cannot use IWebElement.GetAttribut("value") or IWebElement.Text
         * We have interogate the JQuerry for the value from the input
         */
        public static string GetValue(IWebDriver driver, IWebElement element, int index)
        {
            //Ensure that there is are identyfiable elements
            string elementClass = element.GetAttribute("class");
            string elementId = element.GetAttribute("id");

            if (string.IsNullOrWhiteSpace(elementId) && string.IsNullOrWhiteSpace(elementClass))
                Assert.Fail("Empty element 'id' and 'class' attributes");

            //Build selector to get the element by the browser
            // The cssSelector format is "tagName[class=\"className\"]" or "tagName[id=\"element_id\"]"
            string cssSelector;
            if (!string.IsNullOrWhiteSpace(elementId))
            {
                cssSelector = string.Format("{0}[id=\"{1}\"]",
                    element.TagName, elementId);
            }
            else
            {
                cssSelector = string.Format("{0}[class=\"{1}\"]",
                    element.TagName, elementClass);
            }

            //Get WebDriver to look for elements and execute javaScript

            //Ensure that the javaScirpt script finds the exact element
            if (!string.IsNullOrWhiteSpace(cssSelector))
            {
                var foundElements = driver.FindElements(By.CssSelector(cssSelector));
                //Build and execute the javaScript
                if (foundElements.Count > index)
                {
                    string script = string.Format("return $($('{0}')[{1}]).val()", cssSelector, index);
                    return ((IJavaScriptExecutor)driver).ExecuteScript(script) as string;
                }
                return "fail";
            }
            return "fail";
        }

        /*
         * This method returs the text of the selected option from a drop-down
         */
        public string GetSelectedValueTextFormDropDown(IWebDriver driver, IWebElement element)
        {
            try
            {
                return (string)
                ((IJavaScriptExecutor)driver).ExecuteScript(
                    "return arguments[0].options[arguments[0].selectedIndex].text;", element);
            }
            catch (Exception)
            {
                return null;
            }
            
        }
    }
}
