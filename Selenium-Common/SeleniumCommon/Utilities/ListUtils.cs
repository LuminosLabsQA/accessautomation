﻿using System.Collections.Generic;

namespace SeleniumCommon.Utilities
{
    class ListUtils
    {
        public static bool DoListsContainTheSameElements(IList<object> initialList, IList<object> actualList)
        {
            if (initialList.Count != actualList.Count) return false;
            for (int index = 0; index < initialList.Count; index++)
            {
                if (!initialList[index].Equals(actualList[index])) return false;
            }
            return true;
        }       
    }
}
