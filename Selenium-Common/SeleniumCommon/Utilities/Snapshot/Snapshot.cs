﻿using System;
using OpenQA.Selenium;
using SeleniumCommon.ConfigManager;

namespace SeleniumCommon.Utilities.Snapshot
{
    public class Snapshot
    {
        private string _filename;
        private string _saveLocation;

        public string Snap(IWebDriver driver, string testName)
        {
            try
            {
                CreateDirectoryForScreenShot();
                CreateFilename(testName);
                ((ITakesScreenshot) driver).GetScreenshot().SaveAsFile(_filename, ScreenshotImageFormat.Png);
                return _filename;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void CreateDirectoryForScreenShot()
        {
            try
            {
                _saveLocation = TestConfig.ReportPath + TestConfig.ScreenShotPath + DateTime.Now.ToString("yyyy-MM-dd") + "\\";
                bool dirExists = System.IO.Directory.Exists(_saveLocation);
                if (!dirExists)
                    System.IO.Directory.CreateDirectory(_saveLocation);
            }
            catch (Exception) { }
        }

        public void CreateFilename(string testName)
        {
            try
            {
                var timeStamp = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                _filename = testName;
                try
                {
                    _filename = _filename.Substring(0, _filename.IndexOf('('));
                }
                catch (Exception) { }               
                string ext = ".png";
                _filename = _filename + timeStamp;
                _filename = _saveLocation + _filename + ext;
            }
            catch (Exception) { }
        }
    }
}
