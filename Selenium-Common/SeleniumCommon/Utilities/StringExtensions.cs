﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace SeleniumCommon.Utilities
{
    class StringExtensions
    {
        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string GenerateRandomEmail(Random random)
        {
            string address = GenerateAlphaNumericRandomString(random, random.Next(1, 31));
            string site = GeneratRandomString(random, random.Next(1, 16));
            string domain = GeneratRandomString(random, random.Next(2, 4));
            return address + "@" + site + "." + domain;
        }

        public static string GenerateAlphaNumericRandomString(Random random, int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];

            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string GeneratRandomString(Random random, int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var stringChars = new char[length];

            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string GenerateRandomIntegers(Random random, int length)
        {
            var chars = "0123456789";
            var stringChars = new char[length];

            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new string(stringChars);
            return finalString;
        }

        /*
         * Formatting for item number is "Item #XXXXXX-XX"
         * Where "-XX" is optional
         * And number may be 5-8 digits in length
         */
        public bool IsItemNumberFormValid(string itemNumber)
        {
            /*
             * This method contains the regex for the rule above
             * Legend:
             *  - "^" - string starts with
             *  - "\w{5,8}" - 5 to 8 chars or digits
             *  - "(-\w{2})" - "-XX" (2 chars or digits), "(",")" mark a string section
             *  - "*?" - section between brackets is optional
             *  - "$" - marks the end of string
             */

            Regex rgx = new Regex(@"^\w{5,8}(-\w{2})*?$");
            return rgx.IsMatch(itemNumber);
        }
        
        /*
         * Formatting for item price is "$NN,NNN.NN"
         */
        public bool IsItemPriceFormValid(string itemPrice)
        {
            /*
             * This method contains the regex for the rule above
             * Legend:
             *  - "^" - string starts with
             *  - "[$]" - the first char is "$"
             *  - "(\d+[,])*?" represensts an optional section of one ore more digits followed by ","
             *  - "(\d{1,3})" represensts a section of one to three digits
             *  - "(.\d{2})" - price ends with "." followed by 2 decimals
             *  - "$" - marks the end of string
             */
            Regex rgx = new Regex(@"^[$](\d+[,])*?(\d{1,3})(.\d{2})$");
            return rgx.IsMatch(itemPrice);
        }

        //A method that returns the last X (tailLength) chars from a string
        public static string GetLast(string source, int tailLength)
        {
            if (tailLength >= source.Length)
                return source;
            return source.Substring(source.Length - tailLength);
        }

        public static string FirstCharToUpper(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("The string was null or empty.");
            return input.First().ToString().ToUpper() + string.Join("", input.Skip(1)).ToLower();
        }
    }
}
