﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;

namespace SeleniumCommon.WindowHandler
{
    class WindowHandler
    {
        private readonly IWebDriver _driver;

        public WindowHandler(IWebDriver driver)
        {
            _driver = driver;
        }

        public void OpenNewWindow()
        {
            IJavaScriptExecutor jscript = _driver as IJavaScriptExecutor;
            jscript.ExecuteScript("window.open()");
        }

        public void SwitchToLatestOpenWindow()
        {
            List<string> handles = _driver.WindowHandles.ToList<string>();
            _driver.SwitchTo().Window(handles.Last());
        }

        public void SwithcToFirstDiferrentWindow(string initialWindowHandle)
        {
            foreach (string actualWindowHandle in GetAllWIndowHandles())
            {
                if (actualWindowHandle != initialWindowHandle)
                {
                    _driver.SwitchTo().Window(actualWindowHandle);
                    break;
                }
            }
        }

        private ReadOnlyCollection<string> GetAllWIndowHandles()
        {
            return _driver.WindowHandles;
        }

        public string GetCurrentWindowHandle()
        {
            string initialWindowHandle = _driver.CurrentWindowHandle;
            Debug.WriteLine("Current window handler: " + initialWindowHandle);
            return initialWindowHandle;
        }

        public void CloseCurrentWindow()
        {
            _driver.Close();
        }
    }
}
